# Finitodo  |  [Download (893KB)](https://gitlab.com/finitodo/finitodo/-/raw/main/Bin/finitodo-app)  |  [Screenshot](#screenshot)

### A versatile and straightforward task / todo list manager for Linux

Finitodo is a graphical task/todo list manager for Linux that aims to be flexible and capable without being overly complicated. It has robust functionality in the areas needed such as task recurrence, filters and sorting etc, but doesn't require digging through lots of menu and option screens to do what you want. Everything can be seen and done from one single user-friendly screen.
  
  
#### Installation / Usage

##### Option 1: Download ready-made application

Simply download the application file:

[Download Finitodo](https://gitlab.com/finitodo/finitodo/-/raw/main/Bin/finitodo-app)

...and then set it as executable using either a graphical file manager or via the command line with:

`chmod +x finitodo-app`

The program can then be run by double-clickng it or typing its name at the command line and hitting Enter.

##### Option 2: Build from source

From the terminal:
```
git clone https://gitlab.com/finitodo/finitodo
cd finitodo
make
make install
```

##### Option 3: Install from Arch Users Repository (AUR)

For Arch-based Linux distrubitions, from the terminal:

`yay -S finitodo`

#### Additional Information

Finitodo uses GTK3 and should run correctly on most modern popular Linux distributions, but has been tested most extensively with k/Ubuntu, Linux Mint, Manjaro, MX Linux, and ElementaryOS.  
  

#### [Download (893KB)](https://gitlab.com/finitodo/finitodo/-/raw/main/Bin/finitodo-app)  

| Release Info  |   |
| -------- | -------|  
| Version | 2.0-31 |  
| Release Date | February 23, 2025 |  
| SHA256 Checksum | b041dc8b10edd3c23c69caa00111e7c6947036e89170f9d8b1c93852623c8be4 |  
| Compatible OS | Linux |  
  
    

#### License

Finitodo is released under the GPL version 3, as published by the Free Software Foundation.

  
#### Screenshot
![Screenshot](source/images/screenshots/finitodoss.png)


##### AUR link: https://aur.archlinux.org/packages/finitodo

