#include <gtk/gtk.h>
#include <stdlib.h>
#include "sqlite3.h"
#include <pthread.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>


GtkWidget *win = NULL;
GtkWidget * trunk;
GtkWidget *ulabel,*subtaskbutton,*recsuffix,*pentrybox, * sortl,*notesview,*labeltest,*donelabel;
GtkWidget * ddc,*ddb,*sdb,*sdp,*ddp,*wdtext, *fhidedone,*fhidefuture,*fhideinactive,*fhidecheckboxes,*usecolors,*tib;
GtkWidget * vbox4, *vbox3,*vbox5a,*buttonloc,*hboxtop,*vboxtop1,*vbox3alt,*comparent,*hboxch,*hboxh,*vbox9;
GtkWidget * vbox5,*tf1,*tf2,*tf3,*tf4,*tagory,*labelrba,*helpbutton;
GtkWidget * ddentry,*cview,*inotes,*icat,*ititle,*gf2,*gf3,*gf4,*gf10,*gftop,*gfh;
GtkWidget * sdentry,*tagfilter,*combotf,*labelch,*labelt,*ebch,*ebch2,*catelabel,*acbutton,*tagslabel,*zwindow;
GtkWidget * tentry, * rentry, * comborty, * comborba, * comboper, * combow, *hboxrec1, *hboxdays,*hboxmonth;
GtkWidget * weekday[10], *swindow,*gf6,*adp,*adb,*adentry,*ahour,*aminute,*ampmlabel,*colabel,*nonelabel,*ntbutton;
GtkSpinButton * pentry;
GtkTreeModel  *maintreestore = NULL;
GtkTreeModel * tfmodel=NULL;
GtkListStore * slist=NULL;
GtkListStore * clist=NULL;
GtkTreeModelFilter * gtf;
GtkTreePath * utruepath,*upath;
gint seluid;
GtkTreeIter * seliter=NULL;
GtkStyleContext * context;
GtkCssProvider * provider0;
gboolean rowpick (GtkTreeModel*,GtkTreePath*,GtkTreeIter*,gpointer);
sqlite3 * sgp;
char dbi[2200]={};
char dbu[2200]={};
char dbx[2200]={};
char dbv[2200]={};
char ceffect[1000]={};
char * chapars=NULL;
char curtagory[100]={};
char curdatfile[1000]={};
char ubuf[1000]={};
char * ebuf=NULL;
char * dbz=NULL;
char * dbd=NULL;
char * dbnotes=NULL;
char * dbvalt=NULL;
char * ss=NULL;
char * tempsels=NULL;
char * uidlist=NULL;
char * uidlist2=NULL;
int holdoff=0,startup=1,rdirty=0,unisort=-1,rcpick=0,icall=0,delling=0,chaparmode=0,welcome=0,initload=0,valt=0,vnotes=0;
int togafter=0,noac=0;
double vvertical,hhorizontal,ddpi,sscf,fontfactor;
struct tm unitm;
int whatever,noscroll=0,nosort=0,tpress=0;
char * valtagory, * valnotes;
void AlertMessage (GtkWidget *wid, gchar * mess);
void figstatus(char * c, time_t sda, time_t dda, time_t fda);
void colorstatus(GtkTreeIter * iter);
void ftext(GtkWidget * g,int psize,const char * c);
void checksst(sqlite3_stmt * s, int pa, GtkTreeIter * pi);
void checktags();
void rfilter();
void dbupdate();
void togcomp();
void onSelectionChanged (GtkTreeSelection *treeselection,gpointer user_data);

GtkTreeViewColumn *colarray[8];
GtkTreeViewColumn * chekcol;
GtkTextBuffer * notesbuffer;

pthread_t thread1, thread2,thread3;
pthread_mutex_t lock;
pthread_mutex_t zlock;
pthread_mutexattr_t Attr;
int iret1,iret2;

#define vv *vvertical
#define hh *hhorizontal




enum cols
{
    col_title=0,
    col_priority,
    col_startdatetext,
    col_duedatetext,
    col_creationdatetext,
    col_notes,
    col_tags,
    col_startdateval,
    col_duedateval,
    col_donedateval,
    col_creationdateval,
    col_uid,
    col_pseudopritext,
    col_statustext,
    col_recurtypeval,
    col_recurfreqval,
    col_recurbaseval,
    col_weekdaystext,
    col_periodval,
    col_perioddayval,
    col_sourceduedateval,
    col_pweight,
    col_pcolor,
    col_xp,
    col_alarmdate,
    col_alarmhour,
    col_alarmminute,
    col_alarmdone,
    col_taskdoneint

};

enum dbcols
{
    dbcol_uid=0,
    dbcol_parent,
    dbcol_priority,
    dbcol_duedateval,
    dbcol_startdateval,
    dbcol_tags,
    dbcol_title,
    dbcol_donedateval,
    dbcol_recurtype,
    dbcol_recurfreq,
    dbcol_recurbase,
    dbcol_creationdateval,
    dbcol_weekdaystext,
    dbcol_periodval,
    dbcol_perioddayval,
    dbcol_sourceduedateval,
    dbcol_notes,
    dbcol_xp,
    dbcol_alarmdate,
    dbcol_alarmhour,
    dbcol_alarmminute,
    dbcol_alarmdone


};


typedef struct sset
{
    int heading;
    int ad;
} sset;

struct sset sortset[7]={};



void msgbox(char * m)
{
    AlertMessage(NULL,m);
}


void qcpy(char** c,int i,char* extra,int lim)
{
    if(!i || !*c){*c=realloc(*c,strlen(extra)+2);memset(*c,0,strlen(extra)+2);}
    else{*c=realloc(*c,(strlen(*c)+strlen(extra)+2));}
    strncat(*c,extra,lim==-1 ? strlen(extra) : lim);
}


void repx(char ** fullstr, char * oldstr, char * newstr)
{
    if(strlen(oldstr)==0){return;}
    char * dvx = NULL;
    char * ftr = *fullstr, *str=*fullstr;

    while (1)
    {
        ftr=strstr(ftr,oldstr);
        if(ftr == NULL)
        {
            qcpy(&dvx,1,str,-1);
            break;
        }
        qcpy(&dvx,1,str,(ftr-str));
        qcpy(&dvx,1,newstr,-1);
        ftr+=strlen(oldstr);
        str=ftr;
    }

    qcpy(fullstr,0,dvx,-1);
    ((*fullstr)+strlen(dvx))[0]=0;
    free(dvx);

}




void reps(char * fullstr, char * oldstr, char * newstr, size_t maxlen)
{
    if(strlen(oldstr)==0){return;}
    char * ftr = fullstr;
    maxlen-=1;

    while(1)
    {
        ftr=strstr(ftr,oldstr);
        if(ftr == NULL) {break; }
        int maxavail=maxlen-(ftr+strlen(newstr)-fullstr)+0;
        (maxavail<0) && (maxavail=0);
        memmove(ftr+strlen(newstr),ftr+strlen(oldstr),maxavail);
        memset(fullstr+maxlen,0,1);
        maxavail=maxlen-(ftr-fullstr)+0;
        (maxavail<0) && (maxavail=0);
        memcpy(ftr,newstr,maxavail > strlen(newstr) ? strlen(newstr) : maxavail);
        ftr+=strlen(newstr);
    }
    fullstr[strlen(fullstr)]=0;
    return;
}

void fixquot(char * fullstr)
{
    int chekar=strlen(fullstr)-1;
    int quotcount=0;
    while (fullstr[chekar]=='\''){chekar--;quotcount++;}
    if (quotcount & 1){fullstr[strlen(fullstr)-1]=0;}
}



void sfix(char * fullstr, size_t maxlen)
{
    reps(fullstr,"'","''",maxlen);
    fixquot(fullstr);
}


void zcpy(char** c,int i,char* extra)
{
    pthread_mutex_lock(&zlock);
    if(!i || !*c){*c=realloc(*c,strlen(extra)+2);strcpy(*c,extra);}
    else{*c=realloc(*c,(strlen(*c)+strlen(extra)+2));strcat(*c,extra);}
    pthread_mutex_unlock(&zlock);
}




gboolean checkxp (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer       userdata)
{
    gint ixp;
    gtk_tree_model_get(model,iter,col_xp,&ixp,-1);
    if(ixp){gtk_tree_view_expand_row((GtkTreeView *)trunk,path,FALSE);}
    return FALSE;
}

gboolean checkal (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer       userdata)
{
    time_t tnow=time(0);
    struct tm tday=*localtime(&tnow);
    static int curday=0;
    static int lastminute=0;
    tnow=time(0);
    tday=*localtime(&tnow);
    gchar * cname=NULL;
    gdouble aldate,donedate;
    gint aldone,muid;
    gtk_tree_model_get(model,iter,col_title,&cname,col_alarmdate,&aldate,col_alarmdone,&aldone,col_uid,&muid,col_donedateval,&donedate,-1);

    tnow+=(60*60) * (tday.tm_isdst);

    if(aldate)
    {
        char buf[100]={};
        sprintf(buf,"%f",aldate);
        sprintf(buf,"%f",(double)tnow);
    }
    if(aldate && (aldate < (double)tnow) && !aldone && !donedate)
    {
        char buf[100]={};
        sprintf(buf,"aldate: %f",aldate);
        sprintf(buf,"nowdate: %f",(double)tnow);
        AlertMessage(NULL,cname);
        gtk_tree_store_set((GtkTreeStore *)maintreestore,iter,col_alarmdone,1,-1);
        sprintf(buf,"update tasks set alarmdone=1 where UID=%i;",muid);
        zcpy(&dbz,1,buf);
        dbupdate();
    }
    g_free(cname);

}

gboolean fack (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer       userdata)
{
    gtk_tree_store_set((GtkTreeStore *)model,iter,col_statustext,"",-1);
    gtk_tree_store_set((GtkTreeStore *)model,iter,col_pcolor,"#333333",col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);
}

gboolean fupdate (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer       userdata)
{
    gdouble dda,sda,fda;
    gchar * ptitle;
    gtk_tree_model_get(model,iter,col_startdateval,&sda,col_duedateval,&dda,col_donedateval,&fda,col_title,&ptitle,-1);

    char fstatus[100];
    figstatus(fstatus,(time_t)sda,(time_t)dda,(time_t)fda);

    int doneint;
    if(fda > 0 || fda < -1){doneint=1;}else{doneint=0;}

    gtk_tree_store_set((GtkTreeStore*)model,iter,col_statustext,(gchar*)fstatus,-1);
    gtk_tree_store_set((GtkTreeStore*)model,iter,col_taskdoneint,doneint,-1);


    colorstatus(iter);
    g_free(ptitle);
    return FALSE;
}

void supdate()
{
    gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
}


void savetf()
{
    GtkTreeIter iter;
    GtkWidget * combotftext = gtk_bin_get_child((GtkBin*)combotf);
    const gchar * curtf=gtk_entry_get_text((GtkEntry*)combotftext);

    if(!gtk_tree_model_get_iter_first(tfmodel,&iter))
    {
        gtk_tree_store_prepend((GtkTreeStore*)tfmodel, &iter,NULL);
        gtk_tree_store_set((GtkTreeStore*)tfmodel, &iter,0,curtf,-1);
        return;
    }

    int found=0;
    do
    {   gchar * cname=NULL;
        gtk_tree_model_get(tfmodel,&iter,0,&cname,-1);
        if(!strcmp(cname,curtf)){found=1;}
        g_free(cname);
    } while(gtk_tree_model_iter_next(tfmodel,&iter));

    if(!found)
    {
        gtk_tree_store_prepend((GtkTreeStore*)tfmodel, &iter,NULL);
        gtk_tree_store_set((GtkTreeStore*)tfmodel, &iter,0,curtf,-1);
    }

}

void helpscreen()
{
    static int shown=0;

    if(!shown)
    {
        GtkWidget * bla=gtk_bin_get_child((GtkBin*)helpbutton);
        ftext(bla,40,"x");
        gtk_widget_hide(gf2);
        gtk_widget_hide(gf10);
        gtk_widget_show(gfh);
        shown=1;
    }
    else
    {
        GtkWidget * bla=gtk_bin_get_child((GtkBin*)helpbutton);
        ftext(bla,40,"?");
        gtk_widget_show(gf2);
        gtk_widget_show(gf10);
        gtk_widget_hide(gfh);
        shown=0;
    }






}



void datopen(char * dfile)
{
    pthread_mutex_lock(&lock);
    if(!strlen(dfile)){AlertMessage(NULL,"No currently loaded task data file. Please use the Open File option to open an existing task data file or create a new one.");pthread_mutex_unlock(&lock);return;}
    sqlite3_stmt * sst;
    gtk_tree_store_clear((GtkTreeStore*)maintreestore);
    gtk_list_store_clear(clist);
    GtkTreeIter nciter;
    gtk_list_store_append(clist, &nciter);
    gtk_list_store_set(clist,&nciter,0,"(None)",-1);
    gtk_combo_box_text_remove_all((GtkComboBoxText*)tagory);
    gtk_combo_box_text_append((GtkComboBoxText*)tagory,0,"(None)");
    FILE * fp=fopen(dfile,"r");

    if(!fp)
    {
        FILE * fp=fopen(dfile,"w+");
        sprintf(ubuf,"Could not write task data file:\n\n %s \n\nPlease use the Open File option to open an existing task data file or create a new one.",dfile);
        if(!fp){AlertMessage(NULL,ubuf);curdatfile[0]=0;pthread_mutex_unlock(&lock);return;}
        else{fclose(fp);}

        if(sqlite3_open(dfile,&sgp)==SQLITE_OK)
        {
            sqlite3_exec(sgp,"CREATE TABLE tasks ( UID integer primary key, parent INTEGER, priority INTEGER, duedate REAL, startdate REAL, tags TEXT , title TEXT, donedate REAL, recurtype INTEGER, recurfreq INTEGER, recurbase INTEGER, creationdate REAL, weekdays TEXT, period INTEGER, periodday INTEGER, sourceduedate REAL, notes TEXT, xp INTEGER, alarmdate REAL, alarmhour INTEGER, alarmminute INTEGER, alarmdone INTEGER);",NULL,NULL,NULL);
            sqlite3_exec(sgp,"INSERT INTO tasks ( title, tags,notes,recurfreq, parent ) VALUES ('Welcome Task','','Welcome to Finitodo\n\nClick the Help button in the top menu bar for more information.',1,0);",NULL,NULL,NULL);
            strcpy(curdatfile,dfile);
            sqlite3_close(sgp);
            welcome=1;
            strcpy(ubuf,"Finitodo (");
            strcat(ubuf,curdatfile);
            strcat(ubuf,")");
            gtk_window_set_title (GTK_WINDOW (win), ubuf);
            sqlite3_close(sgp);
        }
        else{AlertMessage(NULL,"Error creating new task data file");curdatfile[0]=0;pthread_mutex_unlock(&lock);return;}
    }

    if(fp){fclose(fp);}
    fp=fopen(dfile,"r+");
    sprintf(ubuf,"Could not write to task data file:\n\n %s \n\nPlease use the Open File option to open an existing task data file or create a new one.",dfile);
    if(!fp){AlertMessage(NULL,ubuf);curdatfile[0]=0;pthread_mutex_unlock(&lock);return;}
    else{fclose(fp);}

    if(sqlite3_open(dfile,&sgp)==SQLITE_OK)
    {
        sqlite3_prepare_v2(sgp,"PRAGMA schema_version",-1,&sst,NULL);
        if(SQLITE_ROW==sqlite3_step(sst))
        {
            sqlite3_finalize(sst);strcpy(curdatfile,dfile);
            strcpy(ubuf,"Finitodo (");
            strcat(ubuf,curdatfile);
            strcat(ubuf,")");
            gtk_window_set_title (GTK_WINDOW (win), ubuf);
            if(sqlite3_prepare_v2(sgp,"select * from tasks",-1,&sst,NULL)==SQLITE_OK)
            {
                zcpy(&uidlist,0,"select * from tasks where UID!=-1");
                checksst(sst,0,NULL);
                initload=1;
                gtk_tree_model_foreach((GtkTreeModel*)gtf, checkxp, NULL);
                initload=0;
                sqlite3_finalize(sst);
                checktags();
            }
            GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
            gtk_tree_selection_unselect_all(treeselection);
            gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
            rfilter();

        GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
        gtk_tree_sortable_set_sort_column_id(sortable, 99, GTK_SORT_DESCENDING);
        supdate();
        gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);

        }
        else
        {
            sqlite3_finalize(sst);
            AlertMessage(NULL,"Selected file does not appear to be a valid task data file");
            curdatfile[0]=0;
        }
    }
    sqlite3_close(sgp);
    pthread_mutex_unlock(&lock);
    return;
}


int File_Copy (char FileSource [], char FileDestination [])
{
    int   c;
    FILE *stream_R;
    FILE *stream_W;

    stream_R = fopen (FileSource, "r");
    if (stream_R == NULL)
        return -1;
    stream_W = fopen (FileDestination, "w");
    if (stream_W == NULL)
     {
        fclose (stream_R);

        return -2;
     }
    while ((c = fgetc(stream_R)) != EOF)
        fputc (c, stream_W);
    fclose (stream_R);
    fclose (stream_W);

    return 0;
}


void trysave()
{
    int fc;
    gint res;
    GtkWidget * sdialog = gtk_file_chooser_dialog_new ("Save File",NULL,GTK_FILE_CHOOSER_ACTION_SAVE, ("_Cancel"),GTK_RESPONSE_CANCEL,("_Save"),GTK_RESPONSE_ACCEPT,NULL);
    GtkFileChooser *chooser = GTK_FILE_CHOOSER (sdialog);
    gtk_file_chooser_set_filename(chooser,curdatfile);
    gtk_file_chooser_set_do_overwrite_confirmation (chooser, FALSE);
    res = gtk_dialog_run (GTK_DIALOG (sdialog));
    if (res == GTK_RESPONSE_ACCEPT)
  {
    gchar * filename=NULL;
    filename = gtk_file_chooser_get_filename (chooser);
    if(!strcmp(filename,curdatfile)){gtk_widget_destroy (sdialog);return;}
    fc=File_Copy(curdatfile,(char*)filename);
    if(!fc)
    {
        strcpy(curdatfile,(char*)filename);
        strcpy(ubuf,"Finitodo (");
        strcat(ubuf,curdatfile);
        strcat(ubuf,")");
        gtk_window_set_title (GTK_WINDOW (win), ubuf);
    }
    g_free (filename);
  }

   gtk_widget_destroy (sdialog);
   if(fc){AlertMessage(NULL,"Error saving task data file");}
}


void tryopen()
{
    gint res;
    GtkWidget * sdialog = gtk_file_chooser_dialog_new ("Open File",NULL,GTK_FILE_CHOOSER_ACTION_SAVE, ("_Cancel"),GTK_RESPONSE_CANCEL,("_Open"),GTK_RESPONSE_ACCEPT,NULL);
    GtkFileChooser *chooser = GTK_FILE_CHOOSER (sdialog);
    gtk_file_chooser_set_filename(chooser,curdatfile);
    res = gtk_dialog_run (GTK_DIALOG (sdialog));

    if (res == GTK_RESPONSE_ACCEPT)
      {
        gchar *filename=NULL;

        filename = gtk_file_chooser_get_filename (chooser);
        gtk_widget_hide(sdialog);
        datopen((char*)filename);
        g_free (filename);
      }

    gtk_widget_destroy (sdialog);


}



gboolean sforeach_func (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer       userdata)
  {
      gint colnum;
      gtk_tree_model_get(model, iter, 2, &colnum, -1);

      for(int x=1;x<7;x++)
      {
          if(colnum==sortset[x].heading)
          {
              gtk_list_store_set(GTK_LIST_STORE(slist),iter,1,(sortset[x].ad==0 ? "ASC" : "DES"),-1);
              continue;
          }
      }
      return FALSE;
  }



void checktags()
{
    pthread_mutex_lock(&lock);
    sqlite3_stmt * s;
    sqlite3_prepare_v2(sgp,"select tags from tasks where tags!='' GROUP BY tags ORDER BY tags ASC;",-1,&s,NULL);
    while (SQLITE_ROW==sqlite3_step(s))
    {
        GtkTreeIter citer;
        gtk_combo_box_text_append_text((GtkComboBoxText*)tagory,sqlite3_column_text(s,0));
        if(!sqlite3_column_text(s,0)){continue;}
        gtk_list_store_append(clist, &citer);
        gtk_list_store_set(clist,&citer,0,sqlite3_column_text(s,0),-1);
    }
    sqlite3_finalize(s);
    pthread_mutex_unlock(&lock);
}



void trunkfocus(GtkWidget * g, int i)
{
    GtkWidget * f = gtk_window_get_focus((GtkWindow*)win);
    if (!GTK_IS_ENTRY(f) && !gtk_widget_is_focus(g)){gtk_widget_grab_focus(g);}
}

gboolean newtasko(int i)
{
    if(gtk_widget_is_focus(tentry)){gtk_widget_grab_focus(trunk);return TRUE;}
    else if(gtk_widget_is_focus(trunk)){togcomp();return TRUE;}
    return FALSE;

}

gboolean newtasku(int i)
{
    if(gtk_widget_is_focus(trunk)){togcomp();return TRUE;}
    return FALSE;

}




gboolean resel(GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gint ud=GPOINTER_TO_INT(userdata);
    gint guid;
    gtk_tree_model_get(model,iter,col_uid,&guid,-1);


    if(guid==ud)
    {
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));

        upath = gtk_tree_model_filter_convert_path_to_child_path(GTK_TREE_MODEL_FILTER (gtf),path);
        upath = gtk_tree_model_filter_convert_child_path_to_path(GTK_TREE_MODEL_FILTER (gtf),upath);
        gtk_tree_selection_select_path(sel,path);


        return TRUE;
    }
    return FALSE;
}


gboolean colvis3 (GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gint gi;
    gtk_tree_model_get(model,iter,2,&gi,-1);

    if(!gtk_tree_view_column_get_visible(colarray[gi]))
    {gtk_list_store_set(GTK_LIST_STORE(slist),iter,4,"h",-1);}
    return FALSE;

}



void loadprefs()
{

    char arg1[200];
    char exepath[1024] = {0};
    sprintf( arg1, "/proc/%d/exe", getpid() );
    readlink( arg1, exepath, 1024 );
    char inipath[1000]={};
    strcat(inipath,exepath);
    strrchr(inipath,'/')[0]=0;
    strcat(inipath,"/finitodo.ini");

    FILE * fp;
    fp=fopen(inipath,"r+");
    if (!fp){fp=fopen(inipath,"w+");}
    if (!fp)
    {
        const char *homedir;
        homedir = getenv("HOME");
        strcpy(inipath,homedir);
        strcat(inipath,"/.config");
        int result = mkdir(inipath, 0777);
        strcat(inipath,"/finitodo");
        result = mkdir(inipath, 0777);
        strcat(inipath,"/finitodo.ini");
        fp=fopen(inipath,"r+");
    }
    if (fp==NULL){fp=fopen(inipath,"w+");}
    if (fp==NULL){AlertMessage(NULL,"Unable to write to settings file. Please move the program application file to a location where write access is permitted. The program will now close.");exit(0);}


    strrchr(inipath,'/')[0]=0;
    strcat(inipath,"/finitodo1.dat");
    strcpy(curdatfile,inipath);
    int defdatfile=1;

    char buffer[1000]={},prefname[100]={};
    GtkTreeViewColumn * colpos[9]={};

    while (fgets(buffer,999,fp))
    {
        char * prefval=strchr(buffer,27);
        if (prefval==NULL){return;}
        prefval++[0]=0;
        char* nline=strchr(prefval,10);
        if (nline!=NULL){nline[0]=0;}

        char * prefname=buffer;
        long pval=strtol(prefval,NULL,10);

        if(!strcmp(prefname,"fhidedone"))
        {gtk_toggle_button_set_active((GtkToggleButton*)fhidedone,pval);}

        if(!strcmp(prefname,"fhidefuture"))
        {gtk_toggle_button_set_active((GtkToggleButton*)fhidefuture,pval);}

        if(!strcmp(prefname,"fhideinactive"))
        {gtk_toggle_button_set_active((GtkToggleButton*)fhideinactive,pval);}

        if(!strcmp(prefname,"fhidecheckboxes"))
        {gtk_toggle_button_set_active((GtkToggleButton*)fhidecheckboxes,pval);}

        if(!strcmp(prefname,"usecolors"))
        {gtk_toggle_button_set_active((GtkToggleButton*)usecolors,pval);}

        if(!strcmp(prefname,"comparent"))
        {gtk_toggle_button_set_active((GtkToggleButton*)comparent,pval);}

        if(!strcmp(prefname,"buttonloc"))
        {gtk_toggle_button_set_active((GtkToggleButton*)buttonloc,pval);}

        if(!strcmp(prefname,"tf1"))
        {gtk_toggle_button_set_active((GtkToggleButton*)tf1,pval);}

        if(!strcmp(prefname,"tf2"))
        {gtk_toggle_button_set_active((GtkToggleButton*)tf2,pval);}

        if(!strcmp(prefname,"tf4"))
        {gtk_toggle_button_set_active((GtkToggleButton*)tf4,pval);}

        if(!strcmp(prefname,"inotes"))
        {gtk_toggle_button_set_active((GtkToggleButton*)inotes,pval);}

        if(!strcmp(prefname,"icat"))
        {gtk_toggle_button_set_active((GtkToggleButton*)icat,pval);}

        if(!strcmp(prefname,"ititle"))
        {gtk_toggle_button_set_active((GtkToggleButton*)ititle,pval);}


        if(!strcmp(prefname,"sorder"))
        {
            char * c = strchr(prefval,32);
            int h=1;int a=0;
            while(c && h<=6)
            {
                c++;
                int p=atol(c);
                if(!a){sortset[h].heading=p;}
                else{sortset[h].ad=p;}
                a^=1;if(!a){h++;}
                c=strchr(c,32);
            }
            GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
            GtkTreeSortable * sortable2 = GTK_TREE_SORTABLE(slist);
            gtk_tree_sortable_set_sort_column_id(sortable2, 2, GTK_SORT_DESCENDING);
            gtk_tree_sortable_set_sort_column_id(sortable2, 1, GTK_SORT_DESCENDING);
            gtk_tree_sortable_set_sort_column_id(sortable, 10, GTK_SORT_DESCENDING);
            gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);
            gtk_tree_model_foreach(GTK_TREE_MODEL(slist), sforeach_func, NULL);
        }

        int cw[6];

        if(!strcmp(prefname,"colwidth5"))
        {cw[5]=pval;gtk_tree_view_column_set_expand(colarray[5],FALSE);gtk_tree_view_column_set_fixed_width(colarray[5],pval);gtk_tree_view_column_set_expand(colarray[5],TRUE);}

        if(!strcmp(prefname,"colwidth4"))
        {cw[4]=pval;gtk_tree_view_column_set_fixed_width(colarray[4],pval);}

        if(!strcmp(prefname,"colwidth3"))
        {cw[3]=pval;gtk_tree_view_column_set_fixed_width(colarray[3],pval);}

        if(!strcmp(prefname,"colwidth2"))
        {cw[2]=pval;gtk_tree_view_column_set_fixed_width(colarray[2],pval);}

        if(!strcmp(prefname,"colwidth1"))
        {cw[1]=pval;gtk_tree_view_column_set_fixed_width(colarray[1],pval);}

        if(!strcmp(prefname,"colwidth6"))
        {cw[6]=pval;gtk_tree_view_column_set_fixed_width(colarray[6],pval);}

        int tcw=cw[5]+cw[4]+cw[3]+cw[2]+cw[1]+cw[6];

        char buf[100]={};


        if(!strcmp(prefname,"colvis6"))
        {gtk_tree_view_column_set_visible(colarray[6],pval);}
        if(!strcmp(prefname,"colvis5"))
        {gtk_tree_view_column_set_visible(colarray[5],pval);}
        if(!strcmp(prefname,"colvis4"))
        {gtk_tree_view_column_set_visible(colarray[4],pval);}
        if(!strcmp(prefname,"colvis3"))
        {gtk_tree_view_column_set_visible(colarray[3],pval);}
        if(!strcmp(prefname,"colvis2"))
        {gtk_tree_view_column_set_visible(colarray[2],pval);}
        if(!strcmp(prefname,"colvis1"))
        {gtk_tree_view_column_set_visible(colarray[1],pval);}


        if(!strcmp(prefname,"colpos1")){colpos[1]=colarray[pval];}
        if(!strcmp(prefname,"colpos2")){colpos[2]=colarray[pval];}
        if(!strcmp(prefname,"colpos3")){colpos[3]=colarray[pval];}
        if(!strcmp(prefname,"colpos4")){colpos[4]=colarray[pval];}
        if(!strcmp(prefname,"colpos5")){colpos[5]=colarray[pval];}
        if(!strcmp(prefname,"colpos6")){colpos[6]=colarray[pval];}
        if(!strcmp(prefname,"colpos7")){colpos[7]=colarray[pval];}

        gtk_tree_model_foreach((GtkTreeModel*)slist,colvis3, NULL);



        if(!strcmp(prefname,"tftext"))
        {
            GtkWidget * combotftext = gtk_bin_get_child((GtkBin*)combotf);
            gtk_entry_set_text((GtkEntry*)combotftext,prefval);
        }

        if(!strcmp(prefname,"tfvals"))
        {
            char * token;
            char* rest = prefval;
            GtkTreeIter iter;
            while ((token = strtok_r(rest, "|", &rest)))
            {
                gtk_tree_store_append((GtkTreeStore*)tfmodel,&iter,NULL);
                gtk_tree_store_set((GtkTreeStore*)tfmodel, &iter,0,token,-1);
            }
        }

        if(!strcmp(prefname,"sels"))
        {
            while(strchr(prefval,'|'))
            {
                prefval=strchr(prefval,'|');
                prefval++;
                int usel=atol(prefval);
                gtk_tree_model_foreach((GtkTreeModel*)gtf, resel, (gpointer)(long)usel);
                GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
                onSelectionChanged(sel,NULL);

            }
        }

        if(!strcmp(prefname,"csels"))
        {
            GtkTreeIter iter;
            gtk_tree_model_get_iter_first((GtkTreeModel*)clist,&iter);

            do
            {
                gchar * cname=NULL;
                gtk_tree_model_get((GtkTreeModel *)clist,&iter,0,&cname,-1);
                char chname[100]={};
                strcpy(chname,"|");
                strncat(chname,cname,90);
                strcat(chname,"|");
                if(strstr(prefval,chname))
                {
                    GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(cview));
                    gtk_tree_selection_select_iter(treeselection,&iter);

                }
                g_free(cname);


            } while(gtk_tree_model_iter_next((GtkTreeModel*)clist,&iter));
            gtk_tree_model_filter_refilter(gtf);
        }
        if(!strcmp(prefname,"datfile"))
        {defdatfile=0;strncpy(curdatfile,prefval,900);datopen(curdatfile);}

    }

    colpos[0]=NULL;

    int allcolpos=0;
    for(int x=1;x<=7;x++)
    {allcolpos+=(int)colpos[x];}
    if(allcolpos==28)
    {
        for(int x=1;x<=7;x++)
        {gtk_tree_view_move_column_after((GtkTreeView*)trunk,colpos[x],colpos[x-1]);}
    }

    gtk_tree_view_move_column_after((GtkTreeView*)trunk,colarray[7],NULL);
    gtk_tree_view_move_column_after((GtkTreeView*)trunk,colarray[5],colarray[7]);


    if(defdatfile){datopen(curdatfile);}

    fclose(fp);
}



gboolean storesel(GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gchar * ptitle;
    gint guid;
    char buffer[1000];
    gtk_tree_model_get(model,iter,col_title,&ptitle,col_uid,&guid,-1);
    snprintf(buffer,999,"|%i",guid);
    zcpy(userdata,1,buffer);
    g_free(ptitle);
    return FALSE;
}


gboolean storchas(GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gint guid;
    char buffer[1000];
    gtk_tree_model_get(model,iter,col_uid,&guid,-1);
    sprintf(buffer," or UID=%i ",guid);
    zcpy(userdata,1,buffer);
    GtkTreeIter niter;
    gtk_tree_model_filter_convert_iter_to_child_iter(gtf,&niter,iter);
    gtk_tree_store_set(GTK_TREE_STORE(maintreestore),&niter,col_pcolor,"#ff9900",col_pweight,(gdouble)PANGO_WEIGHT_BOLD,-1);
    return FALSE;

}


void chapar(GtkWidget * g,int i)
{
    pthread_mutex_lock(&lock);
    gtk_widget_grab_focus(trunk);
    char buf[1000];

    char chaparsx[1000]={};

    if (!i)
    {
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        if (!gtk_tree_selection_count_selected_rows(sel)){pthread_mutex_unlock(&lock);return;}

        zcpy(&chapars,0,"");
        GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_tree_selection_selected_foreach(sel1, storchas, &chapars);
        gtk_tree_selection_set_mode(gtk_tree_view_get_selection(trunk),GTK_SELECTION_SINGLE);
        gtk_tree_selection_unselect_all(sel1);

        gtk_widget_set_sensitive(gftop,FALSE);
        gtk_widget_set_sensitive(gf2,FALSE);
        gtk_widget_set_sensitive(gf10,FALSE);
        gtk_widget_hide(labelt);
        gtk_widget_show(ebch);
        gtk_widget_show(ebch2);

        chaparmode=1;

    }
    else if(i==1)
    {
        GtkTreeIter iter;
        gint puid=0;
        GtkTreeModel * mdl;
        GtkTreePath * apath;
        gchar * newpar=NULL;


        ftext(labelt,20,"One moment please...");
        gtk_widget_hide(ebch);
        gtk_widget_hide(ebch2);
        gtk_widget_show(labelt);
        gtk_widget_queue_draw (labelt);

        struct timespec ts={};
        ts.tv_nsec=1000000;

            for(int uu=1;uu<30;uu++)
            {
                while (gtk_events_pending ())
                gtk_main_iteration ();
                nanosleep(&ts,NULL);
                //usleep(1000);
            }


        GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        if (gtk_tree_selection_count_selected_rows(sel1))
        {
            gtk_tree_selection_get_selected(sel1,&mdl,&iter);
            gtk_tree_model_get(gtf,&iter,col_title,&newpar,-1);
            GtkTreePath * path=gtk_tree_model_get_path(gtf,&iter);
            GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path(GTK_TREE_MODEL_FILTER (gtf),path);
            GtkTreeIter riter;
            gtk_tree_model_get_iter(gtf, &riter, path);
            gtk_tree_model_get(gtf,&iter,col_uid,&puid,col_title,&newpar,-1);
            gtk_tree_path_free(path);
            gtk_tree_path_free(true_path);
        }

        char puidcheck[100]={};
        sprintf(puidcheck," or UID=%i ",puid);
        if(strstr(chapars,puidcheck)){puid=0;chapar(NULL,2);pthread_mutex_unlock(&lock);return;}


        sprintf(buf,"update tasks set parent=%i where UID=-1",puid);
        pthread_mutex_lock(&zlock);
        zcpy(&dbz,0,buf);
        zcpy(&dbz,1,chapars);
        pthread_mutex_unlock(&zlock);
        tdbupdate();
        chaparmode=0;
        gtk_tree_selection_set_mode(gtk_tree_view_get_selection(trunk),GTK_SELECTION_MULTIPLE);
        gtk_widget_hide(swindow);
        gtk_tree_store_clear(maintreestore);
        sqlite3_open(curdatfile,&sgp);
        sqlite3_stmt * sst;
        sqlite3_prepare_v2(sgp,"select * from tasks",-1,&sst,NULL);
        zcpy(&uidlist,0,"select * from tasks where UID!=-1");
        zcpy(&uidlist2,0,"update tasks set parent=0 where UID!=-1");
        checksst(sst,0,NULL);
        gtk_tree_model_foreach(gtf, checkxp, NULL);
        sqlite3_finalize(sst);
        sqlite3_prepare_v2(sgp,uidlist,-1,&sst,NULL);
        checksst(sst,-1,NULL);
        sqlite3_finalize(sst);
        int c=sqlite3_exec(sgp,uidlist2,NULL,NULL,NULL);
        sqlite3_close(sgp);
        gtk_widget_show(swindow);

        int snone=0;
        if(puid)
        {
            gtk_tree_model_foreach(gtf, resel, puid);
            gtk_tree_selection_unselect_all(gtk_tree_view_get_selection(trunk));
            gtk_tree_view_scroll_to_cell(trunk,upath,NULL,TRUE,0.5,0);
            gtk_tree_view_expand_row(trunk,upath,FALSE);
        }
        else{snone=1;}

        gtk_tree_path_free(upath);
        upath=NULL;
        gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
        rfilter();
        GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
        gtk_tree_sortable_set_sort_column_id(sortable, 10, GTK_SORT_DESCENDING);
        gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);


        gtk_widget_set_sensitive(gftop,TRUE);
        gtk_widget_set_sensitive(gf2,TRUE);
        gtk_widget_set_sensitive(gf10,TRUE);
        gtk_widget_hide(ebch);
        gtk_widget_hide(ebch2);
        gtk_widget_show(labelt);
        g_free(newpar);
        if(snone || 1)
        {
            //if(snone){selnone();}
            char * rsel=strchr(chapars,'=');
            while(strchr(rsel,'='))
            {
                rsel=strchr(rsel,'=');
                rsel++;
                int usel=atol(rsel);
                gtk_tree_model_foreach((GtkTreeModel*)gtf, resel, (gpointer)(long)usel);
                GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
                onSelectionChanged(sel,NULL);
                gtk_widget_grab_focus(trunk);

            }
        }

        ftext(labelt,20,"Task List");
        gtk_widget_hide(ebch);
        gtk_widget_hide(ebch2);
        gtk_widget_show(labelt);



    }
    else if(i==2)
    {
        if(!chaparmode)
        {
            GtkWidget * combotftext = gtk_bin_get_child(combotf);
            if(gtk_widget_is_focus(combotftext))
            {
                gtk_entry_set_text(combotftext,"");
            }

            pthread_mutex_unlock(&lock);
            return;
        }

        ftext(labelt,20,"One moment please...");
        gtk_widget_show(labelt);
        gtk_widget_hide(ebch);
        gtk_widget_hide(ebch2);
        gtk_widget_queue_draw (labelt);

        struct timespec ts={};
        ts.tv_nsec=1000000;
            for(int uu=1;uu<30;uu++)
            {
                while (gtk_events_pending ())
                gtk_main_iteration ();
                nanosleep(&ts,NULL);
                //usleep(1000);
            }


        chaparmode=0;
        gtk_tree_selection_set_mode(gtk_tree_view_get_selection(trunk),GTK_SELECTION_MULTIPLE);
        gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
        rfilter();
        GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
        gtk_tree_sortable_set_sort_column_id(sortable, 10, GTK_SORT_DESCENDING);
        gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);
        gtk_widget_set_sensitive(gftop,TRUE);
        gtk_widget_set_sensitive(gf2,TRUE);
        gtk_widget_set_sensitive(gf10,TRUE);

        ftext(labelt,20,"Task List");

        char * rsel=strchr(chapars,'=');
        while(strchr(rsel,'='))
        {
            rsel=strchr(rsel,'=');
            rsel++;
            int usel=atol(rsel);
            gtk_tree_model_foreach((GtkTreeModel*)gtf, resel, (gpointer)(long)usel);
            GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
            onSelectionChanged(sel,NULL);

        }




    }

    gtk_widget_grab_focus(trunk);
    pthread_mutex_unlock(&lock);
}



void chaparx()
{
    GtkWidget * combotftext = gtk_bin_get_child(combotf);
    if(gtk_widget_is_focus(combotftext)){cleartf();}
    if(!chaparmode){return;}
    chapar(NULL,2);
}

void chaparz()
{
    if(!chaparmode){return;}
    chapar(NULL,1);
}


void SavePrefs()
{
    char buffer[1000]={};
    char prefs[1000]={};

    char * zprefs=NULL;


    zcpy(&zprefs,1,"datfile\x1b");
    sprintf(buffer,"%s\n",curdatfile);
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"fhidedone\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(fhidedone));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"fhidefuture\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(fhidefuture));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"fhideinactive\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(fhideinactive));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"fhidecheckboxes\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(fhidecheckboxes));
    zcpy(&zprefs,1,buffer);



    zcpy(&zprefs,1,"comparent\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(comparent));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"usecolors\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(usecolors));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"buttonloc\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(buttonloc));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"tf1\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(tf1));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"tf2\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(tf2));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"tf4\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(tf4));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"inotes\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(inotes));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"icat\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(icat));
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"ititle\x1b");
    sprintf(buffer,"%i\n",gtk_toggle_button_get_active(ititle));
    zcpy(&zprefs,1,buffer);


    zcpy(&zprefs,1,"sorder\x1b");
    for(int x=1;x<=6;x++)
    {
        sprintf(buffer," %i %i",sortset[x].heading,sortset[x].ad);
        zcpy(&zprefs,1,buffer);
    }
    zcpy(&zprefs,1,"\n");
    zcpy(&zprefs,1,"colwidth1\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_width(colarray[1]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colwidth2\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_width(colarray[2]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colwidth3\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_width(colarray[3]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colwidth4\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_width(colarray[4]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colwidth5\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_width(colarray[5]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colwidth6\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_width(colarray[6]));
    zcpy(&zprefs,1,buffer);

    GtkTreeViewColumn * col;
    col=gtk_tree_view_get_column(trunk,0);
    zcpy(&zprefs,1,"colpos1\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);
    col=gtk_tree_view_get_column(trunk,1);
    zcpy(&zprefs,1,"colpos2\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);
    col=gtk_tree_view_get_column(trunk,2);
    zcpy(&zprefs,1,"colpos3\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);
    col=gtk_tree_view_get_column(trunk,3);
    zcpy(&zprefs,1,"colpos4\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);
    col=gtk_tree_view_get_column(trunk,4);
    zcpy(&zprefs,1,"colpos5\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);
    col=gtk_tree_view_get_column(trunk,5);
    zcpy(&zprefs,1,"colpos6\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);
    col=gtk_tree_view_get_column(trunk,6);
    zcpy(&zprefs,1,"colpos7\x1b");
    for(int x=1;x<=7;x++)
    {if(colarray[x]==col){sprintf(buffer,"%i\n",x);}}
    zcpy(&zprefs,1,buffer);

    zcpy(&zprefs,1,"colvis1\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_visible(colarray[1]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colvis2\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_visible(colarray[2]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colvis3\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_visible(colarray[3]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colvis4\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_visible(colarray[4]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colvis5\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_visible(colarray[5]));
    zcpy(&zprefs,1,buffer);
    zcpy(&zprefs,1,"colvis6\x1b");
    sprintf(buffer,"%i\n",gtk_tree_view_column_get_visible(colarray[6]));
    zcpy(&zprefs,1,buffer);



    zcpy(&zprefs,1,"tfvals\x1b");
    buffer[0]=0;

    GtkTreeIter iter;


    int tfl=10;

    if(gtk_tree_model_get_iter_first(tfmodel,&iter))
    {
        do
        {
            gchar * tfname;
            gtk_tree_model_get(tfmodel,&iter,0,&tfname,-1);
            zcpy(&zprefs,1,"|");
            zcpy(&zprefs,1,tfname);
            g_free(tfname);
        } while(gtk_tree_model_iter_next(tfmodel,&iter) && tfl--);
    }
    zcpy(&zprefs,1,"\n");



    zcpy(&zprefs,1,"tftext\x1b");
    GtkWidget * combotftext = gtk_bin_get_child(combotf);
    gchar * tagtext=gtk_entry_get_text(GTK_ENTRY(combotftext));
    zcpy(&zprefs,1,tagtext);
    zcpy(&zprefs,1,"\n");

    zcpy(&zprefs,1,"csels\x1b");
    buffer[0]=0;

    GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_STORE(cview));
    if (gtk_tree_selection_count_selected_rows(sel))
    {
        GList * list = gtk_tree_selection_get_selected_rows(sel,&clist);
        GtkTreeIter iter;
        GList *item;

		for (item = list; item != NULL; item = g_list_next(item))
		{
		    gchar * cname=NULL;
		    GtkTreePath *treepath = item->data;
		    gtk_tree_model_get_iter(clist,&iter,treepath);
            gtk_tree_model_get(clist,&iter,0,&cname,-1);
            zcpy(&zprefs,1,"|");
            zcpy(&zprefs,1,cname);
            zcpy(&zprefs,1,"|");
            g_free(cname);

		}
        g_list_free(list);
    }
    zcpy(&zprefs,1,"\n");


    zcpy(&zprefs,1,"sels\x1b");
    GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    gtk_tree_selection_selected_foreach(sel1, storesel, &zprefs);


    char arg1[200];
    char exepath[1024] = {0};
    sprintf( arg1, "/proc/%d/exe", getpid() );
    readlink( arg1, exepath, 1024 );
    char inipath[1000]={};
    strcat(inipath,exepath);
    strrchr(inipath,'/')[0]=0;
    strcat(inipath,"/finitodo.ini");

    FILE * fp;
    fp=fopen(inipath,"w+");
    if (!fp)
    {
        const char *homedir;
        homedir = getenv("HOME");
        strcpy(inipath,homedir);
        strcat(inipath,"/.config");
        int result = mkdir(inipath, 0777);
        strcat(inipath,"/finitodo");
        result = mkdir(inipath, 0777);
        strcat(inipath,"/finitodo.ini");
        fp=fopen(inipath,"w+");
    }

    if (fp==NULL){AlertMessage(NULL,"Error writing to settings file");return;}
    fputs(zprefs,fp);
    fclose(fp);
    free(zprefs);
    return;
}




void selnone(int i)
{

    gtk_toggle_button_set_active(sdb,FALSE);
    gtk_toggle_button_set_active(ddb,FALSE);
    gtk_entry_set_text(tentry,"(none)");
    gtk_entry_set_text(pentrybox,"0");
    gtk_entry_set_text(sdentry,"(none)");
    gtk_entry_set_text(ddentry,"(none)");
    gtk_label_set_text(recsuffix,"");
    GtkTextBuffer * vbuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (notesview));
    gtk_text_buffer_set_text(vbuffer,"",-1);
    gtk_entry_set_text(rentry,"1");

    gtk_combo_box_set_active(comborty,0);
    gtk_combo_box_set_active(comboper,-1);
    gtk_combo_box_set_active(combow,-1);
    gtk_combo_box_set_active(comborba,-1);
    GtkWidget * tagorytext = gtk_bin_get_child(tagory);
    gtk_entry_set_text(tagorytext,"");
    gtk_widget_hide(hboxrec1);
    gtk_widget_hide(hboxdays);
    gtk_widget_hide(hboxmonth);
    gtk_widget_hide(comborba);
    gtk_widget_hide(labelrba);

    gtk_toggle_button_set_active(tib,FALSE);
    gtk_widget_hide(donelabel);

    togrem(NULL,0);

    gtk_entry_set_text(wdtext,"");
    for(int x=1;x<8;x++)
    {gtk_toggle_button_set_active(weekday[x],FALSE);}
    gtk_widget_set_sensitive(gf2,FALSE);
}


gboolean tibpressed(GtkWidget * g, int i)
{
    if(!gtk_widget_is_focus(g)){tpress=1;}
    return FALSE;
}


gboolean tibforward(GtkWidget * g, int i)
{
    if(tpress){tpress=0;return FALSE;}
    tpress=0;
    gtk_widget_grab_focus(tentry);
    return FALSE;
}


void tentrymulti(GtkWidget * g, int i)
{
    gchar * gc=gtk_entry_get_text(g);
    if(!strcmp(gc,"(multi-selection)"))
    {gtk_entry_set_text(g,"");}
    GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(g));
}

void entryunhighlight(GtkWidget * g)
{
    gtk_editable_select_region(GTK_EDITABLE(g), 0, 0);
    GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(g));
    GtkTextIter start, end;
    gtk_text_buffer_get_start_iter(buf, &start);
    gtk_text_buffer_get_end_iter(buf, &end);
    gtk_text_buffer_select_range(buf, &start, &start);

}


gboolean checksibs(GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gboolean trypar;
    GtkTreeIter riter;
    GtkTreeIter pariter;

    GtkTreePath * true_path = gtk_tree_model_filter_convert_child_path_to_path(GTK_TREE_MODEL_FILTER (gtf),path);
    gtk_tree_model_get_iter(maintreestore, &riter, true_path);
    gtk_tree_path_free(true_path);

    trypar=gtk_tree_model_iter_parent(maintreestore,&pariter,iter);
    if(!trypar){return;}

    GtkTreeIter child;
    gtk_tree_model_iter_children(model,&child,&pariter);

    int allcomp=1;gdouble fda;
    do
        {
            gtk_tree_model_get(model,&child,col_donedateval,&fda,-1);
            if(!fda){allcomp=0;break;}
        } while(gtk_tree_model_iter_next(model,&child));



    if(allcomp)
    {
        gtk_tree_model_get(model,&pariter,col_donedateval,&fda,-1);
        if((time_t)fda>0 || (time_t)fda<-1 ){return FALSE;}
        GtkTreePath * ppath=gtk_tree_model_get_path(model,&pariter);
        holdoff=1;completerecur(maintreestore,ppath,&pariter,NULL);holdoff=0;
        gtk_tree_path_free(ppath);
        colorstatus(&pariter);
        return TRUE;
    }
    return FALSE;

}


gboolean completerecur(GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    nosort=1;
    gint muid,rty,rfe,rba,rperiod,rperiodday,aldone;
    gdouble dda,cda,sda,fda,oda,ada;
    gchar * wdays=NULL;
    time_t ttt=0;
    time_t newdate,olddate;
    time_t tnow=time(0);
    struct tm tno=*localtime(&tnow);
    tno.tm_isdst=0;
    tno.tm_hour=0;
    tno.tm_min=0;
    tno.tm_sec=1;
    tnow=mktime(&tno);

    GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    int srows=gtk_tree_selection_count_selected_rows(sel);
    gtk_tree_model_get(model,iter,11,&muid,14,&rty,15,&rfe,16,&rba,7,&sda,8,&dda,17,&wdays,col_periodval,&rperiod,col_perioddayval,&rperiodday,col_donedateval,&fda,col_sourceduedateval,&oda,col_alarmdate,&ada,col_alarmdone,&aldone,-1);

    olddate=(time_t)dda;
    int fakedue=0;
    if(sda && !dda){olddate=(time_t)sda;fakedue=1;}
    time_t olderdate=olddate;
    int ensurefuture = rba ==1 ? 0 : 1;
    char buf[1000];


    if(fda)
    {
        time_t nfda;
        if(fda>0){nfda=0;if(!rty){rty=-1;}}
        else if(fda<-1){nfda=-1;if(!rty){rty=-1;}}
        else if(fda==-1){nfda= rty ? -1 : -1 * tnow;if(!rty){rty=-1;};}
        gtk_tree_store_set(model, iter,col_donedateval,(gdouble)nfda,-1);
        fda=nfda;ttt=nfda;
    }

    if (rty==0)
    {
        gtk_tree_store_set(model, iter,col_donedateval,(gdouble)tnow,-1);
        gtk_tree_store_set(model, iter,col_statustext,"Done",-1);
        ttt=time(0);fda=(gdouble)ttt;
    }

     if (rty==1)
     {
         const time_t ONE_DAY = 24 * 60 * 60 ;
         newdate = (rba == 2 ? tnow : olddate);
         do{newdate += (rfe * ONE_DAY) ;}while (newdate <= tnow && ensurefuture);
     }

     if (rty==2)
     {
         const time_t ONE_WEEK = 24 * 60 * 60 * 7 ;
         newdate = (rba == 2 ? tnow : olddate);
         do{newdate += (rfe * ONE_WEEK) ;} while (newdate <= tnow && ensurefuture);
     }

     if (rty==3)
     {
         newdate = (rba == 2 ? tnow : olddate);
         time_t origdate=newdate;
         struct tm nd = *localtime(&origdate) ;
         int origday=nd.tm_mday;
         nd.tm_year+=((int)(rfe/12));

         int inc=rfe%12,round=0;

         do {
                round++;
                nd = *localtime(&origdate);
                nd.tm_mon+=(inc * round);
                newdate=mktime(&nd);
                struct tm ndt = *localtime(&newdate) ;
            } while (newdate <= tnow && ensurefuture);
         nd = *localtime(&newdate) ;


         int resmonth=nd.tm_mon;

         while (nd.tm_mday!=origday)
         {
             newdate-=(60*60*24);
             struct tm resdate=*localtime(&newdate);
             if(resdate.tm_mon!=resmonth){break;}
         }
         int initialday;
         time_t sourcedate=(time_t)oda;
         if(rba==2){sourcedate=tnow;}
         struct tm od=*localtime(&sourcedate);
         initialday=od.tm_mday;

         nd = *localtime(&newdate);
         int nmonth=nd.tm_mon;
         nd.tm_mday=initialday;

         time_t testdate=mktime(&nd);
         struct tm testd = *localtime(&testdate) ;
         if(testd.tm_mon==nmonth)
         {newdate=testdate;}


     }


     if (rty==4)
     {
         newdate = (rba == 2 ? tnow : olddate);
         olddate=newdate;
         struct tm nd = *localtime(&newdate) ;
         struct tm ndo = *localtime(&olddate) ;
         do {
                nd.tm_year+=rfe;
                newdate=mktime(&nd);
                struct tm resdate= *localtime(&newdate) ;
                while (resdate.tm_mon !=ndo.tm_mon)
                {
                    newdate-=(60*60*24);
                    resdate= *localtime(&newdate) ;
                }

            } while (newdate <= tnow && ensurefuture);
     }


     if (rty==5)
     {
         if(!wdays || !strlen(wdays) || !strcmp(wdays,"|0|0|0|0|0|0|0")){return;}
         const time_t ONE_DAY = 24 * 60 * 60;
         newdate = (rba == 2 ? tnow : olddate);
         while(1)
         {
             newdate += ONE_DAY;
             struct tm nd = *localtime(&newdate);
             char c[100];
             if(nd.tm_wday==0){nd.tm_wday=7;}
             sprintf(c,"%i",nd.tm_wday);
             int cv=(int)c[0];
             int p=strchr(wdays,cv);
             time_t tn=time(0);
             if(strchr(wdays,cv) && !ensurefuture){break;}
             if(strchr(wdays,cv) && ensurefuture && (newdate > tnow)){break;}
         }
     }

     if(rty==6)
         {
             newdate = olddate;
             struct tm nd = *localtime(&newdate);
             int targday=(int)rperiodday;
             targday++;if(targday==7){targday==0;}
             int targper=(int)rperiodday;
             int tinc=(targper<=3 ? 1 : -1);
             do
             {
                nd.tm_mon++;
                nd.tm_mday=1;
                if(targper>3)
                {
                    nd.tm_mon++;
                    newdate=mktime(&nd);
                    newdate-=(60*60*24);
                    nd = *localtime(&newdate);
                }
                newdate=mktime(&nd);
                int tally=0,tallytarg=targper+1;
                if(targper==5){tallytarg=1;}
                if(targper==4){tallytarg=2;}
                int testr=30;
                do
                {
                    if(nd.tm_wday==targday){tally++;}
                    if(tally==tallytarg){break;}
                    newdate+=(60*60*24*tinc);
                    nd = *localtime(&newdate);

                } while(1);

            } while (newdate <= tnow && ensurefuture);
         }



        if(rty>0)
        {
             noscroll=0;if(fhidefuture && !fda && (!sda || sda<=tnow)){noscroll=1;}
             struct tm ndate = *localtime(&newdate);
             ndate.tm_hour=0;ndate.tm_min=0;ndate.tm_sec=1;
             ndate.tm_isdst=0;
             newdate=mktime(&ndate);dda=(gdouble)newdate;
             strftime(buf,99,"%Y-%m-%d",&ndate);
             if(srows==1 && !fakedue){gtk_entry_set_text(ddentry,buf);}
             else if(srows==1 && fakedue){gtk_entry_set_text(sdentry,buf);}
             else
             {
                     GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
                     onSelectionChanged(sel,1);
             }
             strftime(buf,99,"%d %b %Y (%a)",&ndate);
             //strftime(buf,99,"%d %b %Y",&ndate);
             if(!fakedue){gtk_tree_store_set(model,iter,8,dda,3,buf,-1);}
             if(sda)
             {
                 time_t tdiff=newdate-olderdate;
                 sda+=(gdouble)tdiff;
                 if(fakedue){sda=(gdouble)newdate;}
                 time_t nsdate=(time_t)sda;
                 ndate = *localtime(&nsdate);
                 strftime(buf,99,"%Y-%m-%d",&ndate);
                 if(srows==1){gtk_entry_set_text(sdentry,buf);}
                 else
                 {
                         GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
                         onSelectionChanged(sel,1);
                 }
                 strftime(buf,99,"%d %b %Y (%a)",&ndate);
                 //strftime(buf,99,"%d %b %Y",&ndate);
                 gtk_tree_store_set(model,iter,7,(gdouble)sda,2,buf,-1);
                 immchange(NULL,NULL);

             }
             if(ada)
             {
                 time_t tdiff=newdate-olderdate;
                 ada+=(gdouble)tdiff;
                 time_t nsdate=(time_t)ada;
                 ndate = *localtime(&nsdate);
                 strftime(buf,99,"%Y-%m-%d",&ndate);
                 if(srows==1){gtk_entry_set_text(adentry,buf);}
                 else
                 {
                         GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
                         onSelectionChanged(sel,1);
                 }
                 gtk_tree_store_set(model,iter,col_alarmdate,(gdouble)ada,col_alarmdone,0,-1);
                 aldone=0;
                 immchange(NULL,NULL);
             }

             if(fakedue){dda=0;}
             char fstatus[100];
             figstatus(fstatus,(time_t)sda,(time_t)dda,(time_t)fda);
             gtk_tree_store_set(model,iter,col_statustext,(gchar*)fstatus,-1);
             GtkTreeIter child;
             if(gtk_tree_model_iter_children(model,&child,iter))
             {donesubs(model,&child);gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);}

             noscroll=0;

        }
        gboolean cs=0;
        char fstatus[100];
        figstatus(fstatus,(time_t)sda,(time_t)dda,(time_t)fda);
        gtk_tree_store_set(model,iter,col_statustext,(gchar*)fstatus,-1);

        sprintf(buf,"update tasks set duedate=%lld,donedate=%lld,startdate=%lld,alarmdate=%lld,alarmdone=%i where UID=%i;",(time_t)dda,(time_t)ttt,(time_t)sda,(time_t)ada,(int)aldone,(int)muid);
        zcpy(&dbz,1,buf);



        if(!fda && (!sda || sda<=tnow))
        {
            GtkTreePath * gp=gtk_tree_model_get_path(maintreestore,iter);
            gtk_tree_view_scroll_to_cell(trunk,gp,NULL,TRUE,0.5,0);
            gtk_tree_path_free(gp);
        }
        else if (gtk_toggle_button_get_active(comparent))
        {cs=checksibs(model,path,iter,0);}

    gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
    rfilter();

    g_free(wdays);

    nosort=1;
    supdate();
    nosort=0;
    GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
    gtk_tree_sortable_set_sort_column_id(sortable, 99, GTK_SORT_DESCENDING);
    gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);

    return FALSE;

}





void togrem(GtkWidget * g,int i)
{
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g)) || i)
      {
          gtk_widget_set_sensitive(adentry,TRUE);
          gtk_widget_set_sensitive(adp,TRUE);
          gtk_widget_set_sensitive(ahour,TRUE);
          gtk_widget_set_sensitive(aminute,TRUE);
          gtk_toggle_button_set_active(adb,TRUE);
          gtk_widget_show(adentry);
          gtk_widget_show(ahour);
          gtk_widget_show(aminute);
          gtk_widget_show(ampmlabel);
          gtk_widget_show(colabel);
          gtk_widget_hide(nonelabel);
          gtk_widget_show(adp);
          if (!gtk_entry_get_text_length(adentry) && gtk_entry_get_text_length(ddentry))
          {
              gtk_entry_set_text(adentry,gtk_entry_get_text(ddentry));
              gtk_spin_button_set_value(ahour,14);
              gtk_spin_button_set_value(aminute,30);
          }
          else if (!gtk_entry_get_text_length(adentry) && gtk_entry_get_text_length(sdentry))
          {
              gtk_entry_set_text(adentry,gtk_entry_get_text(sdentry));
              gtk_spin_button_set_value(ahour,14);
              gtk_spin_button_set_value(aminute,30);
          }
          else if (!gtk_entry_get_text_length(adentry))
          {
              time_t curdate=time(0);
              struct tm cdate=*localtime(&curdate);
              char buf[100]={};
              strftime(buf,99,"%Y-%m-%d",&cdate);
              gtk_entry_set_text(adentry,buf);
              gtk_spin_button_set_value(ahour,14);
              gtk_spin_button_set_value(aminute,30);
          }
      }
      else
      {
            gtk_widget_set_sensitive(adentry,FALSE);
            gtk_widget_set_sensitive(adp,FALSE);
            gtk_widget_set_sensitive(ahour,FALSE);
            gtk_widget_set_sensitive(aminute,FALSE);
            gtk_toggle_button_set_active(adb,FALSE);
            gtk_widget_hide(adentry);
            gtk_widget_hide(ahour);
            gtk_widget_hide(aminute);
            gtk_widget_hide(ampmlabel);
            gtk_widget_hide(colabel);
            gtk_widget_hide(adp);
            gtk_widget_show(nonelabel);
            gtk_spin_button_set_value(ahour,0);
            gtk_spin_button_set_value(aminute,0);
            gtk_label_set_text(ampmlabel,"--:--");
            gtk_entry_set_text(adentry,"");
      }
}




void togdue(GtkWidget * g)
{
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g)))
      {
          gtk_widget_set_sensitive(ddentry,TRUE);
          gtk_widget_set_sensitive(ddp,TRUE);
          if (!gtk_entry_get_text_length(ddentry))
          {
              time_t curdate=time(0);
              struct tm cdate=*localtime(&curdate);
              char buf[100]={};
              strftime(buf,99,"%Y-%m-%d",&cdate);
              gtk_entry_set_text(ddentry,buf);
          }
      }
      else
      {
            gtk_widget_set_sensitive(ddentry,FALSE);
            gtk_widget_set_sensitive(ddp,FALSE);
            gtk_entry_set_text(ddentry,"");
      }
}


void togsta(GtkWidget * g)
{
      if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(g)))
      {
        gtk_widget_set_sensitive(sdentry,TRUE);
        gtk_widget_set_sensitive(sdp,TRUE);
        if (!gtk_entry_get_text_length(sdentry))
        {
          time_t curdate=time(0);
          struct tm cdate=*localtime(&curdate);
          char buf[100]={};
          strftime(buf,255,"%Y-%m-%d",&cdate);
          gtk_entry_set_text(sdentry,buf);
        }
      }
      else
      {
            gtk_widget_set_sensitive(sdentry,FALSE);
            gtk_widget_set_sensitive(sdp,FALSE);
            gtk_entry_set_text(sdentry,"");
      }
}



void ctask(GtkWidget * g, int i)
{
        sprintf(ubuf,"|%lld|",tagory);
        if(strstr(ceffect,ubuf))
        {
            valtagory=gtk_combo_box_text_get_active_text(tagory);
            zcpy(&dbvalt,0,valtagory);
            valt=1;
        }

        sprintf(ubuf,"|notes|");
        if(strstr(ceffect,ubuf))
        {
            GtkTextIter start, end;
            GtkTextBuffer * vbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (notesview));
            gtk_text_buffer_get_bounds (vbuffer, &start, &end);
            gchar * gcv = gtk_text_buffer_get_text(vbuffer,&start,&end,FALSE);
            valnotes=(char*)gcv;
            zcpy(&dbnotes,0,valnotes);
            vnotes=1;
        }




        GList * rr_list = NULL;
        GtkTreeSelection * sel = gtk_tree_view_get_selection((GTK_TREE_VIEW(trunk)));
        GtkTreeModel * gtm=gtk_tree_view_get_model(trunk);
        GList * list = gtk_tree_selection_get_selected_rows(sel,&gtm);
        GtkTreeIter iter;
        GList *item;

		for (item = list; item != NULL; item = g_list_next(item))
		{
		    GtkTreePath *treepath = item->data;
		    GtkTreeRowReference  *rowref;
		    rowref = gtk_tree_row_reference_new(gtm, treepath);
		    rr_list= g_list_append(rr_list, rowref);
            gchar * tempt;
            GtkTreeIter  iter;
            gtk_tree_model_get_iter(gtm, &iter, treepath);
            gtk_tree_model_get(gtm,&iter,0,&tempt,-1);
            g_free(tempt);
           }

     pthread_mutex_lock(&zlock);
     zcpy(&dbz,1,"BEGIN;");

     GList *node;

     for ( node = rr_list;  node != NULL;  node = node->next )
     {
        GtkTreePath *path;
        gchar * tempt;
        path = gtk_tree_row_reference_get_path((GtkTreeRowReference*)node->data);
        GtkTreeIter  iter;
        GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path (GTK_TREE_MODEL_FILTER (gtm),path);
        gtk_tree_model_get_iter(maintreestore, &iter, true_path);
        gtk_tree_model_get(maintreestore,&iter,0,&tempt,-1);
        gtk_tree_model_get(maintreestore,&iter,0,&tempt,-1);
        if(i==0){holdoff=1;delling=1;completerecur(maintreestore,true_path,&iter,NULL);holdoff=0;delling=0;}
        else if(i==1){immupdate(maintreestore,path,&iter,NULL);}
        colorstatus(&iter);
        gtk_tree_path_free(path);
        gtk_tree_path_free(true_path);
        g_free(tempt);
     }
     zcpy(&dbz,1,"COMMIT;");
     pthread_mutex_unlock(&zlock);
     if(i==0){delling=0;GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));onSelectionChanged(sel,0);delling=0;}

     g_list_free(list);
     g_list_foreach(rr_list, (GFunc) gtk_tree_row_reference_free, NULL);
     g_list_free(rr_list);

     free(dbvalt);dbvalt=NULL;
     free(dbnotes);dbnotes=NULL;
     valt=0;vnotes=0;
    if(!i){gtk_widget_grab_focus(trunk);}
}


void checkdelsubs(sqlite3_stmt * s, sqlite3 * sgp, int pa)
{
    pthread_mutex_lock(&lock);
    sqlite3_reset(s);
    int rp=0,rt=0,c=0;
    char buf[1000]={};
    char cbuf[1000]={};

    while (SQLITE_ROW==sqlite3_step(s))
    {
       if(rt++<rp) {continue;}
       rp++;
       if(pa==sqlite3_column_int(s,dbcol_parent) || pa==-1)
       {
            sprintf(buf," or UID=%i",sqlite3_column_int(s,dbcol_uid));
            zcpy(&dbz,1,buf);
            if(pa!=-1){rt=0;checkdelsubs(s,sgp,sqlite3_column_int(s,dbcol_uid));rt=0;}
       }
    }
    pthread_mutex_unlock(&lock);
}




void deltask(GtkWidget * g,int i)
{
        pthread_mutex_lock(&lock);
        GList * rr_list = NULL;
        GtkTreeSelection * sel = gtk_tree_view_get_selection((GTK_TREE_VIEW(trunk)));
        GtkTreeModel * gtm=gtk_tree_view_get_model(trunk);
        GList * list = gtk_tree_selection_get_selected_rows(sel,&gtm);
        GtkTreeIter iter;
        GList *item;

        pthread_mutex_lock(&zlock);
        zcpy(&dbz,0,"BEGIN;delete from tasks where UID=-1");
        int c=sqlite3_open(curdatfile,&sgp);
        sqlite3_stmt * sst;
        sqlite3_prepare_v2(sgp,"select * from tasks",-1,&sst,NULL);

		for (item = list; item != NULL; item = g_list_next(item))
		{
		    GtkTreePath *treepath = item->data;
		    GtkTreeRowReference  *rowref;
		    rowref = gtk_tree_row_reference_new(gtm, treepath);
		    rr_list= g_list_append(rr_list, rowref);
		    GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path (GTK_TREE_MODEL_FILTER (gtm),treepath);
		    GtkTreeIter riter;
		    gint duid;
            gtk_tree_model_get_iter(maintreestore, &riter, true_path);
            gtk_tree_model_get(maintreestore,&riter,col_uid,&duid,-1);
            gtk_tree_path_free(true_path);
            char buf[100];
            sprintf(buf," or UID=%i",(int)duid);
            zcpy(&dbz,1,buf);
            checkdelsubs(sst,sgp,(int)duid);
        }
            sqlite3_finalize(sst);
          sqlite3_close(sgp);

     delling=1;

     GList *node;

     for ( node = rr_list;  node != NULL;  node = node->next )
     {
        GtkTreePath *path;
        path = gtk_tree_row_reference_get_path((GtkTreeRowReference*)node->data);
        GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path (GTK_TREE_MODEL_FILTER (gtm),path);

        if(true_path)
        {
            GtkTreeIter  iter;
            if(gtk_tree_model_get_iter(maintreestore, &iter, true_path))
            {
                gtk_tree_store_remove(maintreestore, &iter);
            }
        }
        gtk_tree_path_free(path);
        gtk_tree_path_free(true_path);

     }

     zcpy(&dbz,1,";COMMIT;");
     pthread_mutex_unlock(&zlock);
     dbupdate();
     delling=0;

     g_list_foreach(rr_list, gtk_tree_row_reference_free, NULL);
     g_list_free(rr_list);
     g_list_free(list);

     GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
     gtk_tree_selection_unselect_all(treeselection);
     selnone(0);

    pthread_mutex_unlock(&lock);
}


gboolean newsub (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gint nuid = GPOINTER_TO_INT(userdata);
    GtkTreeIter riter,iiter;

    GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path(GTK_TREE_MODEL_FILTER (gtf),path);
    gtk_tree_model_get_iter(maintreestore, &riter, true_path);
    gtk_tree_path_free(true_path);

    char cbuffer[100]={};
    strcpy(cbuffer,"");
    gchar * partag=NULL;

    if(iter){gtk_tree_store_append(maintreestore, &iiter,&riter);gtk_tree_model_get(maintreestore,&riter,col_tags,&partag,-1);strcpy(cbuffer,partag);}
    else{gtk_tree_store_append(maintreestore, &iiter,0);}

    gtk_tree_store_set(maintreestore, &iiter,0,"New Task",11,(gint)nuid,13,"Active",col_tags,cbuffer,col_recurfreqval,1,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);

    gtk_tree_view_expand_row(trunk,path,FALSE);
    GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    gtk_tree_selection_unselect_all(treeselection);
    GtkTreePath *streepath = gtk_tree_model_get_path(maintreestore,&iiter);
    utruepath = gtk_tree_model_filter_convert_child_path_to_path(GTK_TREE_MODEL_FILTER (gtf),streepath);
    gtk_tree_path_free(streepath);
    if(partag){g_free(partag);}
    return FALSE;


}

gboolean newsib (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gint nuid = GPOINTER_TO_INT(userdata);
    if (!nuid){iter=NULL;}
    GtkTreeIter riter,iiter;

    char cbuffer[100]={};
    strcpy(cbuffer,"");
    gchar * partag=NULL;

    GtkTreeIter * pariter1;
    gtk_tree_model_iter_parent(maintreestore,pariter1,iter);

    GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path(GTK_TREE_MODEL_FILTER (gtf),path);
    gtk_tree_model_get_iter(maintreestore, &riter, true_path);
    gtk_tree_path_free(true_path);

    GtkTreeIter pariter;
    gboolean trypar=gtk_tree_model_iter_parent(maintreestore,&pariter,&riter);

    gtk_tree_model_get(maintreestore,&riter,col_tags,&partag,-1);strcpy(cbuffer,partag);

    if(trypar){gtk_tree_store_append(maintreestore, &iiter,&pariter);}
    else
    {
        gtk_toggle_button_set_active(tf1,TRUE);
        GtkWidget * combotftext = gtk_bin_get_child(combotf);
        gtk_entry_set_text(combotftext,"");
        gtk_tree_model_filter_refilter(gtf);
        gtk_tree_store_append(maintreestore, &iiter,0);
    }

    gtk_tree_store_set(maintreestore, &iiter,0,"New Task",11,(gint)nuid,13,"Active",col_recurfreqval,1,col_tags,cbuffer,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);

    gtk_tree_view_expand_row(trunk,path,FALSE);
    GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    gtk_tree_selection_unselect_all(treeselection);

    GtkTreePath *streepath = gtk_tree_model_get_path(maintreestore,&iiter);
    utruepath = gtk_tree_model_filter_convert_child_path_to_path(GTK_TREE_MODEL_FILTER (gtf),streepath);
    gtk_tree_path_free(streepath);

    if(partag){g_free(partag);}
    return FALSE;

}



void checkduppa(sqlite3_stmt * s, sqlite3 * sgp, int pa,time_t cdate)
{
    pthread_mutex_lock(&lock);
    static time_t plusdate=1;
    sqlite3_reset(s);
    int rp=0,rt=0,c=0;
    char buf[1000]={};
    char cbuf[1000]={};

    while (SQLITE_ROW==sqlite3_step(s))
    {
       if(rt++<rp) {continue;}
       rp++;
       if(pa==sqlite3_column_int(s,1) || pa==-1)
       {
           strcpy(dbi,"insert into tesks select * from tasks where UID=");
           sprintf(cbuf,"%i;",sqlite3_column_int(s,0));
           strcat(dbi,cbuf);
           sprintf(cbuf,"update tesks set creationdate=%lld where UID=%i;",(cdate+plusdate++),sqlite3_column_int(s,0));
           strcat(dbi,cbuf);

           c=sqlite3_exec(sgp,dbi,NULL,NULL,NULL);
           if(pa!=-1){rt=0;checkduppa(s,sgp,sqlite3_column_int(s,0),cdate);rt=0;}

       }
    }
    pthread_mutex_unlock(&lock);
}


void duptask3(int duid,char * nbuf,int creinc)
{
    pthread_mutex_lock(&lock);
    tdbupdate();
    if(!strlen(curdatfile)){AlertMessage(NULL,"No currently loaded task data file. Please use the Open File option to open an existing task data file or create a new one.");pthread_mutex_unlock(&lock);return;}


    char dbuf[110];
    sqlite3 * sgp;
    int c=sqlite3_open(curdatfile,&sgp);
    time_t curdate=time(0);
    curdate+=creinc;
    char cbuf[1000];
    char buf[1000];


    sqlite3_stmt * sst;
    sprintf(buf,"%i",(int)duid);
    strcpy(dbi,"select parent from tasks where UID=");
    strcat(dbi,buf);
    c=sqlite3_prepare_v2(sgp,dbi,-1,&sst,NULL);
    sqlite3_step(sst);
    int pa=sqlite3_column_int(sst,0);
    sqlite3_finalize(sst);



    strcpy(dbi,"BEGIN;");
    strcat(dbi,"CREATE TABLE tesks as SELECT * FROM tasks WHERE UID=-1;");
    strcat(dbi,"insert into tesks SELECT * FROM tasks WHERE UID=");
    sprintf(cbuf,"%i;",(int)duid);
    strcat(dbi,cbuf);
    strcat(dbi,"update tesks set title=title || ' (copy)',creationdate=");
    sprintf(cbuf,"%lld;",curdate);
    strcat(dbi,cbuf);
    c=sqlite3_exec(sgp,dbi,NULL,NULL,NULL);

    sqlite3_prepare_v2(sgp,"select * from tasks",-1,&sst,NULL);
    checkduppa(sst,sgp,(int)duid,curdate);
    sqlite3_finalize(sst);

    c=sqlite3_exec(sgp,"CREATE TABLE tisks as SELECT * FROM tesks;",NULL,NULL,NULL);
    c=sqlite3_exec(sgp,"update tisks set UID=NULL;",NULL,NULL,NULL);
    c=sqlite3_exec(sgp,"insert into tasks select * from tisks;",NULL,NULL,NULL);


    sqlite3_prepare_v2(sgp,"select * from tesks",-1,&sst,NULL);
    sqlite3_stmt * ssv;
    zcpy(&dbd,0,"");

    while (SQLITE_ROW==sqlite3_step(sst))
    {
        int olduid=sqlite3_column_int(sst,dbcol_uid);
        time_t cdate=sqlite3_column_double(sst,dbcol_creationdateval);
        strcpy(dbi,"select * from tasks where creationdate=");
        sprintf(buf,"%lld",cdate);
        strcat(dbi,buf);
        sqlite3_prepare_v2(sgp,dbi,-1,&ssv,NULL);
        sqlite3_step(ssv);
        int newuid=sqlite3_column_int(ssv,dbcol_uid);
        sprintf(buf,"update tesks set parent=%i where parent=%i;",newuid,olduid);
        zcpy(&dbd,1,buf);
        sqlite3_finalize(ssv);
    }
    c=sqlite3_exec(sgp,dbd,NULL,NULL,NULL);

    sqlite3_reset(sst);
    zcpy(&dbd,0,"");
    while (SQLITE_ROW==sqlite3_step(sst))
    {
        time_t cdate=sqlite3_column_double(sst,dbcol_creationdateval);
        sprintf(buf,"update tasks set parent=%i where creationdate=%lld;",sqlite3_column_int(sst,dbcol_parent),cdate);
        zcpy(&dbd,1,buf);
    }
    sqlite3_finalize(sst);
    c=sqlite3_exec(sgp,dbd,NULL,NULL,NULL);
    c=sqlite3_exec(sgp,"drop table tesks;drop table tisks;COMMIT;",NULL,NULL,NULL);



    gtk_widget_hide(swindow);
    gtk_tree_store_clear(maintreestore);
    sqlite3_prepare_v2(sgp,"select * from tasks",-1,&sst,NULL);
    zcpy(&uidlist,0,"select * from tasks where UID!=-1");
    checksst(sst,0,NULL);
    gtk_tree_model_foreach(gtf, checkxp, NULL);
    sqlite3_finalize(sst);
    sqlite3_prepare_v2(sgp,uidlist,-1,&sst,NULL);
    checksst(sst,-1,NULL);
    sqlite3_finalize(sst);
    gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
    rfilter();
    gtk_widget_show(swindow);


    strcpy(dbi,"select UID from tasks where creationdate=");
    strcat(dbi,cbuf);
    c=sqlite3_prepare_v2(sgp,dbi,-1,&sst,NULL);
    sqlite3_step(sst);
    int dupuid=sqlite3_column_int(sst,0);
    sqlite3_finalize(sst);

    char nbuffer[100];
    snprintf(nbuffer,99,"|%i",dupuid);
    zcpy(&nbuf,1,nbuffer);

    sqlite3_close(sgp);

    pthread_mutex_unlock(&lock);

}





void duptask2(GtkWidget * g,int i)
{
        ftext(labelt,20,"One moment please...");
        gtk_widget_queue_draw (labelt);
        struct timespec ts={};
        ts.tv_nsec=1000000;

        for(int uu=1;uu<30;uu++)
        {
            while (gtk_events_pending ())
            gtk_main_iteration ();
            nanosleep(&ts,NULL);
            //usleep(1000);
        }






    GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    char * tbuf=NULL;
    zcpy(&tbuf,0,"");
    gtk_tree_selection_selected_foreach(sel1, storesel, &tbuf);
    char * nbuf=NULL;
    zcpy(&nbuf,0,"");

    delling=1;

    int creinc=0;

    char * rbuf=tbuf;
    while(strchr(rbuf,'|'))
    {
        rbuf=strchr(rbuf,'|');
        rbuf++;
        int usel=atol(rbuf);
        creinc++;
         duptask3(usel,nbuf,creinc);
    }

    delling=0;
    rbuf=nbuf;
    while(strchr(rbuf,'|'))
    {
        rbuf=strchr(rbuf,'|');
        rbuf++;
        int usel=atol(rbuf);
        gtk_tree_model_foreach(gtf, resel, usel);
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        onSelectionChanged(sel,NULL);
           gtk_tree_view_scroll_to_cell(trunk,upath,NULL,TRUE,0.5,0);
    }


    ftext(labelt,20,"Task List");
    gtk_widget_grab_focus(trunk);

    free(tbuf);
    free(nbuf);
}




void newtask(GtkWidget * g,int lv)
{
    pthread_mutex_lock(&lock);
    if(!strlen(curdatfile)){AlertMessage(NULL,"No currently loaded task data file. Please use the Open File option to open an existing task data file or create a new one.");pthread_mutex_unlock(&lock);return;}
    GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    int gcr=gtk_tree_selection_count_selected_rows(sel);
    if(!gcr){lv=0;}


    GtkWidget * combotftext = gtk_bin_get_child(combotf);
    if(!lv || strlen(gtk_entry_get_text(GTK_ENTRY(combotftext))))
    {
        gtk_toggle_button_set_active(tf1,TRUE);
        GtkWidget * combotftext = gtk_bin_get_child(combotf);
        gtk_entry_set_text(combotftext,"");
        gtk_tree_model_filter_refilter(gtf);
    }

    char dbuf[110];
    sqlite3 * sgp;
    int c=sqlite3_open(curdatfile,&sgp);

    time_t curdate=time(0);
    char cbuf[1000];
    char buf[1000];

    gint gi = GPOINTER_TO_INT(g);
    sqlite3_stmt * sst;
    sprintf(buf,"%i",(int)seluid);
    strcpy(dbi,"select parent from tasks where UID=");
    strcat(dbi,buf);
    c=sqlite3_prepare_v2(sgp,dbi,-1,&sst,NULL);
    sqlite3_step(sst);
    int pa=sqlite3_column_int(sst,0);
    sqlite3_finalize(sst);


    strcpy(dbi,"INSERT INTO tasks ( title, tags,recurfreq,startdate, xp, creationdate, parent) VALUES ('New Task','',1,0,1,");
    sprintf(cbuf,"%lld",curdate);
    strcat(dbi,cbuf);
    sprintf(buf,",%i",((lv && (gcr==1)) ? (int)seluid : 0));
    if((lv==2) && (gcr==1)){sprintf(buf,",%i",pa);}

    strcat(dbi,buf);
    strcat(dbi,");");

    c=sqlite3_exec(sgp,dbi,NULL,NULL,NULL);
    if(c!=SQLITE_OK){AlertMessage(NULL,"Error adding new task");pthread_mutex_unlock(&lock);return;}
    strcpy(dbi,"select UID from tasks where creationdate=");
    strcat(dbi,cbuf);
    c=sqlite3_prepare_v2(sgp,dbi,-1,&sst,NULL);
    sqlite3_step(sst);
    int nuid=sqlite3_column_int(sst,0);
    sqlite3_finalize(sst);
    sqlite3_close(sgp);



    if ((lv==1) && (gcr==1))
    {
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_tree_selection_selected_foreach(sel, newsub, nuid);
        gtk_tree_selection_select_path(sel,utruepath);
        gtk_tree_view_set_cursor(trunk,utruepath,NULL,FALSE);
        gtk_tree_view_scroll_to_cell(trunk,utruepath,NULL,TRUE,0.5,0);
        immchange(tagory,NULL);
        gtk_widget_grab_focus (tentry);

    }
    else if ((lv==2) && (gcr==1))
    {
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_tree_selection_selected_foreach(sel, newsib, nuid);
        gtk_tree_selection_select_path(sel,utruepath);
        gtk_tree_view_set_cursor(trunk,utruepath,NULL,FALSE);
        gtk_tree_view_scroll_to_cell(trunk,utruepath,NULL,TRUE,0.5,0);
        immchange(tagory,0);
        gtk_widget_grab_focus (tentry);
    }

    else
    {
        newsub(gtf,NULL,NULL,nuid);
        GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_tree_selection_unselect_all(treeselection);
        gtk_tree_selection_select_path(sel,utruepath);
        gtk_tree_view_set_cursor(trunk,utruepath,NULL,FALSE);
        gtk_tree_view_scroll_to_cell(trunk,utruepath,NULL,TRUE,0.5,0);
        gtk_widget_grab_focus (tentry);
    }
    gtk_tree_path_free(utruepath);

    pthread_mutex_unlock(&lock);

}

gboolean newtaska(int i)
{
    newtask(NULL,i);
    return TRUE;
}

void togcomp()
{
    static int tlight=0;
    if(tlight){return;}
    tlight=1;
    if(gtk_widget_is_focus(trunk)){ctask(NULL,0);}
    tlight=0;
}


gboolean exrow_foreach (GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gtk_tree_view_expand_row(trunk,path,TRUE);
    return FALSE;
}


void exrow()
{
     GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
     gtk_tree_selection_selected_foreach(sel, exrow_foreach,NULL);
}


gboolean immupdate (GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    pthread_mutex_lock(&zlock);
    nosort=1;
    int suid;
    gdouble sda,dda,fda;
    time_t tnow=time(0);
    gdouble gdda;
    char buf[1024]={};
    char bufr[1024]={};
    char * ch;



    gtk_tree_model_get (model, iter,col_uid, &suid,col_donedateval,&fda,col_startdateval,&sda,col_duedateval,&dda,-1);
    zcpy(&dbz,1,"update tasks set UID = UID");


    ch=gtk_entry_get_text(tentry);
    sprintf(buf,"|%lld|",tentry);
    if(strstr(ceffect,buf))
    {
        memset(buf,0,sizeof(buf));
        strncpy(buf,ch,100);
        gtk_tree_store_set(maintreestore, iter,0,buf,-1);
        sfix(buf,100);
        zcpy(&dbz,1,",title='");
        zcpy(&dbz,1,buf);
        zcpy(&dbz,1,"'");
    }

    sprintf(buf,"|%lld|",tib);
    if(strstr(ceffect,buf))
    {
        time_t nfda = fda;
        if (gtk_toggle_button_get_active(tib))
        {
            if(fda>0){nfda=fda*-1;}
            else if(fda==0){nfda=-1;}
        }
        else
        {
            if(fda<-1){nfda=fda*-1;}
            else if(fda==-1){nfda=0;}
        }
        fda=(gdouble)nfda;
        gtk_tree_store_set(maintreestore, iter,col_donedateval,(gdouble)nfda,-1);
        sprintf(buf,",donedate=%lld",nfda);
        zcpy(&dbz,1,buf);
    }



    ch=gtk_entry_get_text(pentrybox);
    int newp=atol((char*)ch);

    sprintf(buf,"|%lld|",pentrybox);
    if(strstr(ceffect,buf))
    {
        sprintf(buf,"        %i",newp);
        gtk_tree_store_set(maintreestore, iter,col_priority,newp,col_pseudopritext,buf,-1);
        if(!newp){gtk_tree_store_set(maintreestore, iter,col_pseudopritext,"",-1);}
        sprintf(buf,",priority=%i",newp);
        zcpy(&dbz,1,buf);
    }

    ch=gtk_combo_box_text_get_active_text(tagory);
    sprintf(buf,"|%lld|",tagory);
    if(strstr(ceffect,buf) || valt)
    {
        if (!strcmp(dbvalt,"(None)")){strcpy(dbvalt,"");}
        gtk_tree_store_set(maintreestore, iter,col_priority,newp,col_tags,dbvalt,-1);
        repx(&dbvalt,"'","''");
        memset(curtagory,0,sizeof(curtagory));
        strncpy(curtagory,dbvalt,50);
        if (!strcmp(dbvalt,"(None)")){strcpy(curtagory,"");strcpy(dbvalt,"");}
        zcpy(&dbz,1,",tags='");
        zcpy(&dbz,1,dbvalt);
        zcpy(&dbz,1,"'");
        //free(dbvalt);dbvalt=NULL;
    }



    int altimechanged=0;
    char hbuf[100]={};
    sprintf(hbuf,"|%lld|",ahour);
    sprintf(buf,"|%lld|",aminute);
    if(strstr(ceffect,buf) || strstr(ceffect,hbuf))
    {
        int hourval=gtk_spin_button_get_value(ahour);
        int minuteval=gtk_spin_button_get_value(aminute);
        int ampm=0;
        if(hourval>12){hourval-=12;ampm=1;}
        if(hourval==12){ampm=1;}

        char buffer[100]={};
        sprintf(buffer,"%02i:%02i %s",hourval,minuteval,(ampm ? "pm" : "am" ));
        gtk_label_set_text(ampmlabel,buffer);
        altimechanged=1;

    }


    for (int x=0;x<2;x++)
    {
        sprintf(buf,"|%lld|",(!x ? ddentry : sdentry));
        if(strstr(ceffect,buf))
        {
            ch=gtk_entry_get_text(!x ? ddentry : sdentry);
            strcpy(buf,ch);
            if(!x && !strlen(buf))
            {
                char sdbuf[100];
                sprintf(sdbuf,"|%lld|",sdentry);
                strcat(ceffect,sdbuf);
            }

            char * savptr;
            char * token = strtok_r(buf, "-",&savptr);
            int df=3,yr=0,mh=0,dy=0;

            while( (token != NULL) && df)
            {
                if (df==3){yr=atol(token);}
                if (df==2){mh=atol(token);}
                if (df==1){dy=atol(token);}
                token = strtok_r(NULL, "-",&savptr);
                df--;
            }

            if(strlen(ch)==8 && !strstr(ch,"-"))
            {
                char sixdate[100]={};
                strcpy(sixdate,ch);
                sixdate[4]=0;
                yr=atol(&sixdate);
                strcpy(sixdate,ch);
                sixdate[6]=0;
                mh=atol(&sixdate[4]);
                strcpy(sixdate,ch);
                dy=atol(&sixdate[6]);
            }


            struct tm date = {} ;
            date.tm_year = yr - 1900 ;
            date.tm_mon = mh - 1 ;
            date.tm_mday = dy ;
            date.tm_hour=0;
            date.tm_min=0;
            date.tm_sec=1;
            struct tm dtest=*localtime(&tnow);
            time_t ndate=mktime(&date);
            date=*localtime(&ndate) ;
            strftime(buf,99,"%d %b %Y (%a)",&date);
            //strftime(buf,99,"%d %b %Y",&date);
            if (ndate<=0){ndate=0;sprintf(buf," ");}
            gtk_tree_store_set(maintreestore, iter,(!x ? col_duedatetext : col_startdatetext),buf,-1);
            gtk_tree_store_set(maintreestore, iter,(!x ? col_duedateval : col_startdateval),(gdouble)ndate,-1);

            if(!x){gtk_tree_store_set(maintreestore, iter,col_sourceduedateval,(gdouble)ndate,-1);}
            if(!x){sprintf(buf,",duedate=%lld,sourceduedate=%lld",ndate,ndate);dda=ndate;}
            else{sprintf(buf,",startdate=%lld",ndate);sda=ndate;}
            if(x && gtk_toggle_button_get_active(sdb) && !gtk_toggle_button_get_active(ddb))
            {
                gtk_tree_store_set(maintreestore, iter,col_sourceduedateval,(gdouble)ndate,-1);
                sprintf(buf,",startdate=%lld,sourceduedate=%lld",ndate,ndate);
            }
            colorstatus(iter);

            zcpy(&dbz,1,buf);

            GtkTreeIter pariter,*origiter;
            origiter=iter;
        }
    }
    char fstatus[100];
    figstatus(fstatus,(time_t)sda,(time_t)dda,(time_t)fda);
    gtk_tree_store_set(maintreestore, iter,col_statustext,fstatus,-1);


    sprintf(buf,"|%lld|",adentry);
    if(strstr(ceffect,buf) || altimechanged)
    {
        ch=gtk_entry_get_text(adentry);
        strcpy(buf,ch);
        char * savptr;
        char * token = strtok_r(buf, "-",&savptr);
        int df=3,yr=0,mh=0,dy=0;

        while( (token != NULL) && df)
        {
            if (df==3){yr=atol(token);}
            if (df==2){mh=atol(token);}
            if (df==1){dy=atol(token);}
            token = strtok_r(NULL, "-",&savptr);
            df--;
        }

        if(strlen(ch)==8 && !strstr(ch,"-"))
        {
            char sixdate[100]={};
            strcpy(sixdate,ch);
            sixdate[4]=0;
            yr=atol(&sixdate);
            strcpy(sixdate,ch);
            sixdate[6]=0;
            mh=atol(&sixdate[4]);
            strcpy(sixdate,ch);
            dy=atol(&sixdate[6]);
        }


        struct tm date = {} ;
        date.tm_year = yr - 1900 ;
        date.tm_mon = mh - 1 ;
        date.tm_mday = dy ;
        date.tm_hour=0;
        date.tm_min=0;
        date.tm_sec=1;
        date.tm_isdst=0;
        int hourval=gtk_spin_button_get_value(ahour);
        int minuteval=gtk_spin_button_get_value(aminute);
        date.tm_hour=hourval;
        date.tm_min=minuteval;
        time_t ndate=mktime(&date);
        if (ndate<=0){ndate=0;}
        gtk_tree_store_set(maintreestore, iter,(col_alarmdate),(gdouble)ndate,col_alarmdone,0,-1);
        sprintf(buf,",alarmdate=%lld,alarmdone=0,alarmhour=%i,alarmminute=%i",ndate,hourval,minuteval);
        zcpy(&dbz,1,buf);
    }


    sprintf(buf,"|%lld|",ahour);
    if(strstr(ceffect,buf))
    {
        ch=gtk_entry_get_text(ahour);
        int ahourp=atol(ch);
        gtk_tree_store_set(maintreestore, iter,col_alarmhour,ahourp,-1);
        sprintf(buf,",alarmhour=%i",ahourp);
        zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|%lld|",aminute);
    if(strstr(ceffect,buf))
    {
        ch=gtk_entry_get_text(aminute);
        int aminutep=atol(ch);
        gtk_tree_store_set(maintreestore, iter,col_alarmminute,aminutep,-1);
        sprintf(buf,",alarmminute=%i",aminutep);
        zcpy(&dbz,1,buf);
    }







    sprintf(buf,"|%lld|",rentry);
    if(strstr(ceffect,buf))
    {
        ch=gtk_entry_get_text(rentry);
        int rentryp=atol(ch);
        if(!rentryp){rentryp=1;}
        gtk_tree_store_set(maintreestore, iter,col_recurfreqval,rentryp,-1);
        sprintf(buf,",recurfreq=%i",rentryp);
        zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|%lld|",comborty);
    if(strstr(ceffect,buf))
    {
        gtk_tree_store_set(maintreestore, iter,col_recurtypeval,gtk_combo_box_get_active(comborty),-1);
        recursec(gtk_combo_box_get_active(comborty));
        sprintf(buf,",recurtype=%i",gtk_combo_box_get_active(comborty));
        zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|%lld|",comborba);
    if(strstr(ceffect,buf))
    {
        gtk_tree_store_set(maintreestore, iter,col_recurbaseval,gtk_combo_box_get_active(comborba),-1);
        sprintf(buf,",recurbase=%i",gtk_combo_box_get_active(comborba));
        zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|weekdays|");
    if(strstr(ceffect,buf))
    {
          char wdays[100]={};
          for (int x=1;x<8;x++)
          {
                sprintf(buf,"|%i",gtk_toggle_button_get_active(weekday[x])*x);
                strcat(wdays,buf);
          }
          gtk_tree_store_set(maintreestore, iter,col_weekdaystext,wdays,-1);
          sprintf(buf,",weekdays='%s'",wdays);
          zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|%lld|",comboper);
    if(strstr(ceffect,buf))
    {
        gtk_tree_store_set(maintreestore, iter,col_periodval,gtk_combo_box_get_active(comboper),-1);
        sprintf(buf,",period=%i",gtk_combo_box_get_active(comboper));
        zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|%lld|",combow);
    if(strstr(ceffect,buf))
    {
        gtk_tree_store_set(maintreestore, iter,col_perioddayval,gtk_combo_box_get_active(combow),-1);
        sprintf(buf,",periodday=%i",gtk_combo_box_get_active(combow));
        zcpy(&dbz,1,buf);
    }

    sprintf(buf,"|notes|");
    if(strstr(ceffect,buf) || vnotes)
    {
        gtk_tree_store_set(maintreestore, iter,col_notes,dbnotes,-1);
        repx(&dbnotes,"'","''");
        zcpy(&dbz,1,",notes='");
        zcpy(&dbz,1,dbnotes);
        zcpy(&dbz,1,"'");
        //free(dbnotes);dbnotes=NULL;
    }



    sprintf(buf," where UID = %i;",suid);
    zcpy(&dbz,1,buf);


    pthread_mutex_unlock(&zlock);

    GtkTreePath * gp=gtk_tree_model_get_path(maintreestore,iter);
    if(!noscroll && !fda){gtk_tree_view_scroll_to_cell(trunk,gp,NULL,FALSE,0.5,0);}
    gtk_tree_path_free(gp);


    gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
    nosort=0;
    GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
    gtk_tree_sortable_set_sort_column_id(sortable, 99, GTK_SORT_DESCENDING);
    gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);

    return FALSE;

}



void tdbupdate()
{
    pthread_mutex_lock(&lock);
    pthread_mutex_lock(&zlock);
    char * dbo=NULL;
    zcpy(&dbo,0,dbz);
    zcpy(&dbz,0,"");
    pthread_mutex_unlock(&zlock);

    if(strstr(dbo,"BEGIN") && !strstr(dbo,"COMMIT")){pthread_mutex_unlock(&lock);return;}
    if (strlen(dbo)<15){pthread_mutex_unlock(&lock);return;}

    sqlite3 * sgp;
    int c=sqlite3_open(curdatfile,&sgp);

    if(strlen(curtagory)>0)
    {

        int found=0;
        GtkTreeIter ctiter;
        if(gtk_tree_model_get_iter_first(clist,&ctiter))
        {
            do
            {
                gchar * cname=NULL;
                gtk_tree_model_get(clist,&ctiter,0,&cname,-1);
                if(!strcmp(curtagory,cname)){found=1;}
                g_free(cname);
            } while(gtk_tree_model_iter_next(clist,&ctiter));
        }

        if(!found)
        {
            gtk_combo_box_text_append_text(tagory,curtagory);
            GtkTreeIter cviter;
            gtk_list_store_append(clist, &cviter);
            gtk_list_store_set(clist,&cviter,0,curtagory,-1);
        }

        curtagory[0]=0;
    }

    int tries=30;
    do
    {
        c=sqlite3_exec(sgp,dbo,NULL,NULL,NULL);
    } while (c!=SQLITE_OK && tries--);
    if(!tries){AlertMessage(NULL,"Error writing to task data file");}

    sqlite3_close(sgp);
    zcpy(&dbo,0,"");
    free(dbo);

    pthread_mutex_unlock(&lock);

}

void dbupdate()
{
    iret1 = pthread_create( &thread1, NULL, tdbupdate, NULL);
}


int immchange(GtkWidget * g, int i)
{

        if(holdoff){return FALSE;}
        if(startup){return FALSE;}
        char buf[100];
        sprintf(buf,"|%lld|",g);
        gint wdcheck = (gint)i;
        if(wdcheck==1111111){sprintf(buf,"|weekdays|");}
        if(wdcheck==2222222){sprintf(buf,"|notes|");}
        if(!strstr(ceffect,buf)){strcat(ceffect,buf);}

        zcpy(&dbz,0,"");
        ctask(g,1);

}

int unfhidefuture(GtkWidget * g, int i)
{
    gtk_toggle_button_set_active(fhidefuture,FALSE);
}

int unfilter(GtkWidget * g, int i)
{
    if(gtk_toggle_button_get_active(inotes))
    {
        GtkWidget * combotftext = gtk_bin_get_child(combotf);
        gtk_entry_set_text(combotftext,"");
        gtk_tree_model_filter_refilter(gtf);
    }
}


void AlertMessage (GtkWidget *wid, gchar * mess)
{
  GtkWidget *dialog = NULL;
  dialog = gtk_message_dialog_new (GTK_WINDOW (win), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, mess);
  gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

gint getseluid (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
  {
      gint guid;
      gtk_tree_model_get (model, iter,11, &guid,-1);
      return guid;
  }


gboolean rowdate (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
  {
    gint col = GPOINTER_TO_INT(userdata);
    gdouble cdate;
    gtk_tree_model_get (model, iter,col, &cdate,-1);
    time_t ctime=(time_t)cdate;
    if(!ctime){ctime=time(0);}
    struct tm nd=*localtime(&ctime);
    nd.tm_year+=1900;
    char buf[100];
    gtk_calendar_select_month(ddc,(guint)nd.tm_mon,(guint)nd.tm_year);
    gtk_calendar_select_day(ddc,(guint)nd.tm_mday);
    return FALSE;

  }




static void showcala(GtkWidget * gi,int w)
{
    static int st6=0;

    if (!st6 && w!=2)
    {
        gtk_widget_show(ddc);
        gtk_widget_hide(gf4);st6=1;
        gtk_fixed_move(gf2,ddc,10 hh,330 vv);
        gtk_button_set_label(adp,"Okay");
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_tree_selection_selected_foreach(sel, rowdate, col_alarmdate);
        gtk_widget_set_sensitive(ddp,FALSE);
        gtk_widget_set_sensitive(sdp,FALSE);
        gtk_widget_set_sensitive(trunk,FALSE);
        gtk_widget_set_sensitive(gftop,FALSE);
        gtk_widget_set_sensitive(gf10,FALSE);
        gtk_widget_set_sensitive(ddb,FALSE);
        gtk_widget_set_sensitive(ddp,FALSE);
        gtk_widget_set_sensitive(adb,FALSE);
        gtk_widget_set_sensitive(ddentry,FALSE);
        gtk_widget_set_sensitive(sdentry,FALSE);
        gtk_widget_set_sensitive(tentry,FALSE);
        gtk_widget_set_sensitive(pentry,FALSE);
        gtk_widget_set_sensitive(sdb,FALSE);

    }
    else
    {
        gtk_widget_show(gf4);
        gtk_widget_hide(ddc);st6=0;
        gtk_button_set_label(adp,"Pick");
        guint gyear,gmonth,gday;
        gtk_calendar_get_date(ddc,&gyear,&gmonth,&gday);
        struct tm nd={};
        nd.tm_year=(int)gyear;
        nd.tm_year-=1900;
        nd.tm_mon=(int)gmonth;
        nd.tm_mday=(int)gday;
        time_t cdate=mktime(&nd);
        char buf[100];
        strftime(buf,99,"%Y-%m-%d",&nd);
        gtk_entry_set_text(adentry,buf);
        immchange(NULL,NULL);
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ddb)))
        {gtk_widget_set_sensitive(ddp,TRUE);gtk_widget_set_sensitive(ddentry,TRUE);}
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sdb)))
        {gtk_widget_set_sensitive(sdp,TRUE);gtk_widget_set_sensitive(sdentry,TRUE);}
        gtk_toggle_button_set_active(fhidefuture,FALSE);
        gtk_widget_set_sensitive(trunk,TRUE);
        gtk_widget_set_sensitive(gftop,TRUE);
        gtk_widget_set_sensitive(gf10,TRUE);
        gtk_widget_set_sensitive(ddb,TRUE);
        gtk_widget_set_sensitive(tentry,TRUE);
        gtk_widget_set_sensitive(pentry,TRUE);
        gtk_widget_set_sensitive(sdb,TRUE);
        gtk_widget_set_sensitive(adb,TRUE);

    }


}


static void showcal(GtkWidget * gi,int w)
{
    static int st4=0,st5=0;

    if(w==2 && !st4 && !st5){showcala(NULL,2);return;}
    if(w==2){w=(st4 == 0 ? 1 : 0) ;}

    if (!st4 && !w){gtk_widget_show(ddc);gtk_widget_hide(gf3);st4=1;st5=0;}
    else if (!st5 && w){gtk_widget_show(ddc);gtk_widget_hide(gf4);gtk_widget_hide(gf6);st5=1;st4=0;}
    else if (st4 && !w){gtk_widget_show(gf3);gtk_widget_hide(ddc);st4=0;st5=0;}
    else if (st5 && w){gtk_widget_show(gf4);gtk_widget_show(gf6);gtk_widget_hide(ddc);st4=0;st5=0;}

    if (st4 && !w)
    {
        gtk_fixed_move(gf2,ddc,10 hh,180 vv);
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_button_set_label(ddp,"Okay");
        gtk_widget_set_sensitive(ddp,TRUE);
        gtk_tree_selection_selected_foreach(sel, rowdate, 8);
        gtk_widget_set_sensitive(trunk,FALSE);
        gtk_widget_set_sensitive(gftop,FALSE);
        gtk_widget_set_sensitive(gf10,FALSE);
        gtk_widget_set_sensitive(tentry,FALSE);
        gtk_widget_set_sensitive(pentry,FALSE);
        gtk_widget_set_sensitive(ddb,FALSE);
    }

    if (!st4 && !w)
    {
        gtk_button_set_label(ddp,"Pick");
        guint gyear,gmonth,gday;
        gtk_calendar_get_date(ddc,&gyear,&gmonth,&gday);
        struct tm nd={};
        nd.tm_year=(int)gyear;
        nd.tm_year-=1900;
        nd.tm_mon=(int)gmonth;
        nd.tm_mday=(int)gday;
        time_t cdate=mktime(&nd);
        char buf[100];
        strftime(buf,99,"%Y-%m-%d",&nd);
        gtk_entry_set_text(ddentry,buf);
        immchange(NULL,NULL);
        gtk_widget_set_sensitive(trunk,TRUE);
        gtk_widget_set_sensitive(gftop,TRUE);
        gtk_widget_set_sensitive(gf10,TRUE);
        gtk_widget_set_sensitive(tentry,TRUE);
        gtk_widget_set_sensitive(pentry,TRUE);
        gtk_widget_set_sensitive(ddb,TRUE);

    }

    if (st5 && w)
    {
        gtk_fixed_move(gf2,ddc,10 hh,230 vv);
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        gtk_button_set_label(sdp,"Okay");
        gtk_widget_set_sensitive(sdp,TRUE);
        gtk_widget_set_sensitive(ddp,FALSE);
        gtk_tree_selection_selected_foreach(sel, rowdate, 7);
        gtk_toggle_button_set_active(fhidefuture,FALSE);
        gtk_widget_set_sensitive(trunk,FALSE);
        gtk_widget_set_sensitive(gftop,FALSE);
        gtk_widget_set_sensitive(gf10,FALSE);
        gtk_widget_set_sensitive(ddb,FALSE);
        gtk_widget_set_sensitive(ddp,FALSE);
        gtk_widget_set_sensitive(ddentry,FALSE);
        gtk_widget_set_sensitive(tentry,FALSE);
        gtk_widget_set_sensitive(pentry,FALSE);
        gtk_widget_set_sensitive(sdb,FALSE);

    }

    if (!st5 && w)
    {
        gtk_button_set_label(sdp,"Pick");
        guint gyear,gmonth,gday;
        gtk_calendar_get_date(ddc,&gyear,&gmonth,&gday);
        struct tm nd={0,0,12};
        nd.tm_year=(int)gyear;
        nd.tm_year-=1900;
        nd.tm_mon=(int)gmonth;
        nd.tm_mday=(int)gday;
        time_t cdate=mktime(&nd);
        char buf[100];
        strftime(buf,99,"%Y-%m-%d",&nd);
        gtk_entry_set_text(sdentry,buf);
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ddb)))
        {gtk_widget_set_sensitive(ddp,TRUE);gtk_widget_set_sensitive(ddentry,TRUE);}
        immchange(NULL,NULL);
        gtk_toggle_button_set_active(fhidefuture,FALSE);
        gtk_widget_set_sensitive(trunk,TRUE);
        gtk_widget_set_sensitive(gftop,TRUE);
        gtk_widget_set_sensitive(gf10,TRUE);
        gtk_widget_set_sensitive(ddb,TRUE);
        gtk_widget_set_sensitive(tentry,TRUE);
        gtk_widget_set_sensitive(pentry,TRUE);
        gtk_widget_set_sensitive(sdb,TRUE);
    }

}



gint smallsort (GtkTreeModel *model,GtkTreeIter  *a,GtkTreeIter  *b, int v)
{
    gint ret=0,ccol1,ccol2;
    GtkTreeSortable * sortable = GTK_TREE_SORTABLE(model);

     gtk_tree_model_get(model, a, 2,&ccol1,-1);
     gtk_tree_model_get(model, b, 2,&ccol2,-1);

    for(gint x=1;x<7;x++)
    {
        if(sortset[x].heading==ccol2 && !ret){ret=-1;}
        if(sortset[x].heading==ccol1 && !ret){ret=1;}
    }

    return ret;
}


gint varisort (GtkTreeModel *model,GtkTreeIter  *a,GtkTreeIter  *b, int v, int o)
{
    gchar * ttle[2];
    ttle[0]=NULL;
    ttle[1]=NULL;
    gchar * ctgy[2];
    ctgy[0]=NULL;
    ctgy[1]=NULL;
    gdouble sda[2],dda[2],fda[2];
    int pri[2],rank[2];
    GtkTreeIter * citerp[2]={};
    citerp[0]=a;citerp[1]=b;
    int ret=0;
    GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
    gint sortid;
    GtkSortType order;

     gtk_tree_model_get(model, a, col_startdateval,&sda[0],col_duedateval,&dda[0],col_donedateval,&fda[0],col_priority,&pri[0],col_title,&ttle[0],col_tags,&ctgy[0],-1);
     gtk_tree_model_get(model, b, col_startdateval,&sda[1],col_duedateval,&dda[1],col_donedateval,&fda[1],col_priority,&pri[1],col_title,&ttle[1],col_tags,&ctgy[1],-1);

    time_t tnow=time(0);
    struct tm tday=*localtime(&tnow);
    tnow+=(60*60) * (tday.tm_isdst);

    if(!dda[0] && !sda[0]){checksubstatus(model,a,&dda[0],&sda[0]);}
    if(!dda[1] && !sda[1]){checksubstatus(model,b,&dda[1],&sda[1]);}

    char nbuf[100]={};

    time_t dst = (time_t)(sda[0]);
    struct tm nndate = *localtime(&dst);
    strftime(nbuf,99,"%Y-%m-%d",&nndate);

    if (v==1)
    {
        rank[0]=4;rank[1]=4;

        for(int x=0;x<2;x++)
        {
            if((tnow>dda[x]-60*60*24) && (dda[x]>0)){rank[x]=3;}
            if((tnow<sda[x] && (sda[x]>0))){rank[x]=5;}
            if((tnow>dda[x]) && (dda[x]>0)){rank[x]=2;}
            if((tnow>dda[x]+60*60*24 && (dda[x]>0))){rank[x]=1;}
            if(fda[x]!=0){rank[x]=7;}
            if(fda[x]<0){rank[x]=6;}
        }

        ret = (rank[1] > rank[0] ? 1 : -1);
        if(o){ret*=-1;}
        if (rank[0]==rank[1]){ret=0;}
        return ret;
    }

    if(v==2)
    {
          ret = (dda[0] > dda[1]) ? -1 : 1;
          if(o){ret*=-1;}
          if (!dda[0]){ret=-1;}
          if (!dda[1]){ret=1;}
          if(dda[0]==dda[1]){ret=0;}
          return ret;
    }

    if(v==3)
    {
          ret = (pri[0] > pri[1] ? -1 : 1);
          if(o){ret*=-1;}
          if (!pri[0]){ret=-1;}
          if (!pri[1]){ret=1;}

          if(pri[0]<0 && pri[1]>=0){ret=-1;}
          if(pri[1]<0 && pri[0]>=0){ret=1;}
          if(pri[0]<0 && pri[1]<0 && pri[0]<pri[1]){ret=-1;}
          if(pri[0]<0 && pri[1]<0 && pri[1]<pri[0]){ret=1;}
          if(pri[0]==pri[1]){ret=0;}
          return ret;
    }

    if(v==4)
    {
          ret = (sda[0] > sda[1]) ? -1 : 1;
          if(o){ret*=-1;}
          if (!sda[0]){ret=-1;}
          if (!sda[1]){ret=1;}
          if(sda[0]==sda[1]){ret=0;}
          return ret;
    }

    if(v==5)
    {
        lowerit(ttle[0]);lowerit(ttle[1]);
        if (ttle[0] == NULL || ttle[1] == NULL)
        {
          if (ttle[0] == NULL && ttle[1] == NULL){ret=0;}
          else{ret = (ttle[0] == NULL) ? -1 : 1;}
        }
        else
        {
          ret = g_utf8_collate(ttle[1],ttle[0]);
          if(o){ret*=-1;}
        }
        return ret;
    }

    if(v==6)
    {   if (ctgy[0] == NULL || ctgy[1] == NULL)
        {
          if (ctgy[0] == NULL && ctgy[1] == NULL){ret=0;}
          else{ret = (ctgy[0] == NULL) ? -1 : 1;}
        }
        else
        {
          ret = g_utf8_collate(ctgy[1],ctgy[0]);
          if(o){ret*=-1;}
        }
        return ret;
    }



    g_free(ttle[0]);
    g_free(ttle[1]);
    g_free(ctgy[0]);
    g_free(ctgy[1]);
    return ret;
}


gboolean fliplist (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer       userdata)
{
      gint colnum;
      gtk_tree_model_get(model, iter, 2, &colnum, -1);
      for(int x=1;x<7;x++)
      {
          if(colnum==sortset[x].heading)
          {
              if(sortset[x].ad==0)
              {sortset[x].ad=1;gtk_list_store_set(GTK_LIST_STORE(slist),iter,1,"DES",-1);}
              else {sortset[x].ad=0;gtk_list_store_set(GTK_LIST_STORE(slist),iter,1,"ASC",-1);}
              break;
          }
      }
      GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
      gtk_tree_sortable_set_sort_column_id(sortable, 10, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);

      return FALSE;
}


void flipfirst(GtkWidget * w)
{
    GtkTreeSelection * sel = gtk_tree_view_get_selection((GTK_TREE_VIEW(sortl)));
    gtk_tree_selection_selected_foreach(sel, fliplist, NULL);
}



gboolean moveupdown2 (GtkTreeModel  *model,GtkTreePath   *path,GtkTreeIter   *iter, gpointer  userdata)
{
      gint mud=GPOINTER_TO_INT(userdata);
      int mudd = mud ? -1 : 1;
      gint colnum,movinx;
      gchar * lastt, * lastad;
      gtk_tree_model_get(model, iter, 2, &colnum, -1);
      for(int x=1;x<7;x++)
      {
          if(colnum==sortset[x].heading)
          {
              if(x==6 && mud){return;}
              sortset[0].heading=sortset[x].heading;
              sortset[0].ad=sortset[x].ad;
              sortset[x].heading=sortset[x-mudd].heading;
              sortset[x].ad=sortset[x-mudd].ad;
              sortset[x-mudd].heading=sortset[0].heading;
              sortset[x-mudd].ad=sortset[0].ad;
              movinx=x;
              break;
          }
      }

      GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
      GtkTreeSortable * sortable2 = GTK_TREE_SORTABLE(slist);
      gtk_tree_sortable_set_sort_column_id(sortable, 10, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable2, 2, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable2, 1, GTK_SORT_DESCENDING);

      return FALSE;

}



void moveupdown1(GtkWidget * w, int i)
{
    GtkTreeSelection * sel = gtk_tree_view_get_selection((GTK_TREE_VIEW(sortl)));
    gtk_tree_selection_selected_foreach(sel, moveupdown2, i);
}


gboolean rcolors2 (GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter, gpointer userdata)
  {
        if(!gtk_toggle_button_get_active(usecolors)){gtk_tree_store_set(model,iter,col_pcolor,"#333333",col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);}
        else{colorstatus(iter);}
        return FALSE;
  }


  void rcolors()
  {
      gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), rcolors2, NULL);
  }



void switchsort(int h, int a)
{

      if(sortset[1].heading==h && a==2)
      {sortset[1].ad^=1;}

      else
      {
          sortset[0].heading=h;
          for(int x=1;x<=6;x++)
          {
              if(sortset[x].heading==h)
              {
                  if(a==2){sortset[0].ad=sortset[x].ad;}
                  else{sortset[0].ad=a;}

                  for(int z=x;z>0;z--)
                  {sortset[z].heading=sortset[z-1].heading;sortset[z].ad=sortset[z-1].ad;}
                  break;
              }
          }
      }

      GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
      GtkTreeSortable * sortable2 = GTK_TREE_SORTABLE(slist);
      gtk_tree_sortable_set_sort_column_id(sortable, 10, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable2, 2, GTK_SORT_DESCENDING);
      gtk_tree_sortable_set_sort_column_id(sortable2, 1, GTK_SORT_DESCENDING);
      gtk_tree_model_foreach(GTK_TREE_MODEL(slist), sforeach_func, NULL);

}

gboolean headerclick(GtkWidget * g, gpointer userdata)
{
    if(chaparmode){chapar(NULL,1);return TRUE;}
    gint gu=GPOINTER_TO_INT(userdata);
    switchsort(gu,2);
    return TRUE;
}



gint multisort (GtkTreeModel *model,GtkTreeIter  *a,GtkTreeIter  *b, gpointer userdata)
{
      if(nosort || userdata==99){return;}

    int ret=0;
    for(int x=1;x<=6;x++)
    {
        ret=varisort(model,a,b,sortset[x].heading,sortset[x].ad);
        if(ret){break;}
    }
    return ret;

}



static void onRowCollapsed (GtkTreeView *view,GtkTreeIter * iter, GtkTreePath * path,gpointer user_data)
{
    gint cuid;
    gint tuid;
    GtkTreeIter riter;
    char buf[100];
    gtk_tree_model_get(gtf,iter,col_uid,&cuid,-1);
    sprintf(buf,"update tasks set xp=0 where UID=%i;",cuid);
    zcpy(&dbz,1,buf);
    GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path(GTK_TREE_MODEL_FILTER (gtf),path);
    gtk_tree_model_get_iter(maintreestore, &riter, true_path);
    gtk_tree_model_get(maintreestore,&riter,col_uid,&tuid,-1);
    gtk_tree_store_set(maintreestore,&riter,col_xp,0,-1);
    gtk_tree_path_free(true_path);

}



static void onRowExpanded (GtkTreeView *view,GtkTreeIter * iter, GtkTreePath * path,gpointer user_data)
{
    if(initload){return;}
    gint cuid,tuid;
    GtkTreeIter riter;
    char buf[100];
    gtk_tree_model_get(gtf,iter,col_uid,&cuid,-1);
    GtkTreePath * true_path = gtk_tree_model_filter_convert_path_to_child_path(GTK_TREE_MODEL_FILTER (gtf),path);
    gtk_tree_model_get_iter(maintreestore, &riter, true_path);
    gtk_tree_model_get(maintreestore,&riter,col_uid,&tuid,-1);
    gtk_tree_store_set(maintreestore,&riter,col_xp,1,-1);
    sprintf(buf,"update tasks set xp=1 where UID=%i;",cuid);
    zcpy(&dbz,1,buf);
    gtk_tree_path_free(true_path);

    GtkTreeIter child,pariter;
    gtk_tree_model_iter_children(maintreestore,&child,&riter);

    int ixp;

    do
        {
            gtk_tree_model_get(maintreestore,&child,col_xp,&ixp,-1);
            GtkTreePath * gtp = gtk_tree_model_get_path(maintreestore,&child);
            GtkTreePath * tpath = gtk_tree_model_filter_convert_child_path_to_path(GTK_TREE_MODEL_FILTER (gtf),gtp);
            if(ixp){gtk_tree_view_expand_row(trunk,tpath,FALSE);}
            gtk_tree_path_free(tpath);
            gtk_tree_path_free(gtp);
        } while(gtk_tree_model_iter_next(maintreestore,&child));

}


static gboolean onColumnsReordered (GtkTreeView *view,GtkTreePath * path,GtkTreeViewColumn * col,gpointer user_data)
{
    if(startup){return FALSE;}
    gtk_tree_view_move_column_after(trunk,chekcol,NULL);
    gtk_tree_view_move_column_after(trunk,colarray[5],chekcol);

    return FALSE;
}



static gboolean onRowReleased (GtkTreeView *view,GtkTreePath * path,GtkTreeViewColumn * col,gpointer user_data)
{
    if(togafter){togcomp();togafter=0;}
    return FALSE;
}



static void onRowActivated (GtkTreeView *view,GtkTreePath * path,GtkTreeViewColumn * col,gpointer user_data)
{
    static int tlight=0;
    if(tlight || noac){noac=0;return;}
    tlight=1;
    togcomp();
    tlight=0;
}


  static gboolean onRowToggled (GtkCellRendererToggle * self,gchar * cpath)
  {
    GtkTreeIter iter,niter;
    gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL (maintreestore), &iter, cpath);
    gtk_tree_model_filter_convert_child_iter_to_iter(gtf,&niter,&iter);
    int togor;
    gtk_tree_model_get(maintreestore,&iter,col_taskdoneint,&togor,-1);
    gtk_tree_store_set(GTK_TREE_MODEL(maintreestore),&iter,col_taskdoneint,!(togor),-1);
    GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));

    togafter=1;
    return FALSE;

  }





void donesubs(GtkTreeModel * model, GtkTreeIter * iter)
  {

      GtkTreeIter child;
      gint rty;
      time_t tnow=time(0);
      time_t sda,dda,fda;
      gdouble gfda;
      int suid;

       do
        {
            gtk_tree_model_get(model,iter,col_recurtypeval,&rty,col_startdateval,&sda,col_duedateval,&dda,col_donedateval,&gfda,col_uid,&suid,-1);

            if(!rty)
            {
                time_t nfda;
                if(gfda>0){nfda=0;}
                else if(gfda<-1){nfda=-1;}
                else if(gfda==-1){nfda=-1;}
                else if(gfda==0){nfda=0;}
                fda=nfda;

                gtk_tree_store_set(maintreestore,iter,col_donedateval,(gdouble)fda,-1);
                colorstatus(iter);
                char buf[1000];
                sprintf(buf,"update tasks set donedate=%lld where UID=%i;",(time_t)fda,suid);
                zcpy(&dbz,1,buf);
            }
            char fstatus[100];
            figstatus(fstatus,sda,dda,fda);
            gtk_tree_store_set(maintreestore,iter,col_statustext,fstatus,-1);

            if(gtk_tree_model_iter_children(model,&child,iter))
            {donesubs(model,&child);}
        } while(gtk_tree_model_iter_next(model,iter));
    }


    void recursec(int i)
    {
        if(i>0 && i<5){gtk_widget_show(hboxrec1);}else{gtk_widget_hide(hboxrec1);}
        if(i==5){gtk_widget_show(hboxdays);}else{gtk_widget_hide(hboxdays);}
        if(i==6){gtk_widget_show(hboxmonth);}else{gtk_widget_hide(hboxmonth);}
        if(i>0){gtk_widget_show(comborba);gtk_widget_show(labelrba);}else{gtk_widget_hide(comborba);gtk_widget_hide(labelrba);}
        if(i==0){gtk_label_set_text(recsuffix,"");}
        if(i==1){gtk_label_set_text(recsuffix,"Days");}
        if(i==2){gtk_label_set_text(recsuffix,"Weeks");}
        if(i==3){gtk_label_set_text(recsuffix,"Months");}
        if(i==4){gtk_label_set_text(recsuffix,"Years");}
        if(i<=0)
        {
            gtk_fixed_move(gf4,catelabel,10 hh,100 vv);
            gtk_fixed_move(gf4,tagory,5 hh,130 vv);
            gtk_fixed_move(gf4,acbutton,340 hh,130 vv);
            gtk_fixed_move(gf4,tagslabel,10 hh,200 vv);
            gtk_fixed_move(gf4,zwindow,5 hh,230 vv);
        }
        else
        {
            gtk_fixed_move(gf4,catelabel,10 hh,210 vv);
            gtk_fixed_move(gf4,tagory,5 hh,235 vv);
            gtk_fixed_move(gf4,acbutton,340 hh,235 vv);
            gtk_fixed_move(gf4,tagslabel,10 hh,290 vv);
            gtk_fixed_move(gf4,zwindow,5 hh,320 vv);
        }

    }


  gboolean rowpick (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
  {
    if(delling){return;}
    GtkTreeSelection * sel = gtk_tree_view_get_selection((GTK_TREE_VIEW(trunk)));
    rcpick++;

    gchar *sd,*dd, *wdays, *name, *gnotes,*gtags;
    gint pri,suid,rty,rfe,rba,rpe,rpd,alhour,alminute,aldone;
    gdouble sdate,ddate,odate,scddate,aldate,doned;
    time_t tdate;
    struct tm ds;
    char buf[1000];

    gtk_tree_model_get (model, iter, 0, &name,1,&pri,2,&sd,3,&dd,7,&sdate,8,&ddate,9,&doned,11,&suid,14,&rty,15,&rfe,16,&rba,17,&wdays,col_periodval,&rpe,col_perioddayval,&rpd,col_sourceduedateval,&scddate,col_notes,&gnotes,col_tags,&gtags,col_alarmdate,&aldate,col_alarmhour,&alhour,col_alarmminute,&alminute,col_alarmdone,&aldone,-1);
    seluid=suid;

    if(rcpick==1){gtk_entry_set_text(tentry,name);gtk_widget_set_sensitive(tentry,TRUE);}
    else{gtk_entry_set_text(tentry,"(multi-selection)");gtk_widget_set_sensitive(tentry,FALSE);}


    if(rcpick==1 && doned<0){gtk_toggle_button_set_active(tib,TRUE);}
    if(rcpick==1 && doned>=0){gtk_toggle_button_set_active(tib,FALSE);}
    if(rcpick>1 && doned<0 && gtk_toggle_button_get_active(tib)){gtk_toggle_button_set_active(tib,TRUE);}
    if(doned>=0){gtk_toggle_button_set_active(tib,FALSE);}


    if(rcpick==1 && (doned<-1 || doned>0)){gtk_widget_show(donelabel);}
    else if(rcpick>1 && (doned<-1 || doned>0) && gtk_widget_is_visible(donelabel)){gtk_widget_show(donelabel);}
    else {gtk_widget_hide(donelabel);}


    gdouble cpri=gtk_spin_button_get_value(pentry);
    if(rcpick==1 || (int)cpri==pri){gtk_spin_button_set_value(pentry,(gdouble)pri);}
    else{gtk_entry_set_text(pentrybox,"");}

    tdate=(time_t)sdate;
    ds = *localtime(&tdate);
    strftime(buf,99,"%Y-%m-%d",&ds);
    if (!tdate){sprintf(buf,"");}
    gchar * gc=gtk_entry_get_text(sdentry);
    if(rcpick==1 || !strcmp(buf,(char*)gc)){gtk_entry_set_text(sdentry,buf);gtk_toggle_button_set_active(sdb,tdate);gtk_widget_set_sensitive(sdp,tdate);gtk_widget_set_sensitive(sdentry,tdate);}
    else{gtk_entry_set_text(sdentry,"");gtk_toggle_button_set_active(sdb,FALSE);gtk_widget_set_sensitive(sdp,FALSE);gtk_widget_set_sensitive(sdentry,FALSE);}

    tdate=(time_t)ddate;
    ds = *localtime(&tdate);
    strftime(buf,99,"%Y-%m-%d",&ds);
    if (!tdate){sprintf(buf,"");}
    gc=gtk_entry_get_text(ddentry);
    if(rcpick==1 || !strcmp(buf,(char*)gc)){gtk_entry_set_text(ddentry,buf);gtk_toggle_button_set_active(ddb,tdate);gtk_widget_set_sensitive(ddp,tdate);gtk_widget_set_sensitive(ddentry,tdate);}
    else{gtk_entry_set_text(ddentry,"");gtk_toggle_button_set_active(ddb,FALSE);gtk_widget_set_sensitive(ddp,FALSE);gtk_widget_set_sensitive(ddentry,FALSE);}

    tdate=(time_t)aldate;
    ds = *localtime(&tdate);
    tdate-=(60*60) * ds.tm_isdst;
    ds = *localtime(&tdate);
    strftime(buf,99,"%Y-%m-%d",&ds);
    if (!tdate){sprintf(buf,"");}
    gc=gtk_entry_get_text(adentry);
    if(rcpick==1 || !strcmp(buf,(char*)gc)){gtk_entry_set_text(adentry,buf);togrem(NULL,tdate);}
    else{togrem(NULL,0);}

    int ampm=0;int hourval=alhour;int minuteval=alminute;
    if(hourval>12){hourval-=12;ampm=1;}
    if(hourval==12){ampm=1;}

    char buffer[100]={};
    sprintf(buffer,"%02i:%02i %s",hourval,minuteval,(ampm ? "pm" : "am" ));
    gtk_label_set_text(ampmlabel,buffer);

    if(!aldate){gtk_label_set_text(ampmlabel,"--:--");}


    int cahour=gtk_spin_button_get_value(ahour);
    if(rcpick==1 || (int)cahour==alhour)
    {gtk_spin_button_set_value(ahour,alhour);}
    else{gtk_spin_button_set_value(ahour,0);}

    int caminute=gtk_spin_button_get_value(aminute);
    if(rcpick==1 || (int)caminute==alminute)
    {gtk_spin_button_set_value(aminute,alminute);}
    else{gtk_spin_button_set_value(aminute,0);}

    if(!aldate || rcpick!=1){gtk_label_set_text(ampmlabel,"--:--");}


    int crty=gtk_combo_box_get_active(comborty);
    if(rcpick==1 || (int)crty==rty)
    {
        gtk_combo_box_set_active(comborty,rty);
        recursec(rty);
    }
    else{gtk_combo_box_set_active(comborty,-1);recursec(-1);}




    gc=gtk_entry_get_text(rentry);
    int crfe=atol((char*)gc);
    sprintf(buf,"%i",rfe);
    if(rcpick==1 || crfe==rfe){gtk_entry_set_text(rentry,buf);}
    else{gtk_entry_set_text(rentry,"");}

    gc=gtk_combo_box_text_get_active_text(tagory);
    GtkWidget * tagentry = gtk_bin_get_child(tagory);
    if(gtags && (rcpick==1 || !strcmp((char*)gc,(char*)gtags)))
    {gtk_entry_set_text(tagentry,(strlen((char*)gtags) ? gtags : "(None)"));}
    else{gtk_entry_set_text(tagentry,"");}

    gc=gtk_entry_get_text(wdtext);
    if(wdays && (rcpick==1 || !strcmp((char*)gc,(char*)wdays)))
    {
        gtk_entry_set_text(wdtext,wdays);
        for(int x=1;x<8;x++)
        {gtk_toggle_button_set_active(weekday[x],FALSE);}

        char * savptr;
        char * token = strtok_r(wdays, "|",&savptr);
        int df=1;

        while((token != NULL) && df<8)
        {
            gtk_toggle_button_set_active(weekday[df],atol(token));
            token = strtok_r(NULL, "|",&savptr);
            df++;
        }
    }
    else
    {
        gtk_entry_set_text(wdtext,"");
        for(int x=1;x<8;x++)
        {gtk_toggle_button_set_active(weekday[x],FALSE);}
    }

    int crpe=gtk_combo_box_get_active(comboper);
    if(rcpick==1 || (int)crpe==rpe){gtk_combo_box_set_active(comboper,rpe);}
    else{gtk_combo_box_set_active(comboper,-1);}

    int crpd=gtk_combo_box_get_active(combow);
    if(rcpick==1 || (int)crpd==rpd){gtk_combo_box_set_active(combow,rpd);}
    else{gtk_combo_box_set_active(combow,-1);}

    int crba=gtk_combo_box_get_active(comborba);
    if(rcpick==1 || (int)crba==rba){gtk_combo_box_set_active(comborba,rba);}
    else{gtk_combo_box_set_active(comborba,-1);}

    GtkTextIter start, end;
    GtkTextBuffer * vbuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (notesview));
    gtk_text_buffer_get_bounds (vbuffer, &start, &end);
    gchar * gcv = gtk_text_buffer_get_text(vbuffer,&start,&end,FALSE);
    if( (rcpick==1 && gnotes) || (gnotes && !strcmp((char*)gcv,(char*)gnotes)) )
    {
        gtk_text_buffer_set_text(vbuffer,gnotes,-1);
    }
    else{gtk_text_buffer_set_text(vbuffer,"",-1);}
    ceffect[0]=0;

    g_free(sd);g_free(dd);g_free(wdays);g_free(name);g_free(gnotes);g_free(gtags);

    pthread_mutex_unlock(&lock);
    return FALSE;


  }





void onSelectionChanged (GtkTreeSelection *treeselection,gpointer user_data)
{
    if(delling){return;}
    if(chaparmode){chapar(NULL,1);return;}

    seluid=0;seliter=NULL;
    holdoff=1;
    rcpick=0;
    gtk_tree_selection_selected_foreach(treeselection, rowpick, user_data);
    gtk_widget_set_sensitive(gf2,gtk_tree_selection_count_selected_rows(treeselection));
    if(!gtk_tree_selection_count_selected_rows(treeselection)){selnone(0);}
    holdoff=0;
    if(!user_data){dbupdate();}

}


void figstatus(char * c, time_t sda, time_t dda, time_t fda)
{
        time_t tnow=time(0);

        struct tm tday=*localtime(&tnow);

        tnow+=(60*60) * (tday.tm_isdst);

        strcpy(c,tnow >= sda ? "Active" : "Future" );
        if((tnow>(dda-60*60*24)) && (dda)){strcpy(c,"Due Tomorrow");}
        if((tnow<sda) && (sda>0)){strcpy(c,"Future");}
        if((tnow>dda) && (dda)){strcpy(c,"Due Today");}
        if((tnow>dda+60*60*24) && (dda>0)){strcpy(c,"Overdue");}
        if(fda){strcpy(c,"Done");}
        if(fda<0){strcpy(c,"Inactive");}



}

void checksubstatus(GtkTreeModel * m,GtkTreeIter * iter, gdouble * ldda,gdouble * lsda)
{
    static int ct=0;
    static gdouble csda=0;
    static int foundzero=0;
    gdouble osda=0;
    ct++;

    if(ct==1){foundzero=0;}

    GtkTreeIter child;
    if (gtk_tree_model_iter_children(maintreestore,&child,iter))
    {
        do
        {
            if(ct==1){csda=0;}
            gdouble tdda,tsda,tdone;
            gint ui;
            gtk_tree_model_get(maintreestore,&child,col_duedateval,&tdda,col_startdateval,&tsda,col_donedateval,&tdone,-1);
            gtk_tree_model_get(maintreestore,&child,col_uid,&ui,-1);
            if (tdone){continue;}

            if(tdda && (tdda<*ldda || !*ldda)){*ldda=tdda;}
            if(tsda && (tsda<*lsda || !*lsda)){*lsda=tsda;}
            foundzero = !tsda ? ct : 0;
            {checksubstatus(m,&child,ldda,lsda);}
            if (!*lsda){*lsda=-1;}
        } while(gtk_tree_model_iter_next(maintreestore,&child));

    }
    ct--;
    if (foundzero>ct){*lsda=-1;}
    if(*lsda==-1 && !ct){*lsda=0;}
}


void colorstatus(GtkTreeIter * iter)
{
    gdouble ddue,dstart,ddone;
    time_t tnow=time(0);
    struct tm tday=*localtime(&tnow);

    tnow+=(60*60) * (tday.tm_isdst);
    gchar * ptitle;
    gdouble dnow=(gdouble)tnow;
    gtk_tree_model_get(maintreestore,iter,col_duedateval,&ddue,col_startdateval,&dstart,col_donedateval,&ddone,col_title,&ptitle,-1);

   if(!gtk_toggle_button_get_active(usecolors)){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);gtk_tree_store_set(maintreestore, iter,22,"#333333",-1);return;}

   if(!ddue && !dstart){checksubstatus(maintreestore,iter,&ddue,&dstart);}

    char nbuf[100];
    time_t dst = (time_t)(dstart);
    struct tm nndate = *localtime(&dst);
    strftime(nbuf,99,"%Y-%m-%d",&nndate);

   GtkTreeIter child, pariter;

    int acf=0;
    if(ddue && !ddone && dnow>ddue+24*60*60){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_BOLD,-1);gtk_tree_store_set(maintreestore, iter,22,"red",-1);}
    else if (ddue && !ddone && dnow>ddue){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_BOLD,-1);gtk_tree_store_set(maintreestore, iter,22,"blue",-1);}
    else if (ddue && !ddone && dnow>ddue-24*60*60 && dnow>=dstart){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_BOLD,-1);gtk_tree_store_set(maintreestore, iter,22,"black",-1);}
    else if (ddone<0){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);gtk_tree_store_set(maintreestore, iter,22,"#999999",-1);}
    else if (ddone){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);gtk_tree_store_set(maintreestore, iter,22,"green",-1);}
    else if (dstart && dnow<dstart){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);gtk_tree_store_set(maintreestore, iter,22,"#aa00aa",-1);}
    else if (dstart || ddue){gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);gtk_tree_store_set(maintreestore, iter,22,"#333333",-1);}
    else{acf=1;gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);gtk_tree_store_set(maintreestore, iter,22,"#333333",-1);}

    if(1)
    {   GtkTreeIter pariter;
        int trypar=gtk_tree_model_iter_parent(maintreestore,&pariter,iter);
        if(trypar)
        {
            gchar * fstatus, * pcolor;
            gtk_tree_model_get(maintreestore,&pariter,col_statustext,&fstatus,col_pcolor,&pcolor,-1);
            if(acf && pcolor && !strcmp(pcolor,"#aa00aa"))
            {
                gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);
                gtk_tree_store_set(maintreestore, iter,22,"#aa00aa",-1);
            }
            if(pcolor && !strcmp(pcolor,"#999999"))
            {
                gtk_tree_store_set(maintreestore, iter,col_pweight,(gdouble)PANGO_WEIGHT_NORMAL,-1);
                gtk_tree_store_set(maintreestore, iter,22,"#999999",-1);
            }
            g_free(fstatus);
            g_free(pcolor);

        }

    }


    g_free(ptitle);

}


void cleartf(GtkWidget * g)
{
    GtkWidget * combotftext = gtk_bin_get_child(combotf);
    gtk_entry_set_text(combotftext,"");
}



void checksst(sqlite3_stmt * s, int pa, GtkTreeIter * pi)
{
    sqlite3_reset(s);
    int rp=0,rt=0;
    GtkTreeIter iter;
    char buf[1000]={};
    char tagories[1000]={};
    while (SQLITE_ROW==sqlite3_step(s))
    {
       if(rt++<rp) {continue;}
       rp++;
       if(pa==sqlite3_column_int(s,1) || pa==-1)
       {
           char ppri[20];
           sprintf(ppri,"        %i",sqlite3_column_int(s,dbcol_priority));
           if (!sqlite3_column_int(s,dbcol_priority)){sprintf(ppri," ");}
           char psda[100];
           time_t tdate=sqlite3_column_double(s,dbcol_startdateval);
           struct tm ds = *localtime(&tdate) ;
           strftime(psda,99,"%d %b %Y (%a)",&ds);
           //strftime(psda,99,"%d %b %Y",&ds);
           if (!tdate){sprintf(psda," ");}
           char pdda[100];
           tdate=sqlite3_column_double(s,dbcol_duedateval);
           ds = *localtime(&tdate) ;
           strftime(pdda,99,"%d %b %Y (%a)",&ds);
           //strftime(pdda,99,"%d %b %Y",&ds);
           if (!tdate){sprintf(pdda," ");}
           char stt[100] ;
           figstatus(&stt,sqlite3_column_double(s,dbcol_startdateval),sqlite3_column_double(s,dbcol_duedateval),sqlite3_column_double(s,dbcol_donedateval));
           time_t tnow=time(0);

           char uidbuf[100]={};
           sprintf(uidbuf," and UID !=%i ",sqlite3_column_int(s,dbcol_uid));
           zcpy(&uidlist,1,uidbuf);
           if(pa!=-1){zcpy(&uidlist2,1,uidbuf);}


           gtk_tree_store_append(maintreestore, &iter,pi);
           gtk_tree_store_set(maintreestore, &iter,col_title,sqlite3_column_text(s,dbcol_title),col_priority,sqlite3_column_int(s,dbcol_priority),col_pseudopritext,ppri,-1);
           gtk_tree_store_set(maintreestore, &iter,col_startdateval,sqlite3_column_double(s,dbcol_startdateval),col_startdatetext,psda,-1);
           gtk_tree_store_set(maintreestore, &iter,col_duedateval,sqlite3_column_double(s,dbcol_duedateval),col_duedatetext,pdda,-1);
           gtk_tree_store_set(maintreestore, &iter,col_donedateval,sqlite3_column_double(s,dbcol_donedateval),-1);
           gtk_tree_store_set(maintreestore, &iter,col_statustext,stt,col_uid,sqlite3_column_int(s,dbcol_uid),-1);
           gtk_tree_store_set(maintreestore, &iter,col_recurtypeval,sqlite3_column_int(s,dbcol_recurtype),col_recurfreqval,sqlite3_column_int(s,dbcol_recurfreq),col_recurbaseval,sqlite3_column_int(s,dbcol_recurbase),-1);
           gtk_tree_store_set(maintreestore, &iter,col_weekdaystext,sqlite3_column_text(s,dbcol_weekdaystext),col_periodval,sqlite3_column_int(s,dbcol_periodval),col_perioddayval,sqlite3_column_int(s,dbcol_perioddayval),col_sourceduedateval,sqlite3_column_double(s,dbcol_sourceduedateval),-1);
           gtk_tree_store_set(maintreestore, &iter,col_notes,sqlite3_column_text(s,dbcol_notes),-1);
           gtk_tree_store_set(maintreestore, &iter,col_xp,sqlite3_column_int(s,dbcol_xp),-1);
           gtk_tree_store_set(maintreestore, &iter,col_tags,sqlite3_column_text(s,dbcol_tags),-1);
           gtk_tree_store_set(maintreestore, &iter,col_alarmdate,sqlite3_column_double(s,dbcol_alarmdate),-1);
           gtk_tree_store_set(maintreestore, &iter,col_alarmhour,sqlite3_column_int(s,dbcol_alarmhour),-1);
           gtk_tree_store_set(maintreestore, &iter,col_alarmminute,sqlite3_column_int(s,dbcol_alarmminute),-1);
           gtk_tree_store_set(maintreestore, &iter,col_alarmdone,sqlite3_column_int(s,dbcol_alarmdone),-1);
           colorstatus(&iter);
           int ex=sqlite3_column_int(s,dbcol_xp);
           if(pa!=-1){rt=0;checksst(s,sqlite3_column_int(s,0),&iter);rt=0;}

       }
    }

}





gboolean colvis2 (GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{
    gint gi;
    gtk_tree_model_get(model,iter,2,&gi,-1);

    if(gtk_tree_view_column_get_visible(colarray[gi]))
    {gtk_tree_view_column_set_visible(colarray[gi],FALSE);gtk_list_store_set(GTK_LIST_STORE(slist),iter,4,"h",-1);}
    else{gtk_tree_view_column_set_visible(colarray[gi],TRUE);gtk_list_store_set(GTK_LIST_STORE(slist),iter,4,"",-1);}

    return FALSE;
}


void colvis(GtkWidget * g)
{
    gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(sortl),colvis2, NULL);
    gtk_tree_view_column_set_visible(colarray[7],!(gtk_toggle_button_get_active(fhidecheckboxes)));
}

void hidewin(GtkWidget * g)
{
    gtk_widget_hide(g);
}

void immquit(GtkWidget * g)
{
    pthread_mutex_lock(&lock);
    gtk_widget_hide(win);
    while (gtk_events_pending()){gtk_main_iteration();}

    SavePrefs();
    sqlite3 * sgp;
    int c=sqlite3_open(curdatfile,&sgp);
    c=sqlite3_exec(sgp,"VACUUM;",NULL,NULL,NULL);
    sqlite3_close((sgp));
    pthread_mutex_unlock(&lock);
    gtk_main_quit();
}

static gboolean row_visible2 (GtkTreeModel *model, GtkTreeIter *iter, GtkTreeStore *store)
{

    gint pri;
    gdouble sda,fda,dda;
    gint ret=1;
    gint cret=1,sret=1;
    gchar * notes=NULL,*tname=NULL,*ptitle=NULL,*pstatus=NULL,*pcolor=NULL;
    gtk_tree_model_get (model, iter,col_priority, &pri,col_duedateval,&dda,col_startdateval,&sda,col_donedateval,&fda,col_notes,&notes,col_tags,&tname,col_title,&ptitle,col_statustext,&pstatus,col_pcolor,&pcolor,-1);
    gdouble tnow=(gdouble)time(0);


    if(!dda && !sda &&!fda){checksubstatus(maintreestore,iter,&dda,&sda);}

     GtkTreeSelection * sel2 = gtk_tree_view_get_selection(GTK_TREE_STORE(cview));
     if(gtk_tree_selection_count_selected_rows(sel2) && !gtk_toggle_button_get_active(tf1))
    {sret=0;}


    gchar * tagtext;
    GtkWidget * combotftext = gtk_bin_get_child(combotf);
    tagtext=gtk_entry_get_text(GTK_ENTRY(combotftext));
    if(strlen(tagtext))
    {
         if(ptitle){lowerit(ptitle);}
        if(tname){lowerit(tname);}
        if(notes){lowerit(notes);}
        sret=0;cret=0;
        char tbuff[1000]={};
        strcpy(tbuff,tagtext);
        char * savptr;
        char * token = strtok_r(tbuff, " ",&savptr);

        while(token != NULL)
        {
            while(token[0]==' '){token++;}

            if(strlen(token))
            {
                if(token){lowerit(token);}
                if(notes && gtk_toggle_button_get_active(inotes) && strstr(notes,token)){sret=1;}
                if(tname && gtk_toggle_button_get_active(icat) && strstr(tname,token)){sret=1;}
                if(ptitle && gtk_toggle_button_get_active(ititle) && strstr(ptitle,token)){sret=1;}
            }
            token = strtok_r(NULL, " ",&savptr);
        }
    }


    GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_STORE(cview));
    if (gtk_tree_selection_count_selected_rows(sel) && !gtk_toggle_button_get_active(tf1))
    {
        cret=0;
        GList * list = gtk_tree_selection_get_selected_rows(sel,&clist);

        GtkTreeIter iter;
        GList *item;

		for (item = list; item != NULL; item = g_list_next(item))
		{
            if(!tname){continue;}
		    gchar * cname;
		    GtkTreePath *treepath = item->data;
		    gtk_tree_model_get_iter(clist,&iter,treepath);
            gtk_tree_model_get(clist,&iter,0,&cname,-1);
            if(!strcmp(tname,cname)){cret=1;}
            if(!strlen(tname) && !strcmp("(None)",cname)){cret=1;}
            g_free(cname);
		}
		if(gtk_toggle_button_get_active(tf4)){cret^=1;}
		g_list_free(list);
    }

    ret=(cret || sret);
    if(gtk_toggle_button_get_active(fhidedone) && fda>0){ret=0;}
    if(gtk_toggle_button_get_active(fhideinactive) && fda<0){ret=0;}
    if(gtk_toggle_button_get_active(fhidefuture) && sda && sda>tnow){ret=0;}


    g_free(tname);
    g_free(notes);
    g_free(ptitle);
    g_free(pstatus);
    g_free(pcolor);

    return ret;
}


gboolean ghostchek (GtkTreeModel  *model,GtkTreePath *path,GtkTreeIter *iter,gpointer userdata)
{

    gchar * pcolor;
    gtk_tree_model_get(model,iter,col_pcolor,&pcolor,-1);
    GtkTreeIter citer;
    gtk_tree_model_filter_convert_iter_to_child_iter(gtf,&citer,iter);

    GtkWidget * combotftext = gtk_bin_get_child(combotf);

    if(gtk_toggle_button_get_active(tf1) && !strlen(gtk_entry_get_text(combotftext)) && (pcolor && !strcmp(pcolor,"#aaaa44")))
    {colorstatus(&citer);}
    else if(!row_visible2(maintreestore,&citer,GTK_TREE_STORE(maintreestore)))
    {
        gtk_tree_store_set(GTK_TREE_STORE(maintreestore),&citer,col_pcolor,"#aaaa44",-1);
        gtk_tree_view_expand_row(trunk,path,TRUE);
    }
    g_free(pcolor);

    return FALSE;

}

void rfilter()
{
    GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    char * tbuf=NULL;
    zcpy(&tbuf,0,"");
    gtk_tree_selection_selected_foreach(sel1, storesel, &tbuf);

    gtk_tree_model_filter_refilter(gtf);
    GtkTreeSelection  *treeselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
    gtk_tree_selection_unselect_all(treeselection);
    gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
    gtk_tree_model_foreach(gtf,ghostchek,0);
    initload=1;gtk_tree_model_foreach(gtf, checkxp, NULL);initload=0;


    char * rbuf=tbuf;
    while(strchr(rbuf,'|'))
    {
        rbuf=strchr(rbuf,'|');
        rbuf++;
        int usel=atol(rbuf);
        gtk_tree_model_foreach(gtf, resel, usel);
        GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
        onSelectionChanged(sel,NULL);
    }
    free(tbuf);

}

void lowerit(char * str)
{
    for(int i = 0; str[i]; i++){
      str[i] = tolower(str[i]);
    }
}

static gboolean row_visible (GtkTreeModel *model, GtkTreeIter *iter, GtkTreeStore *store)
{

    gint pri;
    gdouble sda,fda,dda;
    gint ret=1;
    gint cret=1,sret=1;
    gchar * notes,*tname,*ptitle,*pcolor;
    gtk_tree_model_get (model, iter,col_priority, &pri,col_duedateval,&dda,col_startdateval,&sda,col_donedateval,&fda,col_notes,&notes,col_tags,&tname,col_title,&ptitle,col_pcolor,&pcolor,-1);
    gdouble tnow=(gdouble)time(0);

    if(!dda && !sda && !fda){checksubstatus(maintreestore,iter,&dda,&sda);}

    GtkTreeSelection * sel2 = gtk_tree_view_get_selection(GTK_TREE_STORE(cview));
     if(gtk_tree_selection_count_selected_rows(sel2) && !gtk_toggle_button_get_active(tf1))
    {sret=0;}

    gchar * tagtext;
    GtkWidget * combotftext = gtk_bin_get_child(combotf);
    tagtext=gtk_entry_get_text(GTK_ENTRY(combotftext));
    if(strlen(tagtext))
    {
        if(ptitle){lowerit(ptitle);}
        if(tname){lowerit(tname);}
        if(notes){lowerit(notes);}

        sret=0;cret=0;
        char tbuff[1000]={};
        strcpy(tbuff,tagtext);
        char * savptr;
        char * token = strtok_r(tbuff, " ",&savptr);

        while(token != NULL)
        {
            while(token[0]==' '){token++;}

            if(strlen(token))
            {
                if(token){lowerit(token);}
                if(notes && gtk_toggle_button_get_active(inotes) && strstr(notes,token)){sret=1;}
                if(tname && gtk_toggle_button_get_active(icat) && strstr(tname,token)){sret=1;}
                if(ptitle && gtk_toggle_button_get_active(ititle) && strstr(ptitle,token)){sret=1;}
            }
            token = strtok_r(NULL, " ",&savptr);
        }
    }


    GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_STORE(cview));
    if (gtk_tree_selection_count_selected_rows(sel) && !gtk_toggle_button_get_active(tf1))
    {
        cret=0;
        GList * list = gtk_tree_selection_get_selected_rows(sel,&clist);

        GtkTreeIter iter;
        GList *item;

		for (item = list; item != NULL; item = g_list_next(item))
		{
            if(!tname){continue;}
		    gchar * cname=NULL;
		    GtkTreePath *treepath = item->data;
		    gtk_tree_model_get_iter(clist,&iter,treepath);
            gtk_tree_model_get(clist,&iter,0,&cname,-1);
            if(!strcmp(tname,cname)){cret=1;}
            if(!strlen(tname) && !strcmp("(None)",cname)){cret=1;}
            g_free(cname);
		}
		if(gtk_toggle_button_get_active(tf4)){cret^=1;}
		g_list_free(list);
    }

    ret=(cret || sret);
    if(gtk_toggle_button_get_active(fhidedone) && fda>0){ret=0;}
    if(gtk_toggle_button_get_active(fhideinactive) && fda<0){ret=0;}
    if(gtk_toggle_button_get_active(fhidefuture) && sda>tnow){ret=0;}


    if(!ret)
    {
        GtkTreeIter child;


        if(gtk_tree_model_iter_children(model,&child,iter))
        {
            do
                {
                    gboolean dret = row_visible (model, &child, store);
                    if(dret)
                    {
                        ret=1;break;
                    }
                } while(gtk_tree_model_iter_next(model,&child));
        }
    }

    if(gtk_toggle_button_get_active(fhidedone) && fda>0){ret=0;}
    if(gtk_toggle_button_get_active(fhideinactive) && fda<0){ret=0;}
    if(gtk_toggle_button_get_active(fhidefuture) && sda>tnow){ret=0;}




    if(ret)
    {




    }

    g_free(tname);
    g_free(notes);
    g_free(ptitle);
    g_free(pcolor);
    return ret;
}



gboolean tickover2 (gpointer data)
{
    time_t tnow=time(0);
    struct tm tday=*localtime(&tnow);
    static int curday=0;
    static int lastminute=0;


        tnow=time(0);
        tday=*localtime(&tnow);


        if(curday!=tday.tm_mday)
        {
            if(!curday){curday=tday.tm_mday;pthread_mutex_unlock(&lock);return TRUE;}
            sleep(3); //ensure time ticks past one second after midnight
            tdbupdate();
            gtk_widget_hide(swindow);
            GtkTreeSelection * sel1 = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
            char * tbuf=NULL;
            zcpy(&tbuf,0,"");
            gtk_tree_selection_selected_foreach(sel1, storesel, &tbuf);
            gtk_tree_store_clear(maintreestore);
            sqlite3_open(curdatfile,&sgp);
            sqlite3_stmt * sst;
            sqlite3_prepare_v2(sgp,"select * from tasks",-1,&sst,NULL);
            checksst(sst,0,NULL);
            gtk_tree_model_foreach(gtf, checkxp, NULL);
            sqlite3_finalize(sst);
            gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
            rfilter();
            char * rbuf=tbuf;
            while(strchr(rbuf,'|'))
            {
                rbuf=strchr(rbuf,'|');
                rbuf++;
                int usel=atol(rbuf);
                gtk_tree_model_foreach(gtf, resel, usel);
                GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
                onSelectionChanged(sel,NULL);
            }
            free(tbuf);
            GtkTreeSortable * sortable = GTK_TREE_SORTABLE(maintreestore);
            gtk_tree_sortable_set_sort_column_id(sortable, 99, GTK_SORT_DESCENDING);
            gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);
            gtk_widget_show(swindow);
            sqlite3_close(sgp);

            curday=tday.tm_mday;
        }

        if(tday.tm_min!=lastminute)
        {
            lastminute=tday.tm_min;
            //tnow+=(60*60) * (tday.tm_isdst);
            gtk_tree_model_foreach(maintreestore, checkal, NULL);
        }

        return TRUE;
}



int tickover()
{
    time_t tnow=time(0);
    struct tm tday=*localtime(&tnow);
    int curday=tday.tm_mday;
    int lastminute=0;

    while(1)
    {
        sleep(1);
        tnow=time(0);
        tday=*localtime(&tnow);

        if(curday!=tday.tm_mday)
        {
            gtk_tree_model_foreach(GTK_TREE_MODEL(maintreestore), fupdate, NULL);
            curday=tday.tm_mday;
        }

        if(tday.tm_min!=lastminute)
        {
            lastminute=tday.tm_min;
            GtkTreeIter iter;
        }








    }







}

static gboolean on_output (GtkSpinButton *spin,gpointer data)
{
   GtkAdjustment *adj;
   gchar *text;
   int value;

   adj = gtk_spin_button_get_adjustment (spin);
   value = (int)gtk_adjustment_get_value (adj);
   text = g_strdup_printf ("%02d", value);
   gtk_entry_set_text (GTK_ENTRY (spin), text);
   g_free (text);

   return TRUE;
}




void gfix(GtkWidget * f, GtkWidget * g,gint x, gint y, gint l, gint w)
{
    gtk_fixed_put(f,g,x hh,y vv);
    gtk_widget_set_size_request(g,l hh,w vv);
}


void ftext(GtkWidget * g,int psize,const char * c)
{
    int tsize = (psize * 1000) * fontfactor;
    sprintf(ubuf,"<span font-size='%i'>%s</span>",tsize,c);
    gtk_label_set_markup(g,ubuf);
}

void fctext(GtkWidget * g,int psize,const char * c,const char * clr)
{
    int tsize = (psize * 1000) * fontfactor;
    sprintf(ubuf,"<span font-size='%i' color='%s'>%s</span>",tsize,clr,c);
    gtk_label_set_markup(g,ubuf);
}

void newtagory(GtkWidget * g)
{
    GtkWidget * tagorytext = gtk_bin_get_child(tagory);
    gtk_entry_set_text(tagorytext,"");
    gtk_widget_grab_focus(tagorytext);
}



void unpad(GtkWidget * g)
{
    context = gtk_widget_get_style_context(g);
    gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(provider0),GTK_STYLE_PROVIDER_PRIORITY_USER);
}






int main (int argc, char *argv[])
{
  pthread_mutexattr_init(&Attr);
  pthread_mutexattr_settype(&Attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(&lock, &Attr);
  pthread_mutex_init(&zlock, &Attr);



  GtkWidget *button = NULL;
  GtkWidget *vbox = NULL;
  GtkWidget *label = NULL;

  gtk_init (&argc, &argv);


  int fd = open("/dev/null",02);
 if(fd)
 {
     dup2(fd,0);
     dup2(fd,1);
     dup2(fd,2);
    if (fd > 2)
    close(fd);
 }


  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width (GTK_CONTAINER (win), 1);
  gtk_window_set_title (GTK_WINDOW (win), "Finitodo");
  gtk_window_set_default_size(win,gdk_screen_width()-100,gdk_screen_height());
  gtk_window_set_resizable(win,FALSE);
  g_signal_connect (win, "destroy", gtk_main_quit, NULL);

  GdkScreen * gds=gdk_screen_get_default();
  GtkSettings *settings = gtk_settings_get_default();
  gint xdp;
  g_object_get(settings, "gtk-xft-dpi", &xdp, NULL);
  ddpi=(double)xdp/98304;

  gint gsf=gdk_window_get_scale_factor(win);
  sscf=gsf;
  GdkRectangle gdr;
  gdk_screen_get_monitor_workarea(gds,0,&gdr);

  gtk_window_move(win,gdr.x,gdr.y);
  hhorizontal=(double)gdr.width/(double)1920;
  vvertical=(double)gdr.height/(double)1043;

  fontfactor = hhorizontal < vvertical ? hhorizontal : vvertical;
  fontfactor/=ddpi;

  provider0 = gtk_css_provider_new();
  gtk_css_provider_load_from_data(provider0,"button {padding:0}",-1, NULL);

  GdkDisplay * display = gdk_display_get_default ();
  GdkScreen * screen = gdk_display_get_default_screen (display);
  GtkCssProvider * providerf = gtk_css_provider_new();
  gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (providerf), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  sprintf(ubuf,"calendar {font-size:%fpt} combobox text {font-size:4em;} combobox entry {min-width:1px;padding-left:10px;} checkbutton {padding:0px;min-width:0;} spinbutton {padding:0px;min-height:0;font-size:%fpt;} button:focus {background-color:rgba(0,0,0,0);color:black;} button:active {background-color:#3355aa;} button {padding:2px;min-height:0;} entry {min-width:0;font-size:%fpt;padding-left:10px;min-height:0;} label {font-size:%fpt;padding:0} textview {font-size:%fpt;padding-left:10px;padding-right:10px;} ",fontfactor*12,fontfactor*12,fontfactor*12,fontfactor*12,fontfactor*12);
  gtk_css_provider_load_from_data(providerf,ubuf,-1, NULL);

  GtkWidget * gfx = gtk_fixed_new();
  gtk_container_add(win,gfx);



  gftop = gtk_fixed_new();
  gfix(gfx,gftop,0,0,1914,100);

  button = gtk_button_new_with_label("");
  GtkWidget * bla=gtk_bin_get_child(button);
  ftext(bla,20,"\342\227\274\n+");
  unpad(button);
  gfix(gftop,button,110,10,75,75);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (newtask), 1);
  label = gtk_label_new("");
  gtk_label_set_markup(label,"<span font-weight='bold'>   New\nSubtask</span>");
  gfix(gftop,label,110,85,75,20);
  GtkCssProvider *providernt2 = gtk_css_provider_new();
  sprintf(ubuf,"button {background-image: none;} button {background-color:#eebbee;color:black;} button:focus {background-color:#eebbee;color:black;} button:active {background-color:#3355aa}");
  gtk_css_provider_load_from_data(providernt2,ubuf,-1, NULL);
  context = gtk_widget_get_style_context(button);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providernt2),GTK_STYLE_PROVIDER_PRIORITY_USER);






  button = gtk_button_new_with_label("+");
  ntbutton=button;
  bla=gtk_bin_get_child(button);
  ftext(bla,40,"+");
  unpad(button);
  gfix(gftop,button,10,10,75,75);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (newtask), 0);
  label = gtk_label_new("New Task");
  gtk_label_set_markup(label,"<span font-weight='bold'>New\nTask</span>");
  gfix(gftop,label,10,85,75,20);

  GtkCssProvider *providernt1 = gtk_css_provider_new();
  sprintf(ubuf,"label {font-size:%fpt} button {background-image: none;} button {background-color:#aaddff} button:focus {background-color:#aaddff;color:black;} button:active {background-color:#3355aa}",(40*fontfactor));
  gtk_css_provider_load_from_data(providernt1,ubuf,-1, NULL);
  context = gtk_widget_get_style_context(button);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providernt1),GTK_STYLE_PROVIDER_PRIORITY_USER);


  button = gtk_button_new_with_label("2");
  bla=gtk_bin_get_child(button);
  ftext(bla,20,"\342\227\274+");
  unpad(button);
  gfix(gftop,button,210,10,75,75);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (newtask), 2);
  label = gtk_label_new("");
  gtk_label_set_markup(label,"<span font-weight='bold'>   New\nSibtask</span>");
  gfix(gftop,label,210,85,75,20);
  GtkCssProvider *providernt3 = gtk_css_provider_new();
  sprintf(ubuf,"button {background-image: none;} button {background-color:#bbeeee;color:black;} button:focus {background-color:#bbeeee;color:black;} button:active {background-color:#3355aa}");
  gtk_css_provider_load_from_data(providernt3,ubuf,-1, NULL);
  context = gtk_widget_get_style_context(button);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providernt3),GTK_STYLE_PROVIDER_PRIORITY_USER);


   button = gtk_button_new_with_label("\342\234\224");
   bla=gtk_bin_get_child(button);
   unpad(button);
   gfix(gftop,button,550,10,75,75);
   g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (ctask), 0);
   label = gtk_label_new("");
   gtk_label_set_markup(label,"<span font-weight='bold'>   Toggle\nComplete</span>");
   gfix(gftop,label,550,85,75,20);

   GtkCssProvider *providercom = gtk_css_provider_new();
   sprintf(ubuf,"label {font-size:%fpt} button {background-image: none;}  button {background-color:#ddffdd} button:focus {background-color:#ddffdd;color:black;} button:active {background-color:#3355aa}",(fontfactor * 30));
   gtk_css_provider_load_from_data(providercom,ubuf,-1, NULL);
   context = gtk_widget_get_style_context(button);
   gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providercom),GTK_STYLE_PROVIDER_PRIORITY_USER);
   context = gtk_widget_get_style_context(bla);
   gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providercom),GTK_STYLE_PROVIDER_PRIORITY_USER);


   button = gtk_button_new_with_label("");
   bla=gtk_bin_get_child(button);
   ftext(bla,30,"\342\244\264");
   unpad(button);
   gfix(gftop,button,725,10,75,75);
   g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (chapar), 0);
   label = gtk_label_new("");
   gtk_label_set_markup(label,"<span font-weight='bold'>Change\nParent</span>");
   gfix(gftop,label,725,85,75,20);
   GtkCssProvider *providerchap = gtk_css_provider_new();
   sprintf(ubuf,"button {background-image: none;}  button {background-color:#eeeeaa;color:black;} button:focus {background-color:#eeeeaa;color:black;} button:active {background-color:#3355aa}");
   gtk_css_provider_load_from_data(providerchap,ubuf,-1, NULL);
   context = gtk_widget_get_style_context(button);
   gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerchap),GTK_STYLE_PROVIDER_PRIORITY_USER);


  button = gtk_button_new_with_label("2");
  bla=gtk_bin_get_child(button);
  ftext(bla,20,"\342\227\274\342\227\274");
  unpad(button);
  gfix(gftop,button,330,10,75,75);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (duptask2), 4);
  label = gtk_label_new("");
  gtk_label_set_markup(label,"<span font-weight='bold'>Duplicate\n     Task</span>");
  gfix(gftop,label,330,85,75,20);
  GtkCssProvider *providerdup = gtk_css_provider_new();
  sprintf(ubuf,"button {background-image: none;background-color: @bg_color;} button {background-color:#aabbee;color:black;} button:focus {background-color:#aabbee;color:black;} button:active {background-color:#3355aa}");
  gtk_css_provider_load_from_data(providerdup,ubuf,-1, NULL);
  context = gtk_widget_get_style_context(button);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerdup),GTK_STYLE_PROVIDER_PRIORITY_USER);

   button = gtk_button_new_with_label("\342\234\226");
   bla=gtk_bin_get_child(button);
   unpad(button);
   gfix(gftop,button,900,10,75,75);
   g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (deltask), 0);
   label = gtk_label_new("");
   gtk_label_set_markup(label,"<span font-weight='bold'>Delete\n Task</span>");
   gfix(gftop,label,900,85,75,20);
   GtkCssProvider *providerdel = gtk_css_provider_new();
   sprintf(ubuf,"label {font-size:%fpt} button {background-image: none;background-color: @bg_color;} button {background-color:#ff5555} button:focus {background-color:#ff5555;color:black;} button:active {background-color:#3355aa} ",(fontfactor * 30));
   gtk_css_provider_load_from_data(providerdel,ubuf,-1, NULL);
   context = gtk_widget_get_style_context(button);
   gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerdel),GTK_STYLE_PROVIDER_PRIORITY_USER);
   context = gtk_widget_get_style_context(bla);
   gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerdel),GTK_STYLE_PROVIDER_PRIORITY_USER);





   button = gtk_button_new_with_label("");
   bla=gtk_bin_get_child(button);
   ftext(bla,30,"\342\244\222");
   unpad(button);
   gfix(gftop,button,1180,10,75,75);
   g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (tryopen), 0);
   label = gtk_label_new("");
   gtk_label_set_markup(label,"<span font-weight='bold'>Open File</span>");
   gfix(gftop,label,1180,85,75,20);


   button = gtk_button_new_with_label("");
   bla=gtk_bin_get_child(button);
   ftext(bla,30,"\342\244\223");
   unpad(button);
   gfix(gftop,button,1320,10,75,75);
   g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (trysave), 0);
   label = gtk_label_new("");
   gtk_label_set_markup(label,"<span font-weight='bold'>Save As</span>");
   gfix(gftop,label,1320,85,75,20);


   helpbutton = gtk_button_new_with_label("?");
   bla=gtk_bin_get_child(helpbutton);
   ftext(bla,30,"?");
   unpad(helpbutton);
   gfix(gftop,helpbutton,1530,10,75,75);
   g_signal_connect (G_OBJECT (helpbutton), "clicked", G_CALLBACK (helpscreen), (gpointer) win);
   label = gtk_label_new("");
   gtk_label_set_markup(label,"<span font-weight='bold'>Help</span>");
   gfix(gftop,label,1530,85,75,20);
   gfix(gftop,label,1530,85,75,20);


   GtkCssProvider *providertest = gtk_css_provider_new();
   sprintf(ubuf,"button {background-image: none;} button {font-size:%fem} button:focus {background-color:rgba(0,0,0,0);color:black;} button:active {background-color:green}",(fontfactor * 3));
   gtk_css_provider_load_from_data(providertest,ubuf,-1, NULL);
   context = gtk_widget_get_style_context(helpbutton);
   gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providertest),GTK_STYLE_PROVIDER_PRIORITY_USER);


  button = gtk_button_new_with_label("");
  bla=gtk_bin_get_child(button);
  fctext(bla,30,"Q","red");
  unpad(button);
  gfix(gftop,button,1820,10,75,75);
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (immquit), 0);
  label = gtk_label_new("");
  gtk_label_set_markup(label,"<span font-weight='bold'>Quit</span>");
  gfix(gftop,label,1820,85,75,20);

  GdkColor lcolor;lcolor.red = 0x0000;lcolor.green = 0x0000;lcolor.blue = 0x0000;
  GtkWidget * hline = gtk_vbox_new(TRUE,0);
  gfix(gftop,hline,0,139,1914,1);
  gtk_widget_modify_bg(hline,GTK_STATE_NORMAL,&lcolor);


 GtkTreeStore *treestore;

 GtkTreeSortable *sortable;
 GtkCellRenderer *renderer, *lrenderer, *togrenderer, *nrenderer;
 GtkTreeViewColumn *column;


 maintreestore = gtk_tree_store_new(29, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,G_TYPE_DOUBLE,G_TYPE_DOUBLE,G_TYPE_DOUBLE,G_TYPE_DOUBLE,G_TYPE_INT,G_TYPE_STRING,G_TYPE_STRING,G_TYPE_INT,G_TYPE_INT,G_TYPE_INT,G_TYPE_STRING,G_TYPE_INT,G_TYPE_INT,G_TYPE_DOUBLE,G_TYPE_DOUBLE,G_TYPE_STRING,G_TYPE_INT,G_TYPE_DOUBLE,G_TYPE_INT,G_TYPE_INT,G_TYPE_INT,G_TYPE_INT);
 sortable = GTK_TREE_SORTABLE(maintreestore);
 gtf= gtk_tree_model_filter_new(GTK_TREE_MODEL (maintreestore), NULL);
 gtk_tree_model_filter_set_visible_func (gtf,(GtkTreeModelFilterVisibleFunc) row_visible,maintreestore, NULL);
 trunk = gtk_tree_view_new_with_model(gtf);
 gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(trunk), TRUE);



ebch =gtk_event_box_new();
labelch = gtk_label_new("");
ftext(labelch,20,"Click on new parent task, or here for top-level");
gtk_container_add(ebch,labelch);
g_signal_connect (G_OBJECT (ebch), "button-press-event", G_CALLBACK (chaparz), 0);
gfix(gfx,ebch,0,140,700,50);

GdkColor color;

color.red = 0xeedd;color.green = 0xeedd;color.blue = 0x9999;
gtk_widget_modify_bg(labelch,GTK_STATE_NORMAL,&color);
color.red = 0x1111;color.green = 0x1111;color.blue = 0xffff;
gtk_widget_modify_fg(labelch,GTK_STATE_NORMAL,&color);


color.red = 0xdddd;
color.green = 0xdddd;
color.blue = 0xbbbb;


ebch2 =gtk_event_box_new();
labelch = gtk_label_new("");
ftext(labelch,20,"Click here to cancel");
gtk_container_add(ebch2,labelch);
g_signal_connect (G_OBJECT (ebch2), "button-press-event", G_CALLBACK (chaparx), 0);
gfix(gfx,ebch2,700,140,400,50);

color.red = 0xcccc;color.green = 0xcccc;color.blue = 0xcccc;
gtk_widget_modify_bg(labelch,GTK_STATE_NORMAL,&color);
color.red = 0xeeee;color.green = 0x0000;color.blue = 0x0000;
gtk_widget_modify_fg(labelch,GTK_STATE_NORMAL,&color);


labelt=gtk_label_new("Task List");
ftext(labelt,20,"Task List");
gfix(gfx,labelt,0,140,1100,50);


color.red = 0xdddd;color.green = 0xdddd;color.blue = 0xbbbb;
gtk_widget_modify_bg(labelt,GTK_STATE_NORMAL,&color);

swindow = gtk_scrolled_window_new (NULL, NULL);
gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (swindow),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
gtk_container_add (GTK_CONTAINER (swindow), trunk);
gtk_widget_show (trunk);
gfix(gfx,swindow,0,190,1100,820);
gtk_widget_show (swindow);


 togrenderer = gtk_cell_renderer_toggle_new();

  column = gtk_tree_view_column_new_with_attributes("",togrenderer, "active", col_taskdoneint, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  g_signal_connect(togrenderer, "toggled", G_CALLBACK(onRowToggled), NULL);
  g_signal_connect(trunk, "columns-changed", G_CALLBACK(onColumnsReordered), NULL);
  gtk_cell_renderer_set_alignment(togrenderer,0.5,0.5);
  chekcol=column;
  colarray[7]=column;
  gtk_tree_view_column_set_reorderable(column,FALSE);


 lrenderer = gtk_cell_renderer_text_new ();
 nrenderer = gtk_cell_renderer_text_new ();
 renderer = gtk_cell_renderer_text_new ();

  column = gtk_tree_view_column_new_with_attributes("   Title",lrenderer, "text", 0, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_sort_column_id(column,0);
  gtk_tree_view_column_set_resizable(column,TRUE);
  gtk_tree_view_column_set_reorderable(column,FALSE);
  gtk_tree_view_column_set_expand(column,TRUE);


  gtk_tree_sortable_set_sort_func(sortable, 0, headerclick,GINT_TO_POINTER(5), NULL);
  colarray[5]=column;
  g_signal_connect_after((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),GINT_TO_POINTER(5));
  //g_signal_connect((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),GINT_TO_POINTER(5));
  g_signal_connect(G_OBJECT(trunk), "enter-notify-event", G_CALLBACK(trunkfocus),NULL);
  gtk_tree_view_column_set_fixed_width(colarray[5],320 hh);
  gtk_tree_view_set_expander_column(trunk,column);



  GtkCellRenderer *crenderer;
  crenderer = gtk_cell_renderer_text_new ();
  gtk_cell_renderer_set_alignment(crenderer,0.5,0.5);
  column = gtk_tree_view_column_new_with_attributes(" Priority        ",lrenderer, "text",12, NULL);
  gtk_tree_view_column_set_alignment(column,0.51);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_resizable(column,TRUE);
  gtk_tree_view_column_set_reorderable(column,TRUE);
  gtk_tree_view_column_set_sort_column_id(column,12);
  gtk_tree_sortable_set_sort_func(sortable, 99, multisort,GINT_TO_POINTER(99), NULL);
  gtk_tree_sortable_set_sort_func(sortable, 11, multisort,GINT_TO_POINTER(11), NULL);
  gtk_tree_sortable_set_sort_func(sortable, 12, headerclick,GINT_TO_POINTER(3), NULL);
  colarray[3]=column;
  g_signal_connect_after((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),3);
  gtk_tree_view_column_set_fixed_width(colarray[3],100 hh);



  column = gtk_tree_view_column_new_with_attributes(" Due Date",lrenderer, "text", 3, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_resizable(column,TRUE);
  gtk_tree_view_column_set_reorderable(column,TRUE);
  gtk_tree_view_column_set_sort_column_id(column,8);
  gtk_tree_sortable_set_sort_func(sortable, 8, headerclick,GINT_TO_POINTER(2), NULL);
  colarray[2]=column;
  g_signal_connect_after((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),2);
  gtk_tree_view_column_set_fixed_width(colarray[2],170 hh);

  column = gtk_tree_view_column_new_with_attributes(" Start Date",lrenderer, "text", 2, NULL);
  gtk_tree_view_column_set_resizable(column,TRUE);
  gtk_tree_view_column_set_reorderable(column,TRUE);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_sort_column_id(column,7);
  gtk_tree_sortable_set_sort_func(sortable, 7, headerclick,GINT_TO_POINTER(4), NULL);
  colarray[4]=column;
  g_signal_connect_after((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),4);
  gtk_tree_view_column_set_fixed_width(colarray[4],170 hh);

  column = gtk_tree_view_column_new_with_attributes(" Category",lrenderer, "text", 6, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_resizable(column,TRUE);
  gtk_tree_view_column_set_reorderable(column,TRUE);
  gtk_tree_view_column_set_expand(column,TRUE);
  gtk_tree_view_column_set_sort_column_id(column,6);
  gtk_tree_sortable_set_sort_func(sortable, 6, headerclick,GINT_TO_POINTER(6), NULL);
  colarray[6]=column;
  g_signal_connect_after((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),GINT_TO_POINTER(6));
  g_signal_connect(G_OBJECT(trunk), "enter-notify-event", G_CALLBACK(trunkfocus),NULL);
  gtk_tree_view_column_set_fixed_width(colarray[6],100 hh);



  column = gtk_tree_view_column_new_with_attributes(" Status",lrenderer, "text", 13, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_resizable(column,TRUE);
  gtk_tree_view_column_set_reorderable(column,TRUE);
  gtk_tree_view_column_set_sort_column_id(column,13);
  gtk_tree_sortable_set_sort_func(sortable, 13, headerclick,GINT_TO_POINTER(1), NULL);
  colarray[1]=column;
  g_signal_connect_after((G_OBJECT(column)), "clicked", G_CALLBACK(headerclick),1);
  gtk_tree_view_column_set_fixed_width(colarray[1],140 hh);


  GtkCssProvider *providerhed = gtk_css_provider_new();
  gtk_css_provider_load_from_data(providerhed,"button {color:black;background:#e0e0ee;} button:active {color:blue} button:hover {color:blue}",-1, NULL);

  for(int x=1;x<=7;x++)
  {
      if(x<7){gtk_tree_view_column_set_min_width(colarray[x],100 hh);}
      else{gtk_tree_view_column_set_min_width(colarray[x],30 hh);}
      GtkWidget * cb = gtk_tree_view_column_get_button(colarray[x]);
      context = gtk_widget_get_style_context(cb);
      gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerhed),GTK_STYLE_PROVIDER_PRIORITY_USER);
  }

  column = gtk_tree_view_column_new_with_attributes("wcol",lrenderer,"text",0, "weight",21,"weight_set",TRUE, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_visible(column,FALSE);

  gtk_tree_view_column_set_attributes(column,crenderer,"text",12,"weight",21,"weight_set",TRUE,NULL);

  column = gtk_tree_view_column_new_with_attributes("fcol",lrenderer,"text",0, "foreground",22,"foreground-set",TRUE, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(trunk), column);
  gtk_tree_view_column_set_visible(column,FALSE);

  g_signal_connect(trunk, "row-activated", G_CALLBACK(onRowActivated), NULL);
  g_signal_connect(trunk, "row-expanded", G_CALLBACK(onRowExpanded), NULL);
  g_signal_connect(trunk, "row-collapsed", G_CALLBACK(onRowCollapsed), NULL);
  g_signal_connect(trunk, "button-release-event", G_CALLBACK(onRowReleased), NULL);
  gtk_tree_selection_set_mode(gtk_tree_view_get_selection(trunk),GTK_SELECTION_MULTIPLE);

  label=gtk_label_new("Details");
  labeltest=label;
  ftext(label,20,"Details");
  gfix(gfx,label,1100,140,430,50);
  color.red = 0xaaaa;color.green = 0xbaaa;color.blue = 0xaaaa;
  gtk_widget_modify_bg(label,GTK_STATE_NORMAL,&color);


  gf2 = gtk_fixed_new();
  gfix(gfx,gf2,1120,190,400,850);


  gfh=gtk_fixed_new();
  gfix(gfx,gfh,1100,140,810,940);
  label = gtk_label_new("");
  gfix(gfh,label,0,0,810,940);
  color.red = 0xdddd;color.green = 0xdddd;color.blue = 0xffff;
  gtk_widget_modify_bg(label,GTK_STATE_NORMAL,&color);
  gtk_misc_set_alignment(GTK_MISC(label), 0.1, 0.02);
    int tsize = 15000 * fontfactor;
    int hl=10;
    label=gtk_label_new("");ftext(label,18,"<span font-weight='bold'>Finitodo 2.0-31 </span>");gfix(gfh,label,10,hl,100,20);hl+=60;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"<span underline='single' font-weight='bold'>Keyboard Shortcuts</span>");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);hl+=60;
    label=gtk_label_new("");ftext(label,15,"Insert");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Add New Task (at top/root level)");gfix(gfh,label,300,hl,100,20);hl+=30;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Ctrl-Insert");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Add New Subtask (under selected task)");gfix(gfh,label,300,hl,100,20);hl+=30;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Ctrl-Shift-Insert");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Add New Sibtask (at same level as selected task)");gfix(gfh,label,300,hl,100,20);hl+=60;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Enter or Space");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Toggle task(s) active/done");gfix(gfh,label,300,hl,100,20);hl+=60;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Shift-RightArrow");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Expand task to show subtasks");gfix(gfh,label,300,hl,100,20);hl+=30;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Shift-LeftArrow");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Collapse task to hide subtasks");gfix(gfh,label,300,hl,100,20);hl+=60;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"<span underline='single' font-weight='bold'>Mouse Actions</span>");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);hl+=30;
    label=gtk_label_new("");ftext(label,15,"Double-Click");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Toggle task active/done");gfix(gfh,label,300,hl,100,20);hl+=30;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Ctrl-Click");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Select/Unselect multiple tasks from list");gfix(gfh,label,300,hl,100,20);hl+=60;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"<span underline='single' font-weight='bold'>Category Filter</span>");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);hl+=30;
    label=gtk_label_new("");ftext(label,15,"Ctrl-Click");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"Select multiple categories to filter/show");gfix(gfh,label,300,hl,100,20);hl+=90;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
    label=gtk_label_new("");ftext(label,15,"<span underline='single' font-weight='bold'>Search Filter</span>");gfix(gfh,label,10,hl,100,20);gtk_misc_set_alignment(GTK_MISC(label), 0, .5);hl+=30;
    label=gtk_label_new("");ftext(label,15,"Type search terms separated by spaces for multiple matches");gfix(gfh,label,10,hl,100,20);hl+=30;gtk_misc_set_alignment(GTK_MISC(label), 0, .5);





    zcpy(&ebuf,1,"</span>");
    gtk_widget_hide(gfh);





  label = gtk_label_new("Task title");
  gfix(gf2,label,10,5,490,20);
  gtk_misc_set_alignment(GTK_MISC(label), 0, .5);

  donelabel = gtk_label_new("\342\234\224 Done");
  gfix(gf2,donelabel,200,5,90,20);
  gtk_misc_set_alignment(GTK_MISC(donelabel), 0, .5);



  tentry = gtk_entry_new();
  gtk_entry_set_max_length (GTK_ENTRY (tentry),100);
  gtk_entry_set_width_chars(tentry,5);
  gfix(gf2,tentry,5,30,390,40);
  gtk_widget_add_events(tentry,GDK_FOCUS_CHANGE_MASK);
  g_signal_connect(G_OBJECT(tentry), "changed", G_CALLBACK(immchange), NULL);
  g_signal_connect(G_OBJECT(tentry), "grab-focus", G_CALLBACK(tentrymulti), NULL);
  g_signal_connect(G_OBJECT(tentry), "focus-out-event", G_CALLBACK(entryunhighlight), NULL);



  label = gtk_label_new("Priority" );
  gfix(gf2,label,30,78,50,40);
  GtkWidget * hbox3 = gtk_hbox_new(FALSE,10);
  pentry=gtk_spin_button_new_with_range(-100,100,1);
  gfix(gf2,hbox3,150,78,235,40);
  gtk_box_pack_start(hbox3,pentry,TRUE,TRUE,0);

  GtkCssProvider *providerpentry = gtk_css_provider_new();
  sprintf(ubuf,"button {background-image: none;} button {background-color:#dddddd;color:black;border:1px solid grey} button:focus {background-color:#dddddd;color:black;} button:active {background-color:#3355aa} button:disabled {background-color:#fefefe;color:#dddddd;border:1px solid #dddddd}");
  gtk_css_provider_load_from_data(providerpentry,ubuf,-1, NULL);
  context = gtk_widget_get_style_context(pentry);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerpentry),GTK_STYLE_PROVIDER_PRIORITY_USER);




  GList *children3 = gtk_container_get_children(GTK_CONTAINER(hbox3));
  for (const GList *iter = children3; iter != NULL; iter = g_list_next(iter))
  {
      GtkWidget * g=(GTK_ENTRY(iter->data));
      gtk_widget_add_events(GTK_ENTRY(g),GDK_FOCUS_CHANGE_MASK);
      g_signal_connect(GTK_ENTRY(g), "changed", G_CALLBACK(immchange), NULL);
      g_signal_connect(GTK_ENTRY(g), "focus-out-event", G_CALLBACK(entryunhighlight), NULL);
      pentrybox=g;
  }
    g_list_free(children3);

  tib=gtk_check_button_new_with_label("Inactive");
  gfix(gf2,tib,300,5,90,20);
  g_signal_connect (G_OBJECT (tib), "clicked", G_CALLBACK (immchange), 0);
  g_signal_connect(G_OBJECT(tib), "focus-in-event", G_CALLBACK(tibforward), NULL);
  g_signal_connect (G_OBJECT (tib), "button-press-event", G_CALLBACK (tibpressed), NULL);




  ddb=gtk_check_button_new_with_label("Due Date");
  gfix(gf2,ddb,10,130,100,40);
  g_signal_connect (G_OBJECT (ddb), "clicked", G_CALLBACK (togdue), 0);
  ddentry = gtk_entry_new();
  gtk_entry_set_width_chars(ddentry,5);
  gfix(gf2,ddentry,150,130,170,40);
  gtk_widget_add_events(ddentry,GDK_FOCUS_CHANGE_MASK);
  g_signal_connect(G_OBJECT(ddentry), "changed", G_CALLBACK(immchange), 333);
  g_signal_connect(G_OBJECT(ddentry), "focus-out-event", G_CALLBACK(entryunhighlight), NULL);
  ddp = gtk_button_new_with_label("Pick");
  g_signal_connect (G_OBJECT (ddp), "clicked", G_CALLBACK (showcal), 0);
  gfix(gf2,ddp,320,130,65,40);

  gf3 = gtk_fixed_new();
  gfix(gf2,gf3,0,140,500,700);

  sdb=gtk_check_button_new_with_label("Start Date");
  gfix(gf3,sdb,10,40,100,40);
  g_signal_connect (G_OBJECT (sdb), "clicked", G_CALLBACK (togsta), 0);
  sdentry = gtk_entry_new();
  gtk_entry_set_width_chars(sdentry,5);
  gfix(gf3,sdentry,150,40,170,40);
  gtk_widget_add_events(sdentry,GDK_FOCUS_CHANGE_MASK);
  g_signal_connect(G_OBJECT(sdentry), "changed", G_CALLBACK(immchange), 333);
  g_signal_connect(G_OBJECT(sdentry), "focus-out-event", G_CALLBACK(entryunhighlight), NULL);
  sdp = gtk_button_new_with_label("Pick");
  g_signal_connect (G_OBJECT (sdp), "clicked", G_CALLBACK (showcal), 1);
  gfix(gf3,sdp,320,40,65,40);
  g_signal_connect(G_OBJECT(sdentry), "grab-focus", G_CALLBACK(unfhidefuture), NULL);



  gf6=gtk_fixed_new();
  gfix(gf3,gf6,0,100,400,80);

  adb=gtk_check_button_new_with_label("Reminder");
  gfix(gf6,adb,10,15,100,40);
  g_signal_connect (G_OBJECT (adb), "clicked", G_CALLBACK (togrem), 0);
  adentry = gtk_entry_new();
  gtk_entry_set_width_chars(adentry,5);
  gfix(gf6,adentry,210,15,110,40);
  gtk_widget_add_events(adentry,GDK_FOCUS_CHANGE_MASK);
  g_signal_connect(G_OBJECT(adentry), "changed", G_CALLBACK(immchange), 0);
  adp = gtk_button_new_with_label("Pick");
  g_signal_connect (G_OBJECT (adp), "clicked", G_CALLBACK (showcala), 3);
  gfix(gf6,adp,320,15,65,40);
  gtk_entry_set_width_chars(ahour,2);
  ahour=gtk_spin_button_new_with_range(00,23,1);
  gtk_spin_button_set_wrap(ahour,TRUE);
  gtk_entry_set_width_chars(ahour,2);
  gfix(gf6,ahour,120,0,10,40);

  GtkCssProvider *providerahour = gtk_css_provider_new();
  sprintf(ubuf,"button {background-image: none;} button {background-color:#dddddd;color:black;} button:focus {background-color:#bbeeee;color:black;} button:active {background-color:#3355aa} button:disabled {background-color:#fefefe;color:#dddddd;border:1px solid #dddddd}");
  gtk_css_provider_load_from_data(providerahour,ubuf,-1, NULL);
  context = gtk_widget_get_style_context(ahour);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerahour),GTK_STYLE_PROVIDER_PRIORITY_USER);








  aminute = gtk_spin_button_new_with_range(00,59,1);
  gtk_spin_button_set_wrap(aminute,TRUE);
  gtk_entry_set_width_chars(aminute,2);
  gfix(gf6,aminute,160,0,10,40);

  context = gtk_widget_get_style_context(aminute);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providerahour),GTK_STYLE_PROVIDER_PRIORITY_USER);

  colabel = gtk_label_new(":");
  gfix(gf6,label,163,12,10,40);
  nonelabel=gtk_label_new("(None)");
  gfix(gf6,nonelabel,163,15,10,40);
  gtk_widget_hide(nonelabel);
 gtk_orientable_set_orientation(ahour,GTK_ORIENTATION_VERTICAL);
 g_signal_connect (G_OBJECT (ahour), "output", G_CALLBACK (on_output), 0);
 gtk_orientable_set_orientation(aminute,GTK_ORIENTATION_VERTICAL);
 g_signal_connect (G_OBJECT (aminute), "output", G_CALLBACK (on_output), 0);
 ampmlabel = gtk_label_new("01:30 pm");
 gfix(gf6,ampmlabel,218,50,100,40);
 g_signal_connect (GTK_ENTRY(aminute), "changed", G_CALLBACK (immchange), 0);
 g_signal_connect (GTK_ENTRY(ahour), "changed", G_CALLBACK (immchange), 0);
 if(hhorizontal <0.6)
 {
     gtk_fixed_move(gf6,ahour,90 hh,0);
     gtk_fixed_move(gf6,aminute,150 hh,0);
     GtkWidget * chlabel=gtk_bin_get_child(adb);
     gtk_label_set_text(GTK_LABEL(chlabel),"Rem");
 }




  gf4 = gtk_fixed_new();
  gfix(gf3,gf4,0,180,500,580);



  label = gtk_label_new("Recurrence");
  gfix(gf4,label,10,30,200,40);
  gtk_misc_set_alignment(GTK_MISC(label), 0, .5);

  GtkTreeModel * recmodel= gtk_tree_store_new(2, G_TYPE_INT, G_TYPE_STRING);
  comborty=gtk_combo_box_new_with_model(recmodel);

  double ffd= (double)12000 * (double)fontfactor;
  g_object_set (renderer, "xpad",5,NULL);
  g_object_set (lrenderer, "size",(gint)ffd,"size-set",TRUE, NULL);
  g_object_set (renderer, "size",(gint)ffd,"size-set",TRUE, NULL);
  g_object_set (togrenderer, "size",(gint)ffd,"size-set",TRUE, NULL);
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (comborty), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (comborty), renderer,"text", 1,NULL);

  GtkTreeIter itter,itter0;
  gtk_tree_store_append(recmodel, &itter0,NULL);
  gtk_tree_store_set(recmodel, &itter0,0,0,1,"None",-1);
  gtk_tree_store_append(recmodel, &itter,NULL);
  gtk_tree_store_set(recmodel, &itter,0,1,1,"Daily",-1);
  gtk_tree_store_append(recmodel, &itter,NULL);
  gtk_tree_store_set(recmodel, &itter,0,2,1,"Weekly",-1);
  gtk_tree_store_append(recmodel, &itter,NULL);
  gtk_tree_store_set(recmodel, &itter,0,3,1,"Monthly",-1);
  gtk_tree_store_append(recmodel, &itter,NULL);
  gtk_tree_store_set(recmodel, &itter,0,4,1,"Yearly",-1);
  gtk_tree_store_append(recmodel, &itter,NULL);
  gtk_tree_store_set(recmodel, &itter,0,5,1,"Specific weekdays",-1);
  gtk_tree_store_append(recmodel, &itter,NULL);
  gtk_tree_store_set(recmodel, &itter,0,6,1,"Month periodic",-1);
  gfix(gf4,comborty,150,30,235,40);
  gtk_combo_box_set_active(comborty,0);
  g_signal_connect(G_OBJECT(comborty), "changed", G_CALLBACK(immchange), NULL);

  hboxrec1 = gtk_fixed_new();
  gfix(gf4,hboxrec1,0,75,400,40);
  label = gtk_label_new("Every" );
  gfix(hboxrec1,label,0,0,140,40);
  gtk_misc_set_alignment(GTK_MISC(label), 1, .5);

  GtkWidget * rentrys=gtk_spin_button_new_with_range(1,99,1);
  gfix(hboxrec1,rentrys,150,0,60,40);

  GList *children4 = gtk_container_get_children(GTK_CONTAINER(hboxrec1));
  for (const GList *iter = children4; iter != NULL; iter = g_list_next(iter))
  {
      GtkWidget * g=(GTK_ENTRY(iter->data));
      gtk_widget_add_events(GTK_ENTRY(g),GDK_FOCUS_CHANGE_MASK);
      g_signal_connect(GTK_ENTRY(g), "changed", G_CALLBACK(immchange), NULL);
      rentry=g;
      gtk_entry_set_width_chars(rentry,2);
  }
  gtk_widget_set_size_request(rentrys,170 hh, 40 vv);
  g_list_free(children4);


  recsuffix = gtk_label_new("" );
  gfix(hboxrec1,recsuffix,335,0,60,40);
  hboxdays = gtk_fixed_new();
  gfix(gf4,hboxdays,0,75,400,40);
  const char * mday="Mon";
  const char * tday="Tue";
  const char * wday="Wed";
  const char * hday="Thu";
  const char * fday="Fri";
  const char * aday="Sat";
  const char * uday="Sun";
  if(hhorizontal<0.6)
  {
      mday="M";
      tday="T";
      wday="W";
      hday="T";
      fday="F";
      aday="S";
      uday="S";
  }
  weekday[1]=gtk_check_button_new_with_label(mday);
  weekday[2]=gtk_check_button_new_with_label(tday);
  weekday[3]=gtk_check_button_new_with_label(wday);
  weekday[4]=gtk_check_button_new_with_label(hday);
  weekday[5]=gtk_check_button_new_with_label(fday);
  weekday[6]=gtk_check_button_new_with_label(aday);
  weekday[7]=gtk_check_button_new_with_label(uday);

  int sp=60;
  gfix(hboxdays,weekday[1],0,0,20,40);
  gfix(hboxdays,weekday[2],62,0,20,40);
  gfix(hboxdays,weekday[3],114,0,20,40);
  gfix(hboxdays,weekday[4],176,0,20,40);
  gfix(hboxdays,weekday[5],233,0,20,40);
  gfix(hboxdays,weekday[6],288,0,20,40);
  gfix(hboxdays,weekday[7],344,0,20,40);
  wdtext=gtk_entry_new();
  gfix(gf4,hboxdays,0,390,0,40);
  gtk_widget_hide(wdtext);

  for(int x=1;x<8;x++)
  {g_signal_connect(G_OBJECT(weekday[x]), "clicked", G_CALLBACK(immchange), 1111111);}


  hboxmonth = gtk_fixed_new();
  gfix(gf4,hboxmonth,0,75,400,40);
  label = gtk_label_new("The");

  GtkTreeModel * recper=gtk_tree_store_new(2, G_TYPE_INT, G_TYPE_STRING);
  comboper=gtk_combo_box_new_with_model(recper);
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (comboper), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (comboper), renderer,"text", 1,NULL);

  gtk_tree_store_append(recper, &itter0,NULL);
  gtk_tree_store_set(recper, &itter0,0,0,1,"First",-1);
  gtk_tree_store_append(recper, &itter,NULL);
  gtk_tree_store_set(recper, &itter,0,1,1,"Second",-1);
  gtk_tree_store_append(recper, &itter,NULL);
  gtk_tree_store_set(recper, &itter,0,2,1,"Third",-1);
  gtk_tree_store_append(recper, &itter,NULL);
  gtk_tree_store_set(recper, &itter,0,3,1,"Fourth",-1);
  gtk_tree_store_append(recper, &itter,NULL);
  gtk_tree_store_set(recper, &itter,0,4,1,"Second-Last",-1);
  gtk_tree_store_append(recper, &itter,NULL);
  gtk_tree_store_set(recper, &itter,0,5,1,"Last",-1);
  gfix(hboxmonth,comboper,0,0,160,40);
  gtk_combo_box_set_active(comboper,0);
  g_signal_connect(G_OBJECT(comboper), "changed", G_CALLBACK(immchange), NULL);

  GtkTreeModel * recw=gtk_tree_store_new(2, G_TYPE_INT, G_TYPE_STRING);
  combow=gtk_combo_box_new_with_model(recw);

  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combow), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combow), renderer,"text", 1,NULL);

  gtk_tree_store_append(recw, &itter0,NULL);
  gtk_tree_store_set(recw, &itter0,0,0,1,"Monday",-1);
  gtk_tree_store_append(recw, &itter,NULL);
  gtk_tree_store_set(recw, &itter,0,1,1,"Tuesday",-1);
  gtk_tree_store_append(recw, &itter,NULL);
  gtk_tree_store_set(recw, &itter,0,2,1,"Wednesday",-1);
  gtk_tree_store_append(recw, &itter,NULL);
  gtk_tree_store_set(recw, &itter,0,3,1,"Thursday",-1);
  gtk_tree_store_append(recw, &itter,NULL);
  gtk_tree_store_set(recw, &itter,0,4,1,"Friday",-1);
  gtk_tree_store_append(recw, &itter,NULL);
  gtk_tree_store_set(recw, &itter,0,5,1,"Saturday",-1);
  gtk_tree_store_append(recw, &itter,NULL);
  gtk_tree_store_set(recw, &itter,0,6,1,"Sunday",-1);
  gfix(hboxmonth,combow,170,0,120,40);
  gtk_combo_box_set_active(combow,0);
  g_signal_connect(G_OBJECT(combow), "changed", G_CALLBACK(immchange), NULL);
  label = gtk_label_new("of month");
  gfix(hboxmonth,label,310,0,80,40);


  labelrba = gtk_label_new(" Calculate next due date from:" );
  gtk_misc_set_alignment(GTK_MISC(labelrba), 0, .5);
  gfix(gf4,labelrba,5,125,400,20);

  GtkTreeModel * recmodel2= gtk_tree_store_new(2, G_TYPE_INT, G_TYPE_STRING);
  comborba=gtk_combo_box_new_with_model(recmodel2);
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (comborba), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (comborba), renderer,"text", 1,NULL);
  gtk_tree_store_append(recmodel2, &itter0,NULL);
  gtk_tree_store_set(recmodel2, &itter0,0,0,1,"Due date (ensure new date is in future)",-1);
  gtk_tree_store_append(recmodel2, &itter,NULL);
  gtk_tree_store_set(recmodel2, &itter,0,1,1,"Due date (allow new date to be in past)",-1);
  gtk_tree_store_append(recmodel2, &itter,NULL);
  gtk_tree_store_set(recmodel2, &itter,0,2,1,"Completion date",-1);
  gtk_tree_store_append(recmodel2, &itter,NULL);
  gfix(gf4,comborba,5,155,400,40);
  gtk_combo_box_set_active(comborba,0);
  g_signal_connect(G_OBJECT(comborba), "changed", G_CALLBACK(immchange), NULL);


  ddc=gtk_calendar_new();
  gfix(gf2,ddc,10,230,380,200);
  g_signal_connect(G_OBJECT(ddc), "day-selected-double-click", G_CALLBACK(showcal), 2);

  g_signal_connect(gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk)), "changed", G_CALLBACK(onSelectionChanged), NULL);

  catelabel = gtk_label_new("Category" );
  gtk_misc_set_alignment(GTK_MISC(catelabel), 0, .5);
  gfix(gf4,catelabel,10,210,390,20);

  tagory = gtk_combo_box_text_new_with_entry ();
  GtkWidget * gb = gtk_bin_get_child(tagory);
  gtk_entry_set_width_chars(gb,5);
  gfix(gf4,tagory,5,235,330,40);
  //gtk_combo_box_text_append(tagory,0,"(None)");
  g_signal_connect(G_OBJECT(gb), "focus-out-event", G_CALLBACK(entryunhighlight), NULL);

  GList * cr = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (tagory));
  GtkCellRenderer * gcr = cr->data;
  g_object_set (gcr, "size",(gint)ffd,"size-set",TRUE, NULL);
  g_signal_connect(G_OBJECT(tagory), "changed", G_CALLBACK(immchange), 0);
  g_list_free(cr);

  acbutton = gtk_button_new_with_label("+");
  gfix(gf4,acbutton,340,235,40,40);
  g_signal_connect(G_OBJECT(acbutton), "clicked", G_CALLBACK(newtagory), 0);




tagslabel = gtk_label_new("Tags / Notes / Details");
gfix(gf4,tagslabel,5,290,390,20);
notesview=gtk_text_view_new();
GtkTextBuffer * vbuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (notesview));
gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(notesview),GTK_WRAP_WORD);
gtk_text_buffer_set_text (vbuffer, "",-1 );
g_signal_connect(G_OBJECT(vbuffer), "changed", G_CALLBACK(immchange), 2222222);
g_signal_connect(G_OBJECT(notesview), "grab-focus", G_CALLBACK(unfilter), NULL);
g_signal_connect(G_OBJECT(notesview), "focus-out-event", G_CALLBACK(entryunhighlight), NULL);


zwindow = gtk_scrolled_window_new (NULL, NULL);
gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (zwindow),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
gtk_container_add (GTK_CONTAINER (zwindow), notesview);
gtk_widget_show (notesview);
gfix(gf4,zwindow,5,320,375,160);
gtk_widget_show (zwindow);
gtk_scrolled_window_set_shadow_type(zwindow,3);

gf10 = gtk_fixed_new();
gfix(gfx,gf10,1530,140,384,900);

label=gtk_label_new("Options");
ftext(label,20,"Options");
gfix(gf10,label,0,0,384,50);

color.red = 0xbaaa;color.green = 0xaaaa;color.blue = 0xbaaa;
gtk_widget_modify_bg(label,GTK_STATE_NORMAL,&color);
color.red = 0x0000;color.green = 0x0000;color.blue = 0x0000;
label = gtk_label_new("");
gfix(gf10,label,0,0,1,940);
gtk_widget_modify_bg(label,GTK_STATE_NORMAL,&color);



GtkTreeIter siter;
slist = gtk_list_store_new(5, G_TYPE_STRING, G_TYPE_STRING,G_TYPE_INT,G_TYPE_INT, G_TYPE_STRING);


  gtk_list_store_append(slist, &siter);
  gtk_list_store_set(slist,&siter,0,"Status",1,"(ASC)",2,1,3,0,"");
  gtk_list_store_append(slist, &siter);
  gtk_list_store_set(slist,&siter,0,"Due Date",1,"(ASC)",2,2,3,0,"");
  gtk_list_store_append(slist, &siter);
  gtk_list_store_set(slist,&siter,0,"Priority",1,"(ASC)",2,3,3,0,"");
  gtk_list_store_append(slist, &siter);
  gtk_list_store_set(slist,&siter,0,"Start Date",1,"(ASC)",2,4,3,0,"");
  gtk_list_store_append(slist, &siter);
  gtk_list_store_set(slist,&siter,0,"Title",1,"(ASC)",2,5,3,0,"");
  gtk_list_store_append(slist, &siter);
  gtk_list_store_set(slist,&siter,0,"Category",1,"(ASC)",2,6,3,0,"");
  sortl = gtk_tree_view_new_with_model(slist);
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(sortl), TRUE);
  GtkTreeSortable * sortable2 = GTK_TREE_SORTABLE(slist);
  gtk_tree_sortable_set_sort_func(sortable2, 1, smallsort,GINT_TO_POINTER(1), NULL);


  GtkCssProvider *providercvhed = gtk_css_provider_new();
  gtk_css_provider_load_from_data(providercvhed,"button {color:black;background-color:#aaaaaa;}",-1, NULL);


  GtkWidget * sortl2 = gtk_tree_view_new();
  GtkTreeViewColumn * col = gtk_tree_view_column_new_with_attributes("Multi-Sort Order",renderer, "text", 0, NULL);

  gtk_tree_view_append_column(GTK_TREE_VIEW(sortl), col);
  gtk_tree_view_column_set_expand(col,TRUE);
  GtkWidget * cb = gtk_tree_view_column_get_button(col);
  context = gtk_widget_get_style_context(cb);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providercvhed),GTK_STYLE_PROVIDER_PRIORITY_USER);

  col = gtk_tree_view_column_new_with_attributes("",renderer, "text", 1, NULL);
  gtk_tree_view_column_set_expand(col,TRUE);
  gtk_tree_view_append_column(GTK_TREE_VIEW(sortl), col);
  cb = gtk_tree_view_column_get_button(col);
  context = gtk_widget_get_style_context(cb);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providercvhed),GTK_STYLE_PROVIDER_PRIORITY_USER);

  col = gtk_tree_view_column_new_with_attributes("",renderer, "text", 4, NULL);
  gtk_tree_view_column_set_expand(col,TRUE);
  gtk_tree_view_append_column(GTK_TREE_VIEW(sortl), col);
  cb = gtk_tree_view_column_get_button(col);
  context = gtk_widget_get_style_context(cb);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providercvhed),GTK_STYLE_PROVIDER_PRIORITY_USER);


    GtkWidget * msowindow = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (msowindow),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
    gtk_container_add (GTK_CONTAINER (msowindow), sortl);
    gtk_widget_show (sortl);
    gtk_widget_show (msowindow);
    gfix(gf10,msowindow,20,60,290,215);




  button = gtk_button_new_with_label("Up");
  g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(moveupdown1), 0);
  gfix(gf10,button,22,280,70,20);
  button = gtk_button_new_with_label("Down");
  gfix(gf10,button,94,280,70,20);
  g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(moveupdown1), 1);
  button = gtk_button_new_with_label("Flip");
  gfix(gf10,button,166,280,70,20);
  g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(flipfirst), NULL);
  button = gtk_button_new_with_label("Hide");
  gfix(gf10,button,238,280,70,20);
  g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(colvis), NULL);


 GtkWidget * gsn = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
 gfix(gf10,gsn,5,316,370,3);

 GtkTreeIter citer;
 clist = gtk_list_store_new(1, G_TYPE_STRING);

  cview = gtk_tree_view_new_with_model(clist);
  col = gtk_tree_view_column_new_with_attributes("Category Filter",renderer, "text", 0, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(cview), col);
  gtk_tree_view_column_set_expand(col,TRUE);
  cb = gtk_tree_view_column_get_button(col);
  context = gtk_widget_get_style_context(cb);
  gtk_style_context_add_provider(context,GTK_STYLE_PROVIDER(providercvhed),GTK_STYLE_PROVIDER_PRIORITY_USER);



  gtk_list_store_append(clist, &citer);
  gtk_list_store_set(clist,&citer,0,"(None)",-1);
  g_signal_connect(gtk_tree_view_get_selection(GTK_TREE_VIEW(cview)), "changed", G_CALLBACK(rfilter), NULL);
  g_signal_connect(G_OBJECT(cview), "enter-notify-event", G_CALLBACK(trunkfocus),NULL);
  gtk_tree_selection_set_mode(gtk_tree_view_get_selection(cview),GTK_SELECTION_MULTIPLE);

    GtkWidget * cvwindow = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (cvwindow),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
    gtk_container_add (GTK_CONTAINER (cvwindow), cview);
    gtk_widget_show (cview);
    gtk_widget_show (cvwindow);
    gfix(gf10,cvwindow,20,326,290,145);

 tf1 = gtk_radio_button_new_with_label(NULL,"No filter");
 tf2 = gtk_radio_button_new_with_label_from_widget(tf1,"Show matches");
 tf3 = gtk_radio_button_new_with_label_from_widget(tf1,"Match all tags (comma-separated)");
 tf4 = gtk_radio_button_new_with_label_from_widget(tf1,"Exclude matches");
 g_signal_connect(tf1, "clicked", G_CALLBACK(rfilter), NULL);
 g_signal_connect(tf2, "clicked", G_CALLBACK(rfilter), NULL);
 g_signal_connect(tf4, "clicked", G_CALLBACK(rfilter), NULL);
 gfix(gf10,tf1,20,476,300,20);
 gfix(gf10,tf2,20,506,300,20);
 gfix(gf10,tf4,20,536,300,20);
 gsn = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
 gfix(gf10,gsn,5,568,370,3);



  tfmodel = gtk_tree_store_new(1, G_TYPE_STRING);
  combotf=gtk_combo_box_new_with_model_and_entry(tfmodel);
  gtk_combo_box_set_entry_text_column(combotf,0);
  GtkWidget * gctf = gtk_bin_get_child(combotf);
  gtk_entry_set_width_chars(gctf,5);
  gfix(gf10,combotf,20,605,250,40);
  g_signal_connect(G_OBJECT(combotf), "changed", G_CALLBACK(rfilter), NULL);
  GtkWidget * combotftext = gtk_bin_get_child(combotf);
  g_signal_connect(G_OBJECT(combotftext), "focus-out-event", G_CALLBACK(savetf), 0);
  button = gtk_button_new_with_label("X");
  gfix(gf10,button,275,605,40,40);
  g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(cleartf), NULL);

  GList * ct = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (combotf));
  GtkCellRenderer * gct = ct->data;
  g_object_set (gct, "size",(gint)ffd,"size-set",TRUE, NULL);
  g_list_free(ct);




  label = gtk_label_new("Filter/search: \342\234\224");
  gtk_label_set_markup(label,"<span font-weight='bold'>Search Filter:</span>");
  gtk_misc_set_alignment(GTK_MISC(label), 0, .5);
  gfix(gf10,label,20,575,100,20);

  inotes=gtk_check_button_new_with_label("Include Tags/Notes/Details");
  gfix(gf10,inotes,20,710,300,20);
  g_signal_connect(G_OBJECT(inotes), "clicked", G_CALLBACK(rfilter), NULL);

  icat=gtk_check_button_new_with_label("Include Categories");
  gfix(gf10,icat,20,680,300,20);
  g_signal_connect(G_OBJECT(icat), "clicked", G_CALLBACK(rfilter), NULL);

  ititle=gtk_check_button_new_with_label("Include Titles");
  gtk_toggle_button_set_active(ititle,TRUE);
  gfix(gf10,ititle,20,650,300,20);
  g_signal_connect(G_OBJECT(ititle), "clicked", G_CALLBACK(rfilter), NULL);

  gsn = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  gfix(gf10,gsn,5,740,370,3);

  fhidedone=gtk_check_button_new_with_label("Hide done tasks");
  gfix(gf10,fhidedone,20,745,300,20);
  g_signal_connect(G_OBJECT(fhidedone), "clicked", G_CALLBACK(rfilter), NULL);

  fhidefuture=gtk_check_button_new_with_label("Hide future tasks");
  gfix(gf10,fhidefuture,20,775,100,20);
  g_signal_connect(G_OBJECT(fhidefuture), "clicked", G_CALLBACK(rfilter), NULL);

  fhideinactive=gtk_check_button_new_with_label("Hide inactive tasks");
  gfix(gf10,fhideinactive,200,745,120,20);
  g_signal_connect(G_OBJECT(fhideinactive), "clicked", G_CALLBACK(rfilter), NULL);

  fhidecheckboxes=gtk_check_button_new_with_label("Hide checkboxes");
  gfix(gf10,fhidecheckboxes,200,775,120,20);
  g_signal_connect(G_OBJECT(fhidecheckboxes), "clicked", G_CALLBACK(colvis), NULL);

  usecolors=gtk_check_button_new_with_label("Use text highlights");
  gtk_toggle_button_set_active(usecolors,TRUE);
  gfix(gf10,usecolors,20,805,300,20);
  g_signal_connect(G_OBJECT(usecolors), "clicked", G_CALLBACK(rcolors), NULL);

  comparent=gtk_check_button_new_with_label("Auto-complete parent task if all subs done");
  gfix(gf10,comparent,20,835,300,20);
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(sortl2), TRUE);


sortset[1].heading=1;
sortset[2].heading=2;
sortset[3].heading=3;
sortset[4].heading=4;
sortset[5].heading=5;
sortset[6].heading=6;

gtk_widget_show_all (win);
gtk_widget_hide(hboxrec1);
gtk_widget_hide(hboxdays);
gtk_widget_hide(hboxmonth);
gtk_widget_hide(comborba);
gtk_widget_hide(labelrba);
gtk_widget_hide(ddc);
gtk_widget_hide(gfh);
gtk_widget_hide(ebch);
gtk_widget_hide(ebch2);

delling=1;
selnone(0);
gtk_widget_set_sensitive(vbox3,FALSE);

loadprefs();

if(welcome){gtk_tree_model_foreach(gtf, resel, 1);}

gtk_tree_sortable_set_sort_column_id(sortable, 11, GTK_SORT_DESCENDING);
gtk_tree_sortable_set_sort_column_id(sortable2, 1, GTK_SORT_DESCENDING);

GtkAccelGroup * gag = gtk_accel_group_new();
gtk_window_add_accel_group(win,gag);
gtk_accel_group_connect(gag,GDK_KEY_Insert,0,0,g_cclosure_new_swap(G_CALLBACK(newtaska),0,NULL));
gtk_accel_group_connect(gag,GDK_KEY_Insert,GDK_CONTROL_MASK,0,g_cclosure_new_swap(G_CALLBACK(newtaska),1,NULL));
gtk_accel_group_connect(gag,GDK_KEY_Insert,GDK_CONTROL_MASK | GDK_SHIFT_MASK,0,g_cclosure_new_swap(G_CALLBACK(newtaska),2,NULL));
gtk_accel_group_connect(gag,GDK_KEY_Return,GDK_CONTROL_MASK,0,g_cclosure_new_swap(G_CALLBACK(togcomp),2,NULL));
gtk_accel_group_connect(gag,GDK_KEY_Right,GDK_CONTROL_MASK,0,g_cclosure_new_swap(G_CALLBACK(exrow),2,NULL));
gtk_accel_group_connect(gag,GDK_KEY_Escape,0,0,g_cclosure_new_swap(G_CALLBACK(chaparx),0,NULL));
gtk_accel_group_connect(gag,GDK_KEY_Return,0,0,g_cclosure_new_swap(G_CALLBACK(newtasko),0,NULL));
gtk_accel_group_connect(gag,GDK_KEY_space,0,0,g_cclosure_new_swap(G_CALLBACK(newtasku),0,NULL));


GList * taborder=NULL;
taborder= g_list_append(taborder, gftop);
taborder= g_list_append(taborder, swindow);
taborder= g_list_append(taborder, gf2);
taborder= g_list_append(taborder, gf10);
gtk_container_set_focus_chain(gfx,taborder);



tickover2(NULL);
tickover2(NULL);
g_timeout_add_seconds(10, tickover2, NULL);


supdate();


rfilter();
delling=0;
GtkTreeSelection * sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(trunk));
onSelectionChanged(sel,1);
zcpy(&dbz,0,"");
startup=0;

gtk_widget_grab_focus(trunk);
gtk_main ();
tdbupdate();
pthread_mutexattr_destroy(&Attr);
pthread_mutex_destroy(&lock);
pthread_mutex_destroy(&zlock);
free(dbz);
free(dbd);
free(uidlist);
if(fd){close(fd);}

return 0;

}
