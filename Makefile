build:
	@echo "Building..."
	@gcc -w -pthread `pkg-config --cflags gtk+-3.0` -fexceptions -no-pie -Wno-error=incompatible-pointer-types -Wno-implicit-function-declaration -Wno-int-conversion -Wno-return-mismatch -Wno-error -Wno-unknown-warning-option -O2  ./src/main.c ./src/sqlite3.c `pkg-config --libs gtk+-3.0` -ldl -s -o ./finitodo
	@echo "Done"

install:
	@sudo mkdir -p "/usr/bin"
	@sudo mkdir -p "/usr/share/applications"
	@sudo install -m 755 ./finitodo "/usr/bin"
	@sudo install ./Bin/finitodo.desktop "/usr/share/applications"

uninstall:
	@sudo rm "/usr/bin/finitodo"
	@sudo rm "/usr/share/applications/finitodo.desktop"
